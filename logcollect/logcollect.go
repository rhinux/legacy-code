/*
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-01-02 23:55:59
 * Last-Modified: 2014-09-16 15:06:46
 */
package main

import (
	"bufio"
	"flag"
	"fmt"
	"github.com/nightlyone/lockfile"
	"github.com/widuu/goini"
	"io"
	"log/syslog"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

// command args
var (
	conffile    = flag.String("conf", "./conf/conf.ini", "configure file")
	dev         = flag.String("dev", "false", "dev mode ")
	defLogLevel int
	logPri      string
	logTag      string
)

//loglevel
const (
	DEBUG = 4
	INFO  = 3
	WARN  = 2
	ERR   = 1
)

func main() {
	flag.Parse()

	//start lock pid file
	myPath, _ := getMyPath()
	pidFileBase := filepath.Base(*conffile)
	pidFile := myPath + "/run/" + pidFileBase + ".pid"

	conffile := myPath + "/" + *conffile
	conf := goini.SetConfig(conffile)
	logLevel := conf.GetValue("global", "logLevel")
	logPri = conf.GetValue("global", "logPri")
	logTag = conf.GetValue("global", "logTag")

	switch logLevel {
	case "DEBUG":
		defLogLevel = DEBUG
	case "INFO":
		defLogLevel = INFO
	case "WARN":
		defLogLevel = WARN
	case "ERR":
		defLogLevel = ERR
	default:
		defLogLevel = ERR
	}
	lock, errlock := lockfile.New(pidFile)
	if errlock != nil {
		logstr := "[func writePid] Cannot init lock pidfile  exit process "
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
	errlock = lock.TryLock()
	if errlock != nil {
		logstr := "[func writePid] process is running  exit process " + pidFile
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
	defer lock.Unlock()

	logstr = "[func main] start process lock pidfile successed "
	logcat(logstr, defLogLevel, INFO, logPri, logTag)
	//end lock pid file

	//time.Sleep(1000000 * time.Millisecond)

	data := conf.ReadList()
	for {
		for _, v1 := range data {
			for k2, v2 := range v1 {
				if k2 != "global" {
					logFile := v2["logLocalFile"]
					var logMark bool
					var logMarkString string
					if v2["logMark"] == "true" {
						logMark = true
						logMarkString = v2["logMarkString"]
					} else {
						logMark = false
						logMarkString = ""
					}
					statFile := myPath + "/run/" + k2 + ".stat"
					flumeLogcat(logFile, statFile, logMark, logMarkString)
				}
			}
		}
		time.Sleep(1000 * time.Millisecond)
		logstr := "[func logput] sleep 1000 ms"
		logcat(logstr, defLogLevel, INFO, logPri, logTag)
	}
}

func getInodeWithFile(fileName string) string {
	out, err := exec.Command("/bin/ls", "-i", fileName).Output()
	if err != nil {
		logstr := "[func getInodeWithFile] can't found file " + fileName
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
	}
	str := strings.Split(string(out), " ")
	return str[0]
}

func getFileWithInode(inode string, dir string) (string, bool) {
	out, err := exec.Command("/bin/ls", "-i", dir).Output()
	if err != nil {
		logstr := "[func getFileWithInode ]can't found dir " + dir
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
	}
	str := strings.Split(string(out), "\n")
	var i int
	var logFilestr string
	var returnerr bool
	for i = 0; i < len(str)-1; i++ {
		strTrim := strings.TrimSpace(str[i])
		strsub := strings.Split(strTrim, " ")
		if strsub[0] == inode {
			logFilestr = strsub[1]
			returnerr = false
			//fmt.Println("in GetFile With Inode break" + strsub[0])
			break
		} else {
			returnerr = true
		}
	}
	returnstr := dir + logFilestr
	return returnstr, returnerr
}

type log struct {
	logFile       string
	startPos      int64
	logMark       bool
	logMarkString string
}

func (l *log) logput() int64 {
	fi, err := os.OpenFile(l.logFile, os.O_RDONLY, 0)
	if err != nil {
		logstr := "[func logput ] open log file > " + l.logFile + " < failed"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	} else {
		logstr := "[func logput ] open log file > " + l.logFile + " < successed"
		logcat(logstr, defLogLevel, INFO, logPri, logTag)
	}
	// l.startPos from statfile
	endPos := l.startPos
	fi.Seek(l.startPos, 0)
	br := bufio.NewReader(fi)
	lines := 0
	needMark := l.logMark
	markString := l.logMarkString
	for {
		line, err := br.ReadString('\n')
		if needMark {
			line = markString + line
		}
		//max output line 1024 lines
		if lines > 1024 || err == io.EOF {
			break
		} else {
			fmt.Printf("%v", line)
			endPos += int64(len(line))
			lines += 1
			logstr := "[func logput ] output   " + strconv.Itoa(len(line)) + " strings "
			logcat(logstr, defLogLevel, INFO, logPri, logTag)
		}
	}
	deltaPos := endPos - l.startPos
	deltaPosstr := fmt.Sprintf("%d", deltaPos)
	logstr := "[func logput] trans " + deltaPosstr + " bits " + strconv.Itoa(lines) + " lines"
	logcat(logstr, defLogLevel, WARN, logPri, logTag)
	defer fi.Close()
	return endPos
}

func logcat(logs string, defLogLevel int, curLogLevel int, logPri string, logTag string) {
	devmode := *dev
	if devmode == "true" {
		fmt.Printf("[loglevel: %d ][%s]\n", curLogLevel, logs)
	}

	if defLogLevel >= curLogLevel {
		logpriArr := strings.Split(logPri, ".")
		var l3 *syslog.Writer
		switch logpriArr[0] {
		case "LOCAL0":
			l3, _ = syslog.New(syslog.LOG_LOCAL0, logTag)
		case "LOCAL1":
			l3, _ = syslog.New(syslog.LOG_LOCAL1, logTag)
		case "LOCAL2":
			l3, _ = syslog.New(syslog.LOG_LOCAL2, logTag)
		case "LOCAL3":
			l3, _ = syslog.New(syslog.LOG_LOCAL3, logTag)
		case "LOCAL4":
			l3, _ = syslog.New(syslog.LOG_LOCAL4, logTag)
		case "LOCAL5":
			l3, _ = syslog.New(syslog.LOG_LOCAL5, logTag)
		case "LOCAL6":
			l3, _ = syslog.New(syslog.LOG_LOCAL6, logTag)
		case "LOCAL7":
			l3, _ = syslog.New(syslog.LOG_LOCAL7, logTag)
		default:
			l3, _ = syslog.New(syslog.LOG_LOCAL3, logTag)
		}
		defer l3.Close()
		switch logpriArr[1] {
		case "DEBUG":
			l3.Debug(logs)
		case "INFO":
			l3.Info(logs)
		case "NOTICE":
			l3.Notice(logs)
		case "WARNING":
			l3.Warning(logs)
		case "ERR":
			l3.Err(logs)
		case "CRIT":
			l3.Crit(logs)
		case "ALERT":
			l3.Alert(logs)
		case "EMERG":
			l3.Emerg(logs)
		default:
			l3.Info(logs)
		}
		l3.Close()
	}
}

//return logfile,logfile position, is rotate
func detectLogfilePos(statFile string, localLog string) (string, int, bool) {
	if _, err := os.Stat(statFile); err == nil {
		logstr := "[func detectLogfilePos] found stat file " + statFile + "use it"
		logcat(logstr, defLogLevel, INFO, logPri, logTag)

		if fi, err2 := os.Open(statFile); err2 == nil {
			logstr := "[func detectLogfilePos] open stat file " + statFile
			logcat(logstr, defLogLevel, INFO, logPri, logTag)

			br := bufio.NewReader(fi)
			line, err := br.ReadString('\n')
			if err == io.EOF && len(line) == 0 {
				//if statfile is empty return original localLog file and position 0
				logstr := "[func detectLogfilePos] stat file " + statFile + "empty  use logfile" + localLog + " position 0"
				logcat(logstr, defLogLevel, WARN, logPri, logTag)
				return localLog, 0, false
			} else {
				subline := strings.Split(line, "|")
				//filepath.Dir(localLog)+"/"
				currentInode := getInodeWithFile(localLog)
				if currentInode == subline[0] {
					// same inode return current log file
					logstr := "[func detectLogfilePos] stat file " + statFile + "inode same with " + localLog +
						" inode  " + currentInode + " position " + subline[1]
					logcat(logstr, 0, 0, logPri, logTag)
					filePos, _ := strconv.Atoi(subline[1])
					return localLog, filePos, false
				} else {
					previousFile, err := getFileWithInode(subline[0], filepath.Dir(localLog)+"/")
					if !err {
						logstr := "[func detectLogfilePos] stat file " + statFile + "inode: " + subline[0] + "diff with " +
							localLog + " inode: " + currentInode + "change to previousFile " +
							previousFile + " position: " + subline[1]
						logcat(logstr, 0, 0, logPri, logTag)
						filePos, _ := strconv.Atoi(subline[1])
						return previousFile, filePos, true
					} else {
						logstr := "[func detectLogfilePos] can not Found previousFile with inode " +
							subline[0] + " you can try find it or clear stat file " + statFile
						logcat(logstr, 0, 0, logPri, logTag)
						os.Exit(1)
					}
				}
			}
			defer fi.Close()

		} else {
			logstr := "[func detectLogfilePos] read  stat file failed " + statFile
			logcat(logstr, 0, 0, logPri, logTag)
			os.Exit(1)
		}

	} else { // if statfile not exist create it and return original localLog file and position 0
		logstr := "[func detectLogfilePos] can not found stat file " + statFile + " create it"
		logcat(logstr, 0, 0, logPri, logTag)
		if fi, err := os.OpenFile(statFile, os.O_CREATE, 0666); err == nil {
			logstr := "[func detectLogfilePos] created  new stat file " + statFile
			logcat(logstr, 0, 0, logPri, logTag)
			defer fi.Close()
			return localLog, 0, false
		} else {
			logstr := "[func detectLogfilePos] create new stat file failed " + statFile
			logcat(logstr, 0, 0, logPri, logTag)
			os.Exit(1)
		}

	}
	logstr := "[func detectLogfilePos] unknow err in func detect"
	logcat(logstr, 0, 0, logPri, logTag)
	os.Exit(1)
	return "0", 0, false
}
func getMyPath() (string, string) {
	file, _ := exec.LookPath(os.Args[0])
	file, _ = filepath.Abs(file)
	path := filepath.Dir(file)
	processName := filepath.Base(file)
	return path, processName
}

func isFileExist(file string) bool {
	if _, err := os.Stat(file); err == nil {
		return true
	} else {
		return false
	}
}

func updateStatFile(statFile string, inode string, position string, rotate bool) {
	// truncat it if it already existes
	if fi, err := os.Create(statFile); err == nil {
		var statline string
		if rotate {
			statline = ""
		} else {
			statline = inode + "|" + position
		}
		if _, err2 := fi.WriteString(statline); err2 == nil {
			//fmt.Println("write stats ok")
			logstr := "[func updateStatFile] stat file " + statFile + "update success with " + statline
			logcat(logstr, 0, 0, logPri, logTag)
		} else {
			logstr := "[func updateStatFile] stat file " + statFile + "update false "
			logcat(logstr, 0, 0, logPri, logTag)
			os.Exit(1)
		}
	} else {
		logstr := "[func updateStatFile] stat file " + statFile + "open  false "
		logcat(logstr, 0, 0, logPri, logTag)
		os.Exit(1)
	}
}

func flumeLogcat(firstLogFile string, statFile string, logMark bool, logMarkString string) {
	localLog, startPos, rotate := detectLogfilePos(statFile, firstLogFile)
	mylog := log{localLog, int64(startPos), logMark, logMarkString}
	endPosstr := mylog.logput()
	endPos := fmt.Sprintf("%d", endPosstr)
	//	fmt.Println(endPos)
	logFileInode := getInodeWithFile(localLog)
	updateStatFile(statFile, logFileInode, endPos, rotate)
}
