/*
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-01-02 23:55:59
 * Last-Modified: 2014-10-17 17:23:31
 */
package main

import (
	"bufio"
	"database/sql"
	"flag"
	"fmt"
	_ "github.com/mattn/go-oci8"
	"github.com/nightlyone/lockfile"
	"github.com/widuu/goini"
	"io"
	"log/syslog"
	"os"
	"os/exec"
	"path/filepath"
	"redis"
	"sohu"
	"strconv"
	"strings"
	"time"
	"upyun"
)

// command args
var (
	conffile    = flag.String("conf", "./conf/zhaotoup_supply.ini", "configure file")
	dev         = flag.String("dev", "false", "dev mode ")
	defLogLevel int
	logPri      string
	logTag      string
)

//loglevel
const (
	DEBUG = 4
	INFO  = 3
	WARN  = 2
	ERR   = 1
)

func main() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("\n\n\n\t\t\t", err)
		}
	}()

	flag.Parse()
	myPath, _ := getMyPath()
	pidFileBase := filepath.Base(*conffile)
	pidFile := myPath + "/run/" + pidFileBase + ".pid"
	// init log configure
	conffile := myPath + "/" + *conffile
	conf := goini.SetConfig(conffile)
	logLevel := conf.GetValue("global", "logLevel")
	logPri = conf.GetValue("global", "logPri")
	logTag = conf.GetValue("global", "logTag")
	switch logLevel {
	case "DEBUG":
		defLogLevel = DEBUG
	case "INFO":
		defLogLevel = INFO
	case "WARN":
		defLogLevel = WARN
	case "ERR":
		defLogLevel = ERR
	default:
		defLogLevel = ERR
	}

	//start lock pid file
	lock, errlock := lockfile.New(pidFile)
	if errlock != nil {
		logstr := "[func main init] Cannot init lock pidfile  exit process "
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
	errlock = lock.TryLock()
	if errlock != nil {
		logstr := "[func main init] process is running  exit process " + pidFile
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
	defer lock.Unlock()
	//end lock pid file
	//time.Sleep(1000000 * time.Millisecond)
	const timeFormat = "2006-01-02 15:04:05"
	cacheIP := conf.GetValue("global", "cacheIP")
	cachePort, _ := strconv.Atoi(conf.GetValue("global", "cachePort"))
	cacheDB, _ := strconv.Atoi(conf.GetValue("global", "cacheDB"))
	mqKey := conf.GetValue("global", "mqKey")
	//searchDuration, _ := strconv.Atoi(conf.GetValue("global", "duration"))
	//searchDuration := 10
	databaseIP := conf.GetValue("global", "databaseIP")
	databasePort, _ := strconv.Atoi(conf.GetValue("global", "databasePort"))
	databaseInstans := conf.GetValue("global", "databaseInstans")
	databaseUser := conf.GetValue("global", "databaseUser")
	databasePasswd := conf.GetValue("global", "databasePasswd")
	dsn := zhaoGetdsn(databaseUser, databasePasswd, databaseIP, databasePort, databaseInstans)
	//可以监控statFile如果超过5分钟未更新可以报警
	statFile := myPath + "/stat/" + pidFileBase + ".stat"
	mqRetryFile := myPath + "/stat/" + pidFileBase + "mq.retry"
	baseTime := time.Now().Add(time.Duration(-3 * 24 * time.Hour))
	// 读取查询记录的开始时间
	//开始时间为：为读取记录区间内的最新时间
	//statFile 打不开或者为空或者格式不对的都采用当时时间减3天
	//减3天是为了避免图片更新程序的数据积压
	logstr := "[func main] current time:" + baseTime.Format(timeFormat)
	logcat(logstr, defLogLevel, WARN, logPri, logTag)
	if fi, err2 := os.Open(statFile); err2 == nil {
		logstr := "[func detectBasedate] open stat file " + statFile
		logcat(logstr, defLogLevel, INFO, logPri, logTag)
		br := bufio.NewReader(fi)
		line, err := br.ReadString('\n')
		if err == io.EOF && len(line) == 0 {
			logstr := "[func detectBasedate] stat file  empty  use current time"
			logcat(logstr, defLogLevel, WARN, logPri, logTag)
		} else {
			savedTime, err := time.Parse(timeFormat, line)
			if err != nil {
				logstr := "[func detectBasedate] stat file " + line + " time format error  use current time"
				logcat(logstr, defLogLevel, WARN, logPri, logTag)
			} else {
				baseTime = savedTime
				logstr := "[func detectBasedate]   use statfile savedTime:" + baseTime.Format(timeFormat)
				logcat(logstr, defLogLevel, WARN, logPri, logTag)
			}
		}
	} else {
		logstr := "[func detectBasedate] stat file  open false  use current time"
		logcat(logstr, defLogLevel, WARN, logPri, logTag)
	}
	/*
			   select id,auction_no,created_at from auctions where CLOUD_PIC_URL is null and CREATED_AT >= to_date('2014-05-01 17:45:39','yyyy-mm-dd hh24:mi:ss') and CREATED_AT <to_date('2014-05-22 17:45:39','yyyy-mm-dd hh24:mi:ss')
		//UPDATED_AT为上传图片时更新，CREATED_AT为ERP创建记录时的时间，所以为了避免erp建好后间隔3天以上才制图，选用UPDATED_AT
			   select name,pic_order from auction_pictures where auction_id=2665715
	*/
	for {
		db, err := sql.Open("oci8", dsn)
		if err != nil {
			logstr := "[func zhaoDbUpdate] open db false exit"
			logcat(logstr, defLogLevel, ERR, logPri, logTag)
			os.Exit(1)
		}
		defer db.Close()
		//baseDate := today+ 3days
		//startDate := baseData
		//endDate := baseDate+ 1 Hour
		//每次读取一个小时
		//	baseTime := time.Now().Add(time.Duration(-3 * 24 * time.Hour))
		startDate := baseTime.Format(timeFormat)
		endDate := baseTime.Add(time.Duration(-1 * time.Hour)).Format(timeFormat)

		queryRows := "select id,auction_no from auctions where CLOUD_PIC_URL is null and UPDATED_AT >= to_date('" +
			endDate + "','yyyy-mm-dd hh24:mi:ss') and UPDATED_AT < to_date('" + startDate +
			"','yyyy-mm-dd hh24:mi:ss')" //rownum<=200"

		rows, err := db.Query(queryRows)
		defer rows.Close()

		auctionsMap := make(map[string]string)
		auctionsCount := 0
		if err != nil {
			logstr := "[func main] Query exec  false [ " + queryRows + " ] go next"
			logcat(logstr, defLogLevel, ERR, logPri, logTag)
			break
		} else {
			logstr := "[func main] Query fetch no cloud_pic auctions  exec  successed"
			logcat(logstr, defLogLevel, INFO, logPri, logTag)
			var id, auctionNo string
			for rows.Next() {
				if err := rows.Scan(&id, &auctionNo); err != nil {
					logstr := "[func main] scan auctions row error"
					logcat(logstr, defLogLevel, ERR, logPri, logTag)
				}
				auctionsMap[id] = auctionNo
				auctionsCount++
			}
			logstr = "[func main] found " + strconv.Itoa(auctionsCount) + " auctions"
			logcat(logstr, defLogLevel, INFO, logPri, logTag)
		}
		//通过第一次查询结果获取拍品id和路径地址，组成MQ存入mqS切片
		mqS := []string{}
		for auctionsMapId, auctionsMapNo := range auctionsMap {
			queryRows := "select name,pic_order from auction_pictures where auction_id=" + auctionsMapId
			rows, err := db.Query(queryRows)
			defer rows.Close()
			if err != nil {
				logstr := "[func main] Query exec  false [ " + queryRows + " ] go next"
				logcat(logstr, defLogLevel, ERR, logPri, logTag)
				break
			} else {
				logstr := "[func main] Query fetch no cloud_pic auction_pictures  exec  successed"
				logcat(logstr, defLogLevel, INFO, logPri, logTag)
				var picUri, picOrder string
				for rows.Next() {
					if err := rows.Scan(&picUri, &picOrder); err != nil {
						logstr := "[func main] scan auction_pictures row error"
						logcat(logstr, defLogLevel, ERR, logPri, logTag)
					} else {
						picUriArr := strings.Split(picUri, "/")
						mqLine := auctionsMapId + "_" + auctionsMapNo + "_" + picOrder + "_" + picUriArr[1] + "_" + picUriArr[2]
						logstr := "[func main] append mqLine: " + mqLine
						logcat(logstr, defLogLevel, INFO, logPri, logTag)
						mqS = append(mqS, mqLine)
					}
				}
			}
		}
		db.Close()
		//讲mqS切片内容写入redis
		var recordCount int
		spec := redis.DefaultSpec().Host(cacheIP).Port(cachePort).Db(cacheDB).Password("")
		client, err := redis.NewSynchClientWithSpec(spec)
		if err != nil {
			logstr := "[func lpushMq] connect  redis " + cacheIP + ":" + strconv.Itoa(cachePort) + " false sleep 10000ms"
			logcat(logstr, defLogLevel, ERR, logPri, logTag)
			time.Sleep(10000 * time.Millisecond)
		} else {
			for _, mqLine := range mqS {
				if err := newLpushMq(client, mqKey, mqLine); err == nil {
					logstr := "[func newlpushMq] " + mqLine + " insert success"
					logcat(logstr, defLogLevel, INFO, logPri, logTag)
					recordCount += 1
				} else {
					logstr := "[func newlpushMq] " + mqLine + " insert false"
					logcat(logstr, defLogLevel, ERR, logPri, logTag)
					updateRetryFile(mqRetryFile, mqLine+"\n")
				}
			}
			//前移1天
			client.Quit()
			baseTime = baseTime.Add(time.Duration(-1 * time.Hour))
			updateFile(statFile, baseTime.Format(timeFormat))
			logstr := "[func main] update baseTime to " + baseTime.Format(timeFormat)
			logcat(logstr, defLogLevel, INFO, logPri, logTag)

		}
		if recordCount < 10 {
			logstr := "[func logput] sleep 1000 ms"
			logcat(logstr, defLogLevel, INFO, logPri, logTag)
			time.Sleep(1000 * time.Millisecond)
		} else {
			myTimes := time.Duration(recordCount * 10000)
			logstr := "[func logput] sleep " + strconv.Itoa(recordCount*10000) + " ms"
			logcat(logstr, defLogLevel, INFO, logPri, logTag)
			time.Sleep(myTimes * time.Millisecond)
		}

		//os.Exit(1) //for dev
	} //end read MQ
}

//end main

/* function def
 *
 */

func Substr(str string, start, length int) string {
	rs := []rune(str)
	rl := len(rs)
	end := 0
	if start < 0 {
		start = rl - 1 + start
	}
	end = start + length
	if start > end {
		start, end = end, start
	}
	if start < 0 {
		start = 0
	}
	if start > rl {
		start = rl
	}
	if end < 0 {
		end = 0
	}
	if end > rl {
		end = rl
	}
	return string(rs[start:end])
}

/*
* @filename local image ABS name
* @cloudBucket cloud bucket
* @picAuctionNo
* return  cloudFileName "cloudbucketname/picAuctionNo[0-2] or [0-end]/
*                       picAuctionNo[3-5] or [3-end] or null/imagefile basename"
 */

func getCloudFileName(filename string, bucket string, picAuctionNo string, i int) string {
	subDirP := "/" + bucket
	subDir := ""
	if i == 4 {
		subDirP = "/" + bucket + "/originalimg"
	}
	if len(picAuctionNo) > 3 {
		subDir = "/" + Substr(picAuctionNo, 0, 3) + "/" + Substr(picAuctionNo, 3, 3)
	} else {
		if len(picAuctionNo) > 1 {
			subDir = "/" + Substr(picAuctionNo, 0, 3)
		}
	}

	return subDirP + subDir + "/" + filepath.Base(filename)
}

func writeToup(u *upyun.UpYun, fhup *os.File, cloudFileName string) error {
	err := u.WriteFile(cloudFileName, fhup, true)
	if err == nil {
		logstr := "[func writeToup ] upload " + cloudFileName + " to upai sucessed"
		logcat(logstr, defLogLevel, INFO, logPri, logTag)
		return nil
	} else {
		logstr := "[func writeToup ] upload " + cloudFileName + " to upai false"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err
	}
}

func writeTocs(s *sohu.Sohu, fhcs *os.File, cloudFileName string) error {
	err := s.WriteFile(cloudFileName, fhcs)
	if err == nil {
		logstr := "[func writeTocs ] csload " + cloudFileName + " to cs sucessed"
		logcat(logstr, defLogLevel, INFO, logPri, logTag)
		return nil
	} else {
		logstr := "[func writeTocs ] csload " + cloudFileName + " to cs false"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err
	}
}

/*
* @redisHost
* @redisPort
* @redisDb
* @mqKey
* return  queuestring errorString
 */
func rpopMq(redisHost string, redisPort int, redisDb int, mqKey string) (string, error) {
	spec := redis.DefaultSpec().Host(redisHost).Port(redisPort).Db(redisDb).Password("")
	client, err := redis.NewSynchClientWithSpec(spec)
	if err != nil {
		logstr := "[func rpopMq] connect  redis " + redisHost + ":" + strconv.Itoa(redisPort) + " false"
		return logstr, err
	}
	url, err2 := client.Rpop(mqKey)
	if err2 != nil {
		logstr := "[func rpopMq] pop queue " + mqKey + " false"
		return logstr, err2
	} else {
		return string(url), nil
	}
}

func lpushMq(redisHost string, redisPort int, redisDb int, mqKey string, mqLine string) error {
	mqbyte := []byte(strings.TrimSpace(mqLine))
	spec := redis.DefaultSpec().Host(redisHost).Port(redisPort).Db(redisDb).Password("")
	client, err := redis.NewSynchClientWithSpec(spec)
	if err != nil {
		logstr := "[func lpushMq] connect  redis " + redisHost + ":" + strconv.Itoa(redisPort) + " false"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err
	}
	err2 := client.Lpush(mqKey, mqbyte)
	if err2 != nil {
		logstr := "[func lpushMq] push queue " + mqKey + " false"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err2
	} else {
		return nil
	}
}
func newLpushMq(client redis.Client, mqKey string, mqLine string) error {
	mqbyte := []byte(strings.TrimSpace(mqLine))
	err2 := client.Lpush(mqKey, mqbyte)
	if err2 != nil {
		logstr := "[func lpushMq] push queue " + mqKey + " false"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err2
	} else {
		return nil
	}
}
func updateDB() string {
	/*update oracle database
	 *zhao.auctions.CLOUD_PIC_URL
	 *zhao.auction_pictures.CLOUD_PIC_URL
	 */
	return "0"
}

func zhaoGetdsn(usr string, passwd string, dbIp string, dbPort int, serviceName string) string {
	dsn := usr + "/" + passwd + "@" + dbIp + ":" + strconv.Itoa(dbPort) + "/" + serviceName
	return dsn
}

func zhaoDbUpdate(dsn string, query string) (string, error) {
	db, err := sql.Open("oci8", dsn)
	if err != nil {
		return "[func zhaoDbUpdate] open db false", err
	}
	defer db.Close()

	_, err = db.Exec(query)
	if err != nil {
		return "[func zhaoDbUpdate] update query exec  false", err
	} else {
		return "[func zhaoDbUpdate] update query exec successed", err
	}
}

type zhaopic struct {
	zhaopicFile string
	httpUrl     string
}

func logcat(logs string, defLogLevel int, curLogLevel int, logPri string, logTag string) {
	devmode := *dev
	if devmode == "true" {
		fmt.Printf("[loglevel: %d ][%s]\n", curLogLevel, logs)
	}

	if defLogLevel >= curLogLevel {
		logpriArr := strings.Split(logPri, ".")
		var l3 *syslog.Writer
		switch logpriArr[0] {
		case "LOCAL0":
			l3, _ = syslog.New(syslog.LOG_LOCAL0, logTag)
		case "LOCAL1":
			l3, _ = syslog.New(syslog.LOG_LOCAL1, logTag)
		case "LOCAL2":
			l3, _ = syslog.New(syslog.LOG_LOCAL2, logTag)
		case "LOCAL3":
			l3, _ = syslog.New(syslog.LOG_LOCAL3, logTag)
		case "LOCAL4":
			l3, _ = syslog.New(syslog.LOG_LOCAL4, logTag)
		case "LOCAL5":
			l3, _ = syslog.New(syslog.LOG_LOCAL5, logTag)
		case "LOCAL6":
			l3, _ = syslog.New(syslog.LOG_LOCAL6, logTag)
		case "LOCAL7":
			l3, _ = syslog.New(syslog.LOG_LOCAL7, logTag)
		default:
			l3, _ = syslog.New(syslog.LOG_LOCAL3, logTag)
		}
		defer l3.Close()
		switch logpriArr[1] {
		case "DEBUG":
			l3.Debug(logs)
		case "INFO":
			l3.Info(logs)
		case "NOTICE":
			l3.Notice(logs)
		case "WARNING":
			l3.Warning(logs)
		case "ERR":
			l3.Err(logs)
		case "CRIT":
			l3.Crit(logs)
		case "ALERT":
			l3.Alert(logs)
		case "EMERG":
			l3.Emerg(logs)
		default:
			l3.Info(logs)
		}
		l3.Close()
	}
}

func getMyPath() (string, string) {
	file, _ := exec.LookPath(os.Args[0])
	file, _ = filepath.Abs(file)
	path := filepath.Dir(file)
	processName := filepath.Base(file)
	return path, processName
}

func isFileExist(file string) bool {
	if _, err := os.Stat(file); err == nil {
		return true
	} else {
		return false
	}
}

func updateFile(myfile string, saveStr string) error {
	// truncat it if it already existes
	if fi, err := os.Create(myfile); err == nil {
		if _, err2 := fi.WriteString(saveStr); err2 == nil {
			logstr := "[func updateFile] file " + myfile + " update success with " + saveStr
			logcat(logstr, defLogLevel, INFO, logPri, logTag)
			return err2
		} else {
			logstr := "[func updateFile] file " + myfile + " update false "
			logcat(logstr, defLogLevel, ERR, logPri, logTag)
			return err2
		}
	} else {
		logstr := "[func updateFile]  file " + myfile + " open  false "
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err
	}
}

func updateStatFile(statFile string) {
	// truncat it if it already existes
	if fi, err := os.Create(statFile); err == nil {
		statline := fmt.Sprintf("%s", time.Now())
		if _, err2 := fi.WriteString(statline); err2 == nil {
			logstr := "[func updateStatFile] stat file " + statFile + " update success with " + statline
			logcat(logstr, defLogLevel, INFO, logPri, logTag)
		} else {
			logstr := "[func updateStatFile] stat file " + statFile + " update false "
			logcat(logstr, defLogLevel, ERR, logPri, logTag)
			os.Exit(1)
		}
	} else {
		logstr := "[func updateStatFile] stat file " + statFile + " open  false "
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
}

func updateRetryFile(retryFile string, retryLine string) error {
	var err error
	var fi *os.File
	if isFileExist(retryFile) {
		fi, err = os.OpenFile(retryFile, os.O_RDWR, 0666)
		if err != nil {
			return err
		}
		fi.Seek(0, os.SEEK_END)
	} else {
		fi, err = os.Create(retryFile)
		if err != nil {
			return err
		}
	}
	if _, err2 := fi.WriteString(retryLine); err2 == nil {
		logstr := "[func updateRetryFile] retry file " + retryFile + " update success with " + retryLine
		logcat(logstr, 0, 0, logPri, logTag)
		return nil
	} else {
		logstr := "[func updateRetryFile] retry file " + retryFile + " update false  with " + retryLine + "exit"
		logcat(logstr, 0, 0, logPri, logTag)
		os.Exit(1)
	}
	return nil
}

func upUpload(u *upyun.UpYun, fhup *os.File, cloudFileName string, i int, chup chan int) error {
	errupload := writeToup(u, fhup, cloudFileName)
	if errupload != nil {
		u.UpLoad = false
	}
	fhup.Close()
	chup <- i
	return errupload
}

func csUpload(s *sohu.Sohu, fhcs *os.File, cloudFileName string, i int, chcs chan int) error {
	errupload := writeTocs(s, fhcs, cloudFileName)
	if errupload != nil {
		s.UpLoad = false
	}
	fhcs.Close()
	chcs <- i
	return errupload
}

//域名相同的URL用curUrl，否则合并
func mergerUrl(oldUrl string, curUrl string) string {
	resultUrl := ""
	resultMap := make(map[string]string)
	oldUrlArr := strings.Split(oldUrl, ";")
	curUrlArr := strings.Split(curUrl, ";")
	for i := 0; i < len(oldUrlArr)-1; i++ {
		resultMap[strings.Split(oldUrlArr[i], "/")[2]] = oldUrlArr[i]
	}

	for j := 0; j < len(curUrlArr)-1; j++ {

		resultMap[strings.Split(curUrlArr[j], "/")[2]] = curUrlArr[j]
	}
	for _, url := range resultMap {
		resultUrl = resultUrl + url + ";"
	}
	return resultUrl
}
