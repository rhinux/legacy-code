/*
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-01-02 23:55:59
 * Last-Modified: 2014-09-23 15:10:40
 */
package main

import (
	"database/sql"
	"flag"
	"fmt"
	_ "github.com/mattn/go-oci8"
	"github.com/nightlyone/lockfile"
	"github.com/widuu/goini"
	"io/ioutil"
	"log/syslog"
	"os"
	"os/exec"
	"path/filepath"
	"redis"
	"regexp"
	"sohu"
	"strconv"
	"strings"
	"time"
	"upyun"
)

// command args
var (
	conffile    = flag.String("conf", "./conf/zhaotoup_mark.ini", "configure file")
	piclogfile  = flag.String("listfile", "/tmp/a.log", "list file of picture")
	dev         = flag.String("dev", "false", "dev mode ")
	defLogLevel int
	logPri      string
	logTag      string
)

//loglevel
const (
	DEBUG = 4
	INFO  = 3
	WARN  = 2
	ERR   = 1
)

func main() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("\n\n\n\t\t\t", err)
		}
	}()

	flag.Parse()
	myPath, _ := getMyPath()
	pidFileBase := filepath.Base(*conffile)
	pidFile := myPath + "/run/" + pidFileBase + ".pid"
	// init log configure
	conffile := myPath + "/" + *conffile
	conf := goini.SetConfig(conffile)
	logLevel := conf.GetValue("global", "logLevel")
	logPri = conf.GetValue("global", "logPri")
	logTag = conf.GetValue("global", "logTag")
	switch logLevel {
	case "DEBUG":
		defLogLevel = DEBUG
	case "INFO":
		defLogLevel = INFO
	case "WARN":
		defLogLevel = WARN
	case "ERR":
		defLogLevel = ERR
	default:
		defLogLevel = ERR
	}

	//start lock pid file
	lock, errlock := lockfile.New(pidFile)
	if errlock != nil {
		logstr := "[func main init] Cannot init lock pidfile  exit process "
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
	errlock = lock.TryLock()
	if errlock != nil {
		logstr := "[func main init] process is running  exit process " + pidFile
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
	defer lock.Unlock()
	//end lock pid file
	//time.Sleep(1000000 * time.Millisecond)

	listFile := *piclogfile
	retryFile := myPath + "/run/" + pidFileBase + ".retry"

	//又拍云
	upApi := conf.GetValue("up", "upApi")
	upUser := conf.GetValue("up", "upUser")
	upPasswd := conf.GetValue("up", "upPasswd")
	upBucket := conf.GetValue("up", "upBucket")
	//默认upBucket 也作为空间root目录
	upEnable, err := strconv.ParseBool(conf.GetValue("up", "upEnable"))
	if err != nil {
		logstr := "[func main ] read conf upEnable invalid exit"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
	//搜狐云
	csApi := conf.GetValue("cs", "csApi")
	csAccKey := conf.GetValue("cs", "csAccKey")
	csSecKey := conf.GetValue("cs", "csSecKey")
	csBucket := conf.GetValue("cs", "csBucket")
	//默认csBucket 也作为空间root目录
	csEnable, err := strconv.ParseBool(conf.GetValue("cs", "csEnable"))
	if err != nil {
		logstr := "[func main ] read conf csEnable invalid exit"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
	logstr := "[func init] start process zhaotoup lock pidfile read conffile successed"
	logcat(logstr, defLogLevel, INFO, logPri, logTag)

	fileName := readAllfile(listFile)
	fileNameArr := strings.Split(fileName, "\n")

	var u *upyun.UpYun
	var s *sohu.Sohu
	if upEnable {
		u = upyun.NewUpYun(upBucket, upUser, upPasswd)
		u.SetApiDomain(upApi)
		u.SetBucketName(upBucket)
	}

	if csEnable {
		s = sohu.NewSohu(csBucket, csAccKey, csSecKey)
		s.SetApiHost(csApi)
		s.SetBucketName(csBucket)
		s.SetContentType("image/jpeg")
	}

	for i := 0; i < len(fileNameArr)-1; i++ {
		localFileName := fileNameArr[i]
		fileNameReg := filepath.Base(localFileName)
		reg := regexp.MustCompile(`(^\d+)`)
		picAuctionNo := fmt.Sprintf("%s", reg.FindString(fileNameReg))

		chup := make(chan int)
		chcs := make(chan int)

		fhup, errup := os.Open(localFileName)
		fhcs, errcs := os.Open(localFileName)
		if errup != nil && errcs != nil {
			logstr := "[func main ] open local image file " + localFileName + " false"
			logcat(logstr, defLogLevel, ERR, logPri, logTag)
			continue //如果图片不存在就忽略
		}

		if upEnable {
			upCloudFileName := getCloudFileName(localFileName, upBucket, picAuctionNo, 0)
			u.SetContentMD5(upyun.FileMd5(localFileName))
			logstr := "[func main ] start upai trans"
			logcat(logstr, defLogLevel, INFO, logPri, logTag)
			go upUpload(u, fhup, upCloudFileName, i, chup)
		}
		if csEnable {
			csCloudFileName := getCloudFileName(localFileName, csBucket, picAuctionNo, 0)
			s.SetContentMD5(sohu.FileMd5(localFileName))
			logstr := "[func main ] start csai trans"
			logcat(logstr, defLogLevel, INFO, logPri, logTag)
			go csUpload(s, fhcs, csCloudFileName, i, chcs)
		}
		if upEnable {
			<-chup
		}
		if csEnable {
			<-chcs
		}

		if upEnable && u.UpLoad {
			logstr := "[func main ] upload up successed : " + localFileName
			logcat(logstr, defLogLevel, INFO, logPri, logTag)
		} else if upEnable && !u.UpLoad {
			logstr := "[func main ] upload up false : " + localFileName
			logcat(logstr, defLogLevel, ERR, logPri, logTag)
			retryData := "up|" + localFileName + "\n"
			updateRetryFile(retryFile, retryData)
		}

		if csEnable && s.UpLoad {
			logstr := "[func main ] upload cs successed : " + localFileName
			logcat(logstr, defLogLevel, INFO, logPri, logTag)
		} else if csEnable && !s.UpLoad {
			logstr := "[func main ] upload up false : " + localFileName
			logcat(logstr, defLogLevel, ERR, logPri, logTag)
			retryData := "cs|" + localFileName + "\n"
			updateRetryFile(retryFile, retryData)
		}
		//update Database
		//先读取字段中的内容，再将新的url 添加再后面并且新添加的url需要添加";"作为分隔符

		time.Sleep(100 * time.Millisecond)
		logstr := "[func logput] sleep 100 ms"
		logcat(logstr, defLogLevel, INFO, logPri, logTag)
	}
}

//end main

/* function def
 *
 */

func Substr(str string, start, length int) string {
	rs := []rune(str)
	rl := len(rs)
	end := 0
	if start < 0 {
		start = rl - 1 + start
	}
	end = start + length
	if start > end {
		start, end = end, start
	}
	if start < 0 {
		start = 0
	}
	if start > rl {
		start = rl
	}
	if end < 0 {
		end = 0
	}
	if end > rl {
		end = rl
	}
	return string(rs[start:end])
}

/*
* @filename local image ABS name
* @cloudBucket cloud bucket
* @picAuctionNo
* return  cloudFileName "cloudbucketname/picAuctionNo[0-2] or [0-end]/
*                       picAuctionNo[3-5] or [3-end] or null/imagefile basename"
 */

func getCloudFileName(filename string, bucket string, picAuctionNo string, i int) string {
	subDirP := "/" + bucket
	subDir := ""
	if i == 4 {
		subDirP = "/" + bucket + "/originalimg"
	}
	if len(picAuctionNo) > 3 {
		subDir = "/" + Substr(picAuctionNo, 0, 3) + "/" + Substr(picAuctionNo, 3, 3)
	} else {
		if len(picAuctionNo) > 1 {
			subDir = "/" + Substr(picAuctionNo, 0, 3)
		}
	}

	return subDirP + subDir + "/" + filepath.Base(filename)
}

func writeToup(u *upyun.UpYun, fhup *os.File, cloudFileName string) error {
	err := u.WriteFile(cloudFileName, fhup, true)
	if err == nil {
		logstr := "[func writeToup ] upload " + cloudFileName + " to upai sucessed"
		logcat(logstr, defLogLevel, INFO, logPri, logTag)
		return nil
	} else {
		logstr := "[func writeToup ] upload " + cloudFileName + " to upai false"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err
	}
}

func writeTocs(s *sohu.Sohu, fhcs *os.File, cloudFileName string) error {
	err := s.WriteFile(cloudFileName, fhcs)
	if err == nil {
		logstr := "[func writeTocs ] csload " + cloudFileName + " to cs sucessed"
		logcat(logstr, defLogLevel, INFO, logPri, logTag)
		return nil
	} else {
		logstr := "[func writeTocs ] csload " + cloudFileName + " to cs false"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err
	}
}

/*
* @redisHost
* @redisPort
* @redisDb
* @mqKey
* return  queuestring errorString
 */
func rpopMq(redisHost string, redisPort int, redisDb int, mqKey string) (string, error) {
	spec := redis.DefaultSpec().Host(redisHost).Port(redisPort).Db(redisDb).Password("")
	client, err := redis.NewSynchClientWithSpec(spec)
	if err != nil {
		logstr := "[func rpopMq] connect  redis " + redisHost + ":" + strconv.Itoa(redisPort) + " false"
		return logstr, err
	}
	url, err2 := client.Rpop(mqKey)
	if err2 != nil {
		logstr := "[func rpopMq] pop queue " + mqKey + " false"
		return logstr, err2
	} else {
		return string(url), nil
	}
}

func lpushMq(redisHost string, redisPort int, redisDb int, mqKey string, mqLine string) error {
	mqbyte := []byte(strings.TrimSpace(mqLine))
	spec := redis.DefaultSpec().Host(redisHost).Port(redisPort).Db(redisDb).Password("")
	client, err := redis.NewSynchClientWithSpec(spec)
	if err != nil {
		logstr := "[func lpushMq] connect  redis " + redisHost + ":" + strconv.Itoa(redisPort) + " false"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err
	}
	err2 := client.Lpush(mqKey, mqbyte)
	if err2 != nil {
		logstr := "[func lpushMq] push queue " + mqKey + " false"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err2
	} else {
		return nil
	}
}

func updateDB() string {
	/*update oracle database
	 *zhao.auctions.CLOUD_PIC_URL
	 *zhao.auction_pictures.CLOUD_PIC_URL
	 */
	return "0"
}

func zhaoGetdsn(usr string, passwd string, dbIp string, dbPort int, serviceName string) string {
	dsn := usr + "/" + passwd + "@" + dbIp + ":" + strconv.Itoa(dbPort) + "/" + serviceName
	return dsn
}

func zhaoDbUpdate(dsn string, query string) (string, error) {
	db, err := sql.Open("oci8", dsn)
	if err != nil {
		return "[func zhaoDbUpdate] open db false", err
	}
	defer db.Close()

	_, err = db.Exec(query)
	if err != nil {
		return "[func zhaoDbUpdate] update query exec  false", err
	} else {
		return "[func zhaoDbUpdate] update query exec successed", err
	}
}

type zhaopic struct {
	zhaopicFile string
	httpUrl     string
}

func logcat(logs string, defLogLevel int, curLogLevel int, logPri string, logTag string) {
	devmode := *dev
	if devmode == "true" {
		fmt.Printf("[loglevel: %d ][%s]\n", curLogLevel, logs)
	}

	if defLogLevel >= curLogLevel {
		logpriArr := strings.Split(logPri, ".")
		var l3 *syslog.Writer
		switch logpriArr[0] {
		case "LOCAL0":
			l3, _ = syslog.New(syslog.LOG_LOCAL0, logTag)
		case "LOCAL1":
			l3, _ = syslog.New(syslog.LOG_LOCAL1, logTag)
		case "LOCAL2":
			l3, _ = syslog.New(syslog.LOG_LOCAL2, logTag)
		case "LOCAL3":
			l3, _ = syslog.New(syslog.LOG_LOCAL3, logTag)
		case "LOCAL4":
			l3, _ = syslog.New(syslog.LOG_LOCAL4, logTag)
		case "LOCAL5":
			l3, _ = syslog.New(syslog.LOG_LOCAL5, logTag)
		case "LOCAL6":
			l3, _ = syslog.New(syslog.LOG_LOCAL6, logTag)
		case "LOCAL7":
			l3, _ = syslog.New(syslog.LOG_LOCAL7, logTag)
		default:
			l3, _ = syslog.New(syslog.LOG_LOCAL3, logTag)
		}
		defer l3.Close()
		switch logpriArr[1] {
		case "DEBUG":
			l3.Debug(logs)
		case "INFO":
			l3.Info(logs)
		case "NOTICE":
			l3.Notice(logs)
		case "WARNING":
			l3.Warning(logs)
		case "ERR":
			l3.Err(logs)
		case "CRIT":
			l3.Crit(logs)
		case "ALERT":
			l3.Alert(logs)
		case "EMERG":
			l3.Emerg(logs)
		default:
			l3.Info(logs)
		}
		l3.Close()
	}
}

func getMyPath() (string, string) {
	file, _ := exec.LookPath(os.Args[0])
	file, _ = filepath.Abs(file)
	path := filepath.Dir(file)
	processName := filepath.Base(file)
	return path, processName
}

func isFileExist(file string) bool {
	if _, err := os.Stat(file); err == nil {
		return true
	} else {
		return false
	}
}

func updateStatFile(statFile string) {
	// truncat it if it already existes
	if fi, err := os.Create(statFile); err == nil {
		statline := fmt.Sprintf("%s", time.Now())
		if _, err2 := fi.WriteString(statline); err2 == nil {
			logstr := "[func updateStatFile] stat file " + statFile + " update success with " + statline
			logcat(logstr, defLogLevel, INFO, logPri, logTag)
		} else {
			logstr := "[func updateStatFile] stat file " + statFile + " update false "
			logcat(logstr, defLogLevel, ERR, logPri, logTag)
			os.Exit(1)
		}
	} else {
		logstr := "[func updateStatFile] stat file " + statFile + " open  false "
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
}

func updateRetryFile(retryFile string, retryLine string) error {
	var err error
	var fi *os.File
	if isFileExist(retryFile) {
		fi, err = os.OpenFile(retryFile, os.O_RDWR, 0666)
		if err != nil {
			return err
		}
		fi.Seek(0, os.SEEK_END)
	} else {
		fi, err = os.Create(retryFile)
		if err != nil {
			return err
		}
	}
	if _, err2 := fi.WriteString(retryLine); err2 == nil {
		logstr := "[func updateRetryFile] retry file " + retryFile + " update success with " + retryLine
		logcat(logstr, 0, 0, logPri, logTag)
		return nil
	} else {
		logstr := "[func updateRetryFile] retry file " + retryFile + " update false  with " + retryLine + "exit"
		logcat(logstr, 0, 0, logPri, logTag)
		os.Exit(1)
	}
	return nil
}

func upUpload(u *upyun.UpYun, fhup *os.File, cloudFileName string, i int, chup chan int) error {
	var errupload error
	for retryTimes := 0; retryTimes < 3; retryTimes++ {
		errupload = writeToup(u, fhup, cloudFileName)
		if errupload == nil {
			break
		}
		logstr := "[func upUpload] retry  upload " + cloudFileName
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
	}
	if errupload != nil {
		u.UpLoad = false
	}

	fhup.Close()
	chup <- i
	return errupload
}

func csUpload(s *sohu.Sohu, fhcs *os.File, cloudFileName string, i int, chcs chan int) error {
	var errupload error
	for retryTimes := 0; retryTimes < 3; retryTimes++ {
		errupload := writeTocs(s, fhcs, cloudFileName)
		if errupload == nil {
			break
		}
		logstr := "[func csUpload] retry  upload " + cloudFileName
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
	}

	if errupload != nil {
		s.UpLoad = false
	}

	fhcs.Close()
	chcs <- i
	return errupload
}

//域名相同的URL合并成curlUrl中的第一个
func mergerUrl(oldUrl string, curUrl string) string {
	resultUrl := ""
	oldUrlArr := strings.Split(oldUrl, ";")
	curUrlArr := strings.Split(curUrl, ";")
	for i := 0; i < len(oldUrlArr)-1; i++ {
		same := false
		for j := 0; j < len(curUrlArr)-1; j++ {
			if strings.Split(oldUrlArr[i], "/")[2] == strings.Split(curUrlArr[j], "/")[2] {
				resultUrl = resultUrl + curUrlArr[j] + ";"
				same = true
				break
				//只要匹配到curUrl内有一个和oldUrl相同接下来curUrl后面相同域名的都讲丢弃
			}
		}
		if !same {
			resultUrl = resultUrl + oldUrlArr[i] + ";"
		}
	}
	return resultUrl
}

func readAllfile(path string) string {
	fi, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer fi.Close()
	fd, err := ioutil.ReadAll(fi)
	return string(fd)
}
