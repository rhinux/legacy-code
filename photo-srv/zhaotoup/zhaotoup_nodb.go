/*
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-01-02 23:55:59
 * Last-Modified: 2014-09-20 22:05:04
 */
package main

import (
	"database/sql"
	"flag"
	"fmt"
	_ "github.com/mattn/go-oci8"
	"github.com/nightlyone/lockfile"
	"github.com/widuu/goini"
	"log/syslog"
	"os"
	"os/exec"
	"path/filepath"
	"redis"
	"sohu"
	"strconv"
	"strings"
	"time"
	"upyun"
)

// command args
var (
	conffile    = flag.String("conf", "./conf/zhaotoup.ini", "configure file")
	dev         = flag.String("dev", "false", "dev mode ")
	defLogLevel int
	logPri      string
	logTag      string
)

//loglevel
const (
	DEBUG = 4
	INFO  = 3
	WARN  = 2
	ERR   = 1
)

func main() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("\n\n\n\t\t\t", err)
		}
	}()

	flag.Parse()
	myPath, _ := getMyPath()
	pidFileBase := filepath.Base(*conffile)
	pidFile := myPath + "/run/" + pidFileBase + ".pid"
	// init log configure
	conffile := myPath + "/" + *conffile
	conf := goini.SetConfig(conffile)
	logLevel := conf.GetValue("global", "logLevel")
	logPri = conf.GetValue("global", "logPri")
	logTag = conf.GetValue("global", "logTag")
	switch logLevel {
	case "DEBUG":
		defLogLevel = DEBUG
	case "INFO":
		defLogLevel = INFO
	case "WARN":
		defLogLevel = WARN
	case "ERR":
		defLogLevel = ERR
	default:
		defLogLevel = ERR
	}

	//start lock pid file
	lock, errlock := lockfile.New(pidFile)
	if errlock != nil {
		logstr := "[func main init] Cannot init lock pidfile  exit process "
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
	errlock = lock.TryLock()
	if errlock != nil {
		logstr := "[func main init] process is running  exit process " + pidFile
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
	defer lock.Unlock()
	//end lock pid file
	//time.Sleep(1000000 * time.Millisecond)

	localPath := conf.GetValue("global", "localPath")
	originalPicPath := conf.GetValue("global", "originalPicPath")
	cacheIP := conf.GetValue("global", "cacheIP")
	cachePort, _ := strconv.Atoi(conf.GetValue("global", "cachePort"))
	cacheDB, _ := strconv.Atoi(conf.GetValue("global", "cacheDB"))
	mqKey := conf.GetValue("global", "mqKey")
	retryMqKey := conf.GetValue("global", "retryMqKey")
	readMqWeight, _ := strconv.Atoi(conf.GetValue("global", "readMqWeight"))
	readRetryMqWeight, _ := strconv.Atoi(conf.GetValue("global", "readRetryMqWeight"))
	sumMqWeight := readMqWeight + readMqWeight
	weightInter := 0

	databaseIP := conf.GetValue("global", "databaseIP")
	databasePort, _ := strconv.Atoi(conf.GetValue("global", "databasePort"))
	databaseInstans := conf.GetValue("global", "databaseInstans")
	databaseUser := conf.GetValue("global", "databaseUser")
	databasePasswd := conf.GetValue("global", "databasePasswd")
	dsn := zhaoGetdsn(databaseUser, databasePasswd, databaseIP, databasePort, databaseInstans)
	retryUpdateDbFile := conf.GetValue("global", "retryUpdateDbFile")
	retryUpdateDbFile = myPath + "/" + retryUpdateDbFile
	//statFile := myPath + "/run/" + pidFileBase + ".stat"

	//又拍云
	upApi := conf.GetValue("up", "upApi")
	upUser := conf.GetValue("up", "upUser")
	upPasswd := conf.GetValue("up", "upPasswd")
	upBucket := conf.GetValue("up", "upBucket")
	//默认upBucket 也作为空间root目录
	upCloudDomain := conf.GetValue("up", "upCloudDomain")
	upEnable, err := strconv.ParseBool(conf.GetValue("up", "upEnable"))
	if err != nil {
		logstr := "[func main ] read conf upEnable invalid exit"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
	//搜狐云
	csApi := conf.GetValue("cs", "csApi")
	csAccKey := conf.GetValue("cs", "csAccKey")
	csSecKey := conf.GetValue("cs", "csSecKey")
	csBucket := conf.GetValue("cs", "csBucket")
	//默认csBucket 也作为空间root目录
	csCloudDomain := conf.GetValue("cs", "csCloudDomain")
	csEnable, err := strconv.ParseBool(conf.GetValue("cs", "csEnable"))
	if err != nil {
		logstr := "[func main ] read conf csEnable invalid exit"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
	logstr := "[func init] start process zhaotoup lock pidfile read conffile successed"
	logcat(logstr, defLogLevel, INFO, logPri, logTag)

	for {
		//读取MQ和本地文件部分
		//redis data 1972626_132659002_A_2014_w31
		//{acutionid}_{auction_no}_{order}_{year}_{week}
		weightInter = weightInter + 1
		if weightInter > sumMqWeight {
			weightInter = 1
		}

		picAuctionId := ""
		picAuctionNo := ""
		picOrder := ""
		picYear := ""
		picWeek := ""
		mqLine := ""
		var errReadMq error

		if weightInter <= readRetryMqWeight {
			retryMqLine, errReadMq := rpopMq(cacheIP, cachePort, cacheDB, retryMqKey)
			if errReadMq == nil && len(retryMqLine) > 11 {
				retryMqLineArr := strings.Split(retryMqLine, "|")
				enableCloud := retryMqLineArr[0]
				mqLine = retryMqLineArr[1]
				if enableCloud == "up" {
					upEnable = true
					csEnable = false
				} else if enableCloud == "cs" {
					csEnable = true
					upEnable = false
				} else {
					upEnable = false
					csEnable = false
					logstr := "[func main ] retryqueue cloudName  error sleep 1000ms  " + retryMqLine
					logcat(logstr, defLogLevel, ERR, logPri, logTag)
					//MQ 数据不符合格式要求丢弃
					time.Sleep(1000 * time.Millisecond)
					continue
				}
			} else if len(retryMqLine) == 0 {
				logstr := "[func main ] retryqueue data null sleep 3000ms " + retryMqLine
				time.Sleep(3000 * time.Millisecond)
				logcat(logstr, defLogLevel, INFO, logPri, logTag)
				continue
			} else {
				logstr := "[func main ] retryqueue data format error sleep 1000ms  " + retryMqLine
				logcat(logstr, defLogLevel, ERR, logPri, logTag)
				//MQ 数据不符合格式要求丢弃
				time.Sleep(1000 * time.Millisecond)
				continue

			}

		}
		if weightInter > readRetryMqWeight {
			mqLine, errReadMq = rpopMq(cacheIP, cachePort, cacheDB, mqKey)
		}

		if errReadMq == nil && len(mqLine) > 9 {
			mqLineArr := strings.Split(mqLine, "_")
			picAuctionId = mqLineArr[0]
			picAuctionNo = mqLineArr[1]
			picOrder = mqLineArr[2]
			picYear = mqLineArr[3]
			picWeek = mqLineArr[4]

			logstr := "[func main ] read queue data " + mqLine
			logcat(logstr, defLogLevel, INFO, logPri, logTag)

		} else if len(mqLine) == 0 {
			logstr := "[func main ] queue data null sleep 3000ms " + mqLine
			time.Sleep(3000 * time.Millisecond)
			logcat(logstr, defLogLevel, INFO, logPri, logTag)
			continue
		} else {
			logstr := "[func main ] queue data format error sleep 1000ms  " + mqLine
			logcat(logstr, defLogLevel, ERR, logPri, logTag)
			//MQ 数据不符合格式要求丢弃
			time.Sleep(1000 * time.Millisecond)
			continue
		}

		//sizeType for {picOrder},{picOrder}_M,{picOrder}_S1,{picOrder}_S2,{picOrder}_SS

		fileNameArr := []string{
			localPath + "/" + picYear + "/" + picWeek + "/" + picAuctionNo + picOrder + "_M" + ".jpg",
			localPath + "/" + picYear + "/" + picWeek + "/" + picAuctionNo + picOrder + "_S1" + ".jpg",
			localPath + "/" + picYear + "/" + picWeek + "/" + picAuctionNo + picOrder + "_S2" + ".jpg",
			localPath + "/" + picYear + "/" + picWeek + "/" + picAuctionNo + picOrder + "_SS" + ".jpg",
			originalPicPath + "/" + picAuctionNo + picOrder + "./" + picAuctionNo + picOrder + ".jpg",
			localPath + "/" + picYear + "/" + picWeek + "/" + picAuctionNo + picOrder + ".jpg",
		}

		//upPathPrefix := v2["upPathPrefix"]
		//upRetrylog := myPath + "/run/upReturlog.data"

		//又拍和搜狐整合在一起代码量略少逻辑有点混乱，下个版本打算分开上传和更新数据库逻辑
		var u *upyun.UpYun
		var s *sohu.Sohu
		if upEnable {
			u = upyun.NewUpYun(upBucket, upUser, upPasswd)
			u.SetApiDomain(upApi)
			u.SetBucketName(upBucket)
		}

		if csEnable {
			s = sohu.NewSohu(csBucket, csAccKey, csSecKey)
			s.SetApiHost(csApi)
			s.SetBucketName(csBucket)
			s.SetContentType("image/jpeg")
		}
		for i, localFileName := range fileNameArr {
			chup := make(chan int)
			chcs := make(chan int)

			fhup, errup := os.Open(localFileName)
			fhcs, errcs := os.Open(localFileName)
			if errup != nil && errcs != nil {
				logstr := "[func main ] open local image file " + localFileName + " false"
				logcat(logstr, defLogLevel, ERR, logPri, logTag)
				continue //如果图片不存在就忽略
			}

			if upEnable {
				upCloudFileName := getCloudFileName(localFileName, upBucket, picAuctionNo, i)
				u.SetContentMD5(upyun.FileMd5(localFileName))
				logstr := "[func main ] start upai trans"
				logcat(logstr, defLogLevel, INFO, logPri, logTag)
				go upUpload(u, fhup, upCloudFileName, i, chup)
			}
			if csEnable {
				csCloudFileName := getCloudFileName(localFileName, csBucket, picAuctionNo, i)
				s.SetContentMD5(sohu.FileMd5(localFileName))
				logstr := "[func main ] start csai trans"
				logcat(logstr, defLogLevel, INFO, logPri, logTag)
				go csUpload(s, fhcs, csCloudFileName, i, chcs)
			}
			if upEnable {
				<-chup
			}
			if csEnable {
				<-chcs
			}

			if upEnable && csEnable && !u.UpLoad && !s.UpLoad { //2个云存储都存在upload不成功就送回队列下次重做
				err := lpushMq(cacheIP, cachePort, cacheDB, mqKey, mqLine)
				if err != nil {
					logstr := "[func main ] can not push retry mqLine " + mqLine + "  exit"
					logcat(logstr, defLogLevel, ERR, logPri, logTag)
					os.Exit(1)
				}
				logstr := "[func main ] upload all false push retry mqLine " + mqLine
				logcat(logstr, defLogLevel, ERR, logPri, logTag)
				time.Sleep(1000 * time.Millisecond)
				break
			} else if i == 5 { //如果最后一个图片上传成功则更新数据库
				//只有当up和cs 都经历了最后一个文件才判定是否需要写会原MQ还是单独的RetryMQ
				//弊端：不想增加条件复杂性如果只有一个云存储需要上传的时候不能等到一出错就写会RetryMQ，而是到最后一刻才写会
				/*数据库更新原则 url为picorder=A 需要更新  auction_pictures和auctions
				 *除了url为picOrder ＝A 其他picOrder只要更新 auction_pictures表
				 */
				url := ""
				if upEnable && u.UpLoad {
					url = "http://" + upCloudDomain + getCloudFileName(fileNameArr[i], upBucket, picAuctionNo, i) + ";"
				} else if upEnable && !u.UpLoad {
					//又拍云上传失败
					//{acutionid}_{auction_no}_{order}_{year}_{week}
					retryMq := "up|" + mqLine
					logstr := "[func main ] up upload false save uploadMq" + retryMq
					logcat(logstr, defLogLevel, ERR, logPri, logTag)
					err := lpushMq(cacheIP, cachePort, cacheDB, retryMqKey, retryMq)
					if err != nil {
						logstr := "[func main ] can not push retryMq " + retryMq + "  exit"
						logcat(logstr, defLogLevel, ERR, logPri, logTag)
						os.Exit(1)
					}
				}

				if csEnable && s.UpLoad {
					url = url + "http://" + csCloudDomain + getCloudFileName(fileNameArr[i], csBucket, picAuctionNo, i) + ";"
				} else if csEnable && !s.UpLoad {
					//sohu云上传失败
					retryMq := "cs|" + mqLine
					logstr := "[func main ] up upload false save uploadMq" + retryMq
					logcat(logstr, defLogLevel, ERR, logPri, logTag)
					err := lpushMq(cacheIP, cachePort, cacheDB, retryMqKey, retryMq)
					if err != nil {
						logstr := "[func main ] can not push retryMq " + retryMq + "  exit"
						logcat(logstr, defLogLevel, ERR, logPri, logTag)
						os.Exit(1)
					}
				}

				retryDbData := picAuctionId + "|" + picAuctionNo + "|" + picOrder + "|" + url + "\n"
				//update Database
				//先读取字段中的内容，再将新的url 添加再后面并且新添加的url需要添加";"作为分隔符
				updateRetryFile(retryUpdateDbFile, retryDbData)
				if false {
					fmt.Println(dsn)
				}
				/*
					db, err := sql.Open("oci8", dsn)
					if err != nil {
						logstr := "[func zhaoDbUpdate] open db false exit"
						logcat(logstr, defLogLevel, ERR, logPri, logTag)
						//save retryData
						err = updateRetryFile(retryUpdateDbFile, retryDbData)
						os.Exit(1)
					}
					defer db.Close()

					cloudUrlPrefix := ""
					var nStr sql.NullString
					queryRow := "select CLOUD_PIC_URL from auction_pictures where auction_id=" + picAuctionId + " and PIC_ORDER='" + picOrder + "'"
					err = db.QueryRow(queryRow).Scan(&nStr)
					if err != nil {
						logstr := "[func main] QueryRow(cloudUrlPrefix)  exec  false [ " + queryRow + " ] exit"
						logcat(logstr, defLogLevel, ERR, logPri, logTag)
						err = updateRetryFile(retryUpdateDbFile, retryDbData)
						os.Exit(1)

					} else {
						logstr := "[func main] QueryRow(cloudUrlPrefix)  exec  successed"
						logcat(logstr, defLogLevel, INFO, logPri, logTag)
					}

					if nStr.Valid {
						cloudUrlPrefix = nStr.String
					}
					//			db.Close()

					cloudUrl := cloudUrlPrefix + url
					cloudUrl = url
					logstr := "[func main] QueryRow(cloudUrlPrefix)  exec  successed"
					logcat(logstr, defLogLevel, INFO, logPri, logTag)
					var queryArr []string
					if picOrder == "A" {
						queryArr = []string{"update auctions set CLOUD_PIC_URL='" + cloudUrl + "' where auction_no=" + picAuctionNo,
							"update auction_pictures set CLOUD_PIC_URL='" + cloudUrl + "' where AUCTION_ID=" + picAuctionId + " and PIC_ORDER='" + picOrder + "'"}
					} else {
						queryArr = []string{"update auction_pictures set CLOUD_PIC_URL='" + cloudUrl + "' where AUCTION_ID=" + picAuctionId + " and PIC_ORDER='" + picOrder + "'"}
					}

					for _, query := range queryArr {
						/*
							db, err := sql.Open("oci8", dsn)
							if err != nil {
								logstr := "[func zhaoDbUpdate] open db false"
								logcat(logstr, defLogLevel, ERR,logPri,logTag)
								//save retryData
								err = updateRetryFile(retryUpdateDbFile, retryDbData)
								os.Exit(1)
							}
							defer db.Close()
				*/
				/*					_, err = db.Exec(query)
					if err != nil {
						logstr := "[func main] update query exec  false order " + picOrder
						logcat(logstr, defLogLevel, ERR, logPri, logTag)
						//save url
						err = updateRetryFile(retryUpdateDbFile, retryDbData)
						//os.Exit(1)
					} else {
						logstr := "[func main] update query exec successed order " + picOrder
						logcat(logstr, defLogLevel, INFO, logPri, logTag)
						updateStatFile(statFile)
					}
				}
				db.Close()
				*/
			}
		}

		time.Sleep(100 * time.Millisecond)
		logstr := "[func logput] sleep 100 ms"
		logcat(logstr, defLogLevel, INFO, logPri, logTag)

	} //end read MQ
}

//end main

/* function def
 *
 */

func Substr(str string, start, length int) string {
	rs := []rune(str)
	rl := len(rs)
	end := 0
	if start < 0 {
		start = rl - 1 + start
	}
	end = start + length
	if start > end {
		start, end = end, start
	}
	if start < 0 {
		start = 0
	}
	if start > rl {
		start = rl
	}
	if end < 0 {
		end = 0
	}
	if end > rl {
		end = rl
	}
	return string(rs[start:end])
}

/*
* @filename local image ABS name
* @cloudBucket cloud bucket
* @picAuctionNo
* return  cloudFileName "cloudbucketname/picAuctionNo[0-2] or [0-end]/
*                       picAuctionNo[3-5] or [3-end] or null/imagefile basename"
 */

func getCloudFileName(filename string, bucket string, picAuctionNo string, i int) string {
	subDirP := "/" + bucket
	subDir := ""
	if i == 4 {
		subDirP = "/" + bucket + "/originalimg"
	}
	if len(picAuctionNo) > 3 {
		subDir = "/" + Substr(picAuctionNo, 0, 3) + "/" + Substr(picAuctionNo, 3, 3)
	} else {
		if len(picAuctionNo) > 1 {
			subDir = "/" + Substr(picAuctionNo, 0, 3)
		}
	}

	return subDirP + subDir + "/" + filepath.Base(filename)
}

func writeToup(u *upyun.UpYun, fhup *os.File, cloudFileName string) error {
	err := u.WriteFile(cloudFileName, fhup, true)
	if err == nil {
		logstr := "[func writeToup ] upload " + cloudFileName + " to upai sucessed"
		logcat(logstr, defLogLevel, INFO, logPri, logTag)
		return nil
	} else {
		logstr := "[func writeToup ] upload " + cloudFileName + " to upai false"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err
	}
}

func writeTocs(s *sohu.Sohu, fhcs *os.File, cloudFileName string) error {
	err := s.WriteFile(cloudFileName, fhcs)
	if err == nil {
		logstr := "[func writeTocs ] csload " + cloudFileName + " to cs sucessed"
		logcat(logstr, defLogLevel, INFO, logPri, logTag)
		return nil
	} else {
		logstr := "[func writeTocs ] csload " + cloudFileName + " to cs false"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err
	}
}

/*
* @redisHost
* @redisPort
* @redisDb
* @mqKey
* return  queuestring errorString
 */
func rpopMq(redisHost string, redisPort int, redisDb int, mqKey string) (string, error) {
	spec := redis.DefaultSpec().Host(redisHost).Port(redisPort).Db(redisDb).Password("")
	client, err := redis.NewSynchClientWithSpec(spec)
	if err != nil {
		logstr := "[func rpopMq] connect  redis " + redisHost + ":" + strconv.Itoa(redisPort) + " false"
		return logstr, err
	}
	url, err2 := client.Rpop(mqKey)
	if err2 != nil {
		logstr := "[func rpopMq] pop queue " + mqKey + " false"
		return logstr, err2
	} else {
		return string(url), nil
	}
}

func lpushMq(redisHost string, redisPort int, redisDb int, mqKey string, mqLine string) error {
	mqbyte := []byte(strings.TrimSpace(mqLine))
	spec := redis.DefaultSpec().Host(redisHost).Port(redisPort).Db(redisDb).Password("")
	client, err := redis.NewSynchClientWithSpec(spec)
	if err != nil {
		logstr := "[func lpushMq] connect  redis " + redisHost + ":" + strconv.Itoa(redisPort) + " false"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err
	}
	err2 := client.Lpush(mqKey, mqbyte)
	if err2 != nil {
		logstr := "[func lpushMq] push queue " + mqKey + " false"
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err2
	} else {
		return nil
	}
}

func updateDB() string {
	/*update oracle database
	 *zhao.auctions.CLOUD_PIC_URL
	 *zhao.auction_pictures.CLOUD_PIC_URL
	 */
	return "0"
}

func zhaoGetdsn(usr string, passwd string, dbIp string, dbPort int, serviceName string) string {
	dsn := usr + "/" + passwd + "@" + dbIp + ":" + strconv.Itoa(dbPort) + "/" + serviceName
	return dsn
}

func zhaoDbUpdate(dsn string, query string) (string, error) {
	db, err := sql.Open("oci8", dsn)
	if err != nil {
		return "[func zhaoDbUpdate] open db false", err
	}
	defer db.Close()

	_, err = db.Exec(query)
	if err != nil {
		return "[func zhaoDbUpdate] update query exec  false", err
	} else {
		return "[func zhaoDbUpdate] update query exec successed", err
	}
}

type zhaopic struct {
	zhaopicFile string
	httpUrl     string
}

func logcat(logs string, defLogLevel int, curLogLevel int, logPri string, logTag string) {
	devmode := *dev
	if devmode == "true" {
		fmt.Printf("[loglevel: %d ][%s]\n", curLogLevel, logs)
	}

	if defLogLevel >= curLogLevel {
		logpriArr := strings.Split(logPri, ".")
		var l3 *syslog.Writer
		switch logpriArr[0] {
		case "LOCAL0":
			l3, _ = syslog.New(syslog.LOG_LOCAL0, logTag)
		case "LOCAL1":
			l3, _ = syslog.New(syslog.LOG_LOCAL1, logTag)
		case "LOCAL2":
			l3, _ = syslog.New(syslog.LOG_LOCAL2, logTag)
		case "LOCAL3":
			l3, _ = syslog.New(syslog.LOG_LOCAL3, logTag)
		case "LOCAL4":
			l3, _ = syslog.New(syslog.LOG_LOCAL4, logTag)
		case "LOCAL5":
			l3, _ = syslog.New(syslog.LOG_LOCAL5, logTag)
		case "LOCAL6":
			l3, _ = syslog.New(syslog.LOG_LOCAL6, logTag)
		case "LOCAL7":
			l3, _ = syslog.New(syslog.LOG_LOCAL7, logTag)
		default:
			l3, _ = syslog.New(syslog.LOG_LOCAL3, logTag)
		}
		defer l3.Close()
		switch logpriArr[1] {
		case "DEBUG":
			l3.Debug(logs)
		case "INFO":
			l3.Info(logs)
		case "NOTICE":
			l3.Notice(logs)
		case "WARNING":
			l3.Warning(logs)
		case "ERR":
			l3.Err(logs)
		case "CRIT":
			l3.Crit(logs)
		case "ALERT":
			l3.Alert(logs)
		case "EMERG":
			l3.Emerg(logs)
		default:
			l3.Info(logs)
		}
		l3.Close()
	}
}

func getMyPath() (string, string) {
	file, _ := exec.LookPath(os.Args[0])
	file, _ = filepath.Abs(file)
	path := filepath.Dir(file)
	processName := filepath.Base(file)
	return path, processName
}

func isFileExist(file string) bool {
	if _, err := os.Stat(file); err == nil {
		return true
	} else {
		return false
	}
}

func updateStatFile(statFile string) {
	// truncat it if it already existes
	if fi, err := os.Create(statFile); err == nil {
		statline := fmt.Sprintf("%s", time.Now())
		if _, err2 := fi.WriteString(statline); err2 == nil {
			logstr := "[func updateStatFile] stat file " + statFile + " update success with " + statline
			logcat(logstr, defLogLevel, INFO, logPri, logTag)
		} else {
			logstr := "[func updateStatFile] stat file " + statFile + " update false "
			logcat(logstr, defLogLevel, ERR, logPri, logTag)
			os.Exit(1)
		}
	} else {
		logstr := "[func updateStatFile] stat file " + statFile + " open  false "
		logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
}

func updateRetryFile(retryFile string, retryLine string) error {
	var err error
	var fi *os.File
	if isFileExist(retryFile) {
		fi, err = os.OpenFile(retryFile, os.O_RDWR, 0666)
		if err != nil {
			return err
		}
		fi.Seek(0, os.SEEK_END)
	} else {
		fi, err = os.Create(retryFile)
		if err != nil {
			return err
		}
	}
	if _, err2 := fi.WriteString(retryLine); err2 == nil {
		logstr := "[func updateRetryFile] retry file " + retryFile + " update success with " + retryLine
		logcat(logstr, 0, 0, logPri, logTag)
		return nil
	} else {
		logstr := "[func updateRetryFile] retry file " + retryFile + " update false  with " + retryLine + "exit"
		logcat(logstr, 0, 0, logPri, logTag)
		os.Exit(1)
	}
	return nil
}

func upUpload(u *upyun.UpYun, fhup *os.File, cloudFileName string, i int, chup chan int) error {
	errupload := writeToup(u, fhup, cloudFileName)
	if errupload != nil {
		u.UpLoad = false
	}
	fhup.Close()
	chup <- i
	return errupload
}

func csUpload(s *sohu.Sohu, fhcs *os.File, cloudFileName string, i int, chcs chan int) error {
	errupload := writeTocs(s, fhcs, cloudFileName)
	if errupload != nil {
		s.UpLoad = false
	}
	fhcs.Close()
	chcs <- i
	return errupload
}
