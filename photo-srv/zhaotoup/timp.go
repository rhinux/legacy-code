/**
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-10-15 18:29:35
 * Last-Modified: 2014-10-15 20:29:29
 */
package main

import (
	"fmt"
	"time"
)

func main() {
	const longForm = "2006-01-02 15:04:05"
	savedTimechar := "2011-01-02 15:04:05"
	t, err := time.Parse(longForm, savedTimechar)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(t.Format(longForm))
	timeNow := time.Now()
	fmt.Print(timeNow.Format(longForm))
	drTime := t.Add(time.Duration(-10 * 24 * time.Hour))
	fmt.Print("basetime: " + drTime.Format(longForm))
}
