/**
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-09-22 22:37:39
 * Last-Modified: 2014-09-22 23:24:43
 */
package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

func readAllfile(path string) string {
	fi, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer fi.Close()
	fd, err := ioutil.ReadAll(fi)
	return string(fd)
}

func main() {
	fileList := readAllfile("/tmp/passwd.txt")
	fileListArr := strings.Split(fileList, "\n")
	for i := 0; i < len(fileListArr)-1; i++ {
		fmt.Println(fileListArr[i])
	}
	f := filepath.Base("/tmp/3344134A.txt")
	reg := regexp.MustCompile(`(^\d+)`)
	a := fmt.Sprintf("%s", reg.FindString(f))
}
