/**
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-09-19 17:08:28
 * Last-Modified: 2014-09-19 17:41:35
 */
package main

import (
	"crypto/md5"
	"fmt"
	"io"
)

func StringMd5(s string) string {
	h := md5.New()
	io.WriteString(h, s)
	return fmt.Sprintf("%x", h.Sum(nil))
}

func Substr(str string, start, length int) string {
	rs := []rune(str)
	rl := len(rs)
	end := 0
	if start < 0 {
		start = rl - 1 + start
	}
	end = start + length
	if start > end {
		start, end = end, start
	}
	if start < 0 {
		start = 0
	}
	if start > rl {
		start = rl
	}
	if end < 0 {
		end = 0
	}
	if end > rl {
		end = rl
	}
	return string(rs[start:end])
}

/*
func getCloudFileName(filename string, bucket string, picAuctionNo string, i int) string {
	subDirP := "/" + bucket
	subDir := ""
	bucketMd5 := StringMd5(bucket)
	subDirP := Substr(bucketMd5, 0, 2) + Substr(bucketMd5, 31, 2)
	if i == 4 {
		subDirP = "/" + bucket + "/originalimg"
	}
	if len(picAuctionNo) > 3 {
		subDir = "/" + Substr(picAuctionNo, 0, 3) + "/" + Substr(picAuctionNo, 3, 3)
	} else {
		if len(picAuctionNo) > 1 {
			subDir = "/" + Substr(picAuctionNo, 0, 3)
		}
	}

	return subDirP + subDir + "/" + filepath.Base(filename)
}
*/
func main() {
	bucketMd5 := "53409299A_S2"
	subDirP := Substr(bucketMd5, 0, 3) + "/" + Substr(bucketMd5, 3, 3)
	fmt.Println(subDirP)
}
