/**
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-09-20 23:25:12
 * Last-Modified: 2014-09-28 18:24:21
 */
package main

import (
	"fmt"
	"strings"
)

//域名相同的URL合并成curlUrl中的第一个
func mergerUrl(oldUrl string, curUrl string) string {
	resultUrl := ""
	resultMap := make(map[string]string)
	oldUrlArr := strings.Split(oldUrl, ";")
	curUrlArr := strings.Split(curUrl, ";")
	for i := 0; i < len(oldUrlArr)-1; i++ {
		resultMap[strings.Split(oldUrlArr[i], "/")[2]] = oldUrlArr[i]
	}

	for j := 0; j < len(curUrlArr)-1; j++ {

		resultMap[strings.Split(curUrlArr[j], "/")[2]] = curUrlArr[j]
	}
	for _, url := range resultMap {
		resultUrl = resultUrl + url + ";"
	}
	return resultUrl
}

/*
func mergerUrl(oldUrl string, curUrl string) string {
	resultUrl := ""
	oldUrlArr := strings.Split(oldUrl, ";")
	curUrlArr := strings.Split(curUrl, ";")
	fmt.Println(oldUrlArr, len(oldUrlArr))
	fmt.Println(curUrlArr, len(curUrlArr))
	for i := 0; i < len(oldUrlArr)-1; i++ {
		same := false
		for j := 0; j < len(curUrlArr)-1; j++ {
			if strings.Split(oldUrlArr[i], "/")[2] == strings.Split(curUrlArr[j], "/")[2] {
				resultUrl = resultUrl + curUrlArr[j] + ";"
				same = true
				break
				//只要匹配到curUrl内有一个和oldUrl相同接下来curUrl后面相同域名的都讲丢弃
			}
		}
		if !same {
			resultUrl = resultUrl + oldUrlArr[i] + ";"
		}
	}
	return resultUrl
}
*/
func main() {
	oldUrl := "http://img1.cdn.zhao-cloud.com/zhaoimgstore/157/247/157247002A.jpg;http://img1.cdn.zhao-cloud.com/zhaoimgstore/157/247/157247002A.jpg;http://img2.cdn.zhao-cloud.com/zhaoimages/157/247/157247002A.jpg;"
	curUrl := "http://img1.cdn.zhao-cloud.com/zhaoimgstore/157/247/157247002A.png;http://img2.cdn.zhao-cloud.com/zhaoimages/157/247/157247002A.png;"
	result := mergerUrl(oldUrl, curUrl)
	fmt.Println(result)
}
