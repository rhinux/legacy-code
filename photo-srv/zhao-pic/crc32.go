/**
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-09-29 12:50:52
 * Last-Modified: 2014-10-16 18:24:00
 */
package main

import (
	"fmt"
	"hash/crc32"
)

func main() {
	str := "364054" + ""
	strArr := []byte(str)
	res := crc32.ChecksumIEEE(strArr)
	fmt.Println(res)
}
