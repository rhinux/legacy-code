/**
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-10-27 11:52:14
 * Last-Modified: 2014-12-10 16:16:41
 */
package main

import (
	"database/sql"
	"flag"
	"fmt"
	_ "github.com/mattn/go-oci8"
	"github.com/nightlyone/lockfile"
	"github.com/widuu/goini"
	zp "mylib/Zhaopic"
	"mylib/rhc"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// command args
var (
	conffile = flag.String("conf", "./conf/picConventEc.ini", "configure file")
	//	dev         = flag.String("dev", "false", "dev mode ")
	defLogLevel int
	logPri      string
	logTag      string
)

//loglevel
const (
	DEBUG = 4
	INFO  = 3
	WARN  = 2
	ERR   = 1
)

func main() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("\n\n\n\t\t\t", err)
		}
	}()
	flag.Parse()
	myPath, _ := rhc.GetMyPath()
	pidFileBase := filepath.Base(*conffile)
	pidFile := myPath + "/run/" + pidFileBase + ".pid"
	// init log configure
	conffile := myPath + "/" + *conffile
	conf := goini.SetConfig(conffile)
	logLevel := conf.GetValue("global", "logLevel")
	logPri = conf.GetValue("global", "logPri")
	logTag = conf.GetValue("global", "logTag")
	switch logLevel {
	case "DEBUG":
		defLogLevel = DEBUG
	case "INFO":
		defLogLevel = INFO
	case "WARN":
		defLogLevel = WARN
	case "ERR":
		defLogLevel = ERR
	default:
		defLogLevel = ERR
	}

	//start lock pid file
	lock, errlock := lockfile.New(pidFile)
	if errlock != nil {
		logstr := "[func main init] Cannot init lock pidfile  exit process "
		rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
	errlock = lock.TryLock()
	if errlock != nil {
		logstr := "[func main init] process is running  exit process " + pidFile
		rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
	defer lock.Unlock()
	//end lock pid file
	//time.Sleep(1000000 * time.Millisecond)

	destPrefix := conf.GetValue("global", "destPrefix")
	orgPrefix := conf.GetValue("global", "orgPrefix")
	picSrcPath := conf.GetValue("global", "picSrcPath")
	imageUrlPrefix := conf.GetValue("global", "imageUrlPrefix")
	maxProcessFile, _ := strconv.Atoi(conf.GetValue("global", "maxProcessFile"))

	databaseIP := conf.GetValue("global", "databaseIP")
	databasePort, _ := strconv.Atoi(conf.GetValue("global", "databasePort"))
	databaseInstans := conf.GetValue("global", "databaseInstans")
	databaseUser := conf.GetValue("global", "databaseUser")
	databasePasswd := conf.GetValue("global", "databasePasswd")
	dsn := rhc.ZhaoGetdsn(databaseUser, databasePasswd, databaseIP, databasePort, databaseInstans)
	retryDbFile := conf.GetValue("global", "retryDbFile")
	retryDbFile = myPath + "/" + retryDbFile
	//statFile := myPath + "/run/" + pidFileBase + ".stat"
for{
	var zhaoAuctionSl []*zp.ZhaoAuction
	var destDir, orgDir,curPicDir string
	nowTime := time.Now()
	year, week := nowTime.ISOWeek()
	//存放已取得的图片上传者id和名字
	inOrderUserId := make(map[string]string)
if week < 10 {
            curPicDir = strconv.Itoa(year) + "/w0" + strconv.Itoa(week)
        } else {
            curPicDir = strconv.Itoa(year) + "/w" + strconv.Itoa(week)
        }
	picS, err := rhc.GetFilelist(picSrcPath, maxProcessFile)
	if err != nil {
		logstr := fmt.Sprintf("%v", err)
		rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
	}
	nlsLang := "AMERICAN_AMERICA.AL32UTF8"
	os.Setenv("NLS_LANG", nlsLang)
	db, err := sql.Open("oci8", dsn)
	if err != nil {
		logstr := "[func zhaoDbUpdate] open db false exit"
		rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
	defer db.Close()

	for _, filePath := range picS {
		file := filepath.Base(filePath)
		reg := regexp.MustCompile(`^(\d+)_(\d+)([A-Za-z])\.jpg$`)
		nameReg := reg.ReplaceAllString(file, "$1|$2|$3")
		//$1-userid; $2-auction_no; $3-pic_order
		nameRegarr := strings.Split(nameReg, "|")
		if len(nameRegarr) == 3 {
			//用户名是否合法,不合法记录到日志中
			var newPicture, userIdTrue, auctionNoTrue bool
			if len(inOrderUserId[nameRegarr[0]]) > 0 {
				userIdTrue = true
			} else {
				var nStrTrueName sql.NullString
				queryRow := "select pri.true_name from pri_admins pri where id=" + nameRegarr[0]
				err = db.QueryRow(queryRow).Scan(&nStrTrueName)
				if err != nil {
					logstr := "[func checkuser] exec queryRow " + queryRow + " false " + fmt.Sprintf("%v", err)
					rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
				}
				if nStrTrueName.Valid {
					inOrderUserId[nameRegarr[0]] = nStrTrueName.String
					userIdTrue = true
					logstr := "[func checkuser] found user: " + nStrTrueName.String
					rhc.Logcat(logstr, defLogLevel, INFO, logPri, logTag)
				} else {
					userIdTrue = false
					logstr := "[func main] false user id " + nameRegarr[0] + " not exist"
					rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
					//记录到日志中 删除文件
					continue
				}
			}
			//auctions表 判断拍品编号是否有效
			var nStrAuctionid sql.NullString
			//var nStrPicPath sql.NullString
			//	var picDir string
			queryRow := "select au.id from EC_GOODS au where au.GOODS_NO=" + nameRegarr[1]
			err = db.QueryRow(queryRow).Scan(&nStrAuctionid)
			if err != nil && strings.Contains(fmt.Sprintf("%v", err), "ORA-") {
				logstr := "exec queryRow " + queryRow + " false"
				fmt.Println(logstr)
				auctionNoTrue = false
				rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
				fmt.Println(err)
				continue
			} else {
				if nStrAuctionid.Valid {
					auctionNoTrue = true
					logstr := "found auction no " + nameRegarr[1]
					rhc.Logcat(logstr, defLogLevel, INFO, logPri, logTag)
					//判断picPath是否存在
					/*
						if nStrPicPath.Valid {
							//判断picPath是否合法
							u, err := url.Parse(nStrPicPath.String)
							if err == nil && len(u.Path) > 0 && len(u.Host) > 0 {
								newAuction = false
								//picUri包含images
								//picDir = filepath.Dir(u.Path)
							} else {
								//PicPath存在但格式不合法 删除文件
								//记录日志原路径格式有误无法处理
								//当新的处理完图片后
								//continue
								newAuction = true //值得2次确认
							}
						} else {
							//PicPath不存在为新图
							newAuction = true
						}
					*/
				} else {
					logstr := "can not found auction_no " + nameRegarr[1]
					auctionNoTrue = false
					rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
					//无对应的 auction_no信息 删除文件
					//无法找到拍品号为auction_no的拍品信息
					continue
				}
			}
			//如果图片的拍品编号正确，判断是否为新图片.查询auction_pictures表
			var nStroldPicurlPrefix sql.NullString
			var nStroldPicuri sql.NullString
			var oldPicuri, oldPicurlPrefix string
			queryPicture := "select path,name from ec_good_pictures where auction_id=" +
				nStrAuctionid.String + " and pic_order='" + nameRegarr[2] + "'"
			err := db.QueryRow(queryPicture).Scan(&nStroldPicurlPrefix, &nStroldPicuri)
			if err != nil && strings.Contains(fmt.Sprintf("%v", err), "ORA-") {
				//如果为空是否回返回错误信息
				fmt.Println(err)
				logstr := "[auctionNoTrue] query picture false  [skip]"
				rhc.Logcat(logstr, defLogLevel, INFO, logPri, logTag)
			} else {
				if nStroldPicuri.Valid {
					newPicture = false
					oldPicuri = nStroldPicuri.String
					oldPicurlPrefix = nStroldPicurlPrefix.String
					logstr := "[auctionNoTrue] picuri valid :" + oldPicurlPrefix + " oldPicuri: " + oldPicuri + "[old picture]"
					rhc.Logcat(logstr, defLogLevel, INFO, logPri, logTag)
				} else {
					newPicture = true
					logstr := "[auctionNoTrue] picuri invalid [new picture]"
					rhc.Logcat(logstr, defLogLevel, INFO, logPri, logTag)
				}
			}

			//创建拍品图片信息
			var picUri string
			if userIdTrue && auctionNoTrue {
				picUri = "/images/"+curPicDir + "/" + nameRegarr[1] + nameRegarr[2] + ".jpg"
				logstr := "[main] new  picuri: " + picUri
				rhc.Logcat(logstr, defLogLevel, INFO, logPri, logTag)

				destDir = destPrefix + "/"+curPicDir 
				orgDir = orgPrefix + "/"+curPicDir

				fmt.Println("pic on rule " + nameRegarr[1] + " srcFile :" + filePath + "  destDir: " + destDir)

				zhaoPic := zp.NewZhaopic(filePath)
				logstr = "[main] new zhaopic create: " + filePath
				rhc.Logcat(logstr, defLogLevel, INFO, logPri, logTag)

				//auctionNo, auctionId, auctionOrder, auctionUser, auctionPicUri string, auctionPic *Zhaopic, newPicture bool
				zhaoAuction := zp.NewZhaoAuction(nameRegarr[1], nStrAuctionid.String, nameRegarr[2],
					nameRegarr[0], picUri, zhaoPic, newPicture)
				logstr = "[main] new zhaoAuction create "
				rhc.Logcat(logstr, defLogLevel, INFO, logPri, logTag)
				zhaoAuctionSl = append(zhaoAuctionSl, zhaoAuction)

			} else {
				logstr := "userIdTrue or auctionNoTrue not true"
				rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
			}
			//  insert into auction_pictures(ID,AUCTION_ID,NAME,PATH,PIC_ORDER,OPERATOR_ID,
			//OPERATOR_NAME,REMARKS,STATUS,PIC_FILE_SIZE,PIC_FILE_SPEC,CREATED_AT)
			//	orderId := nameRegarr[2]
			//	fmt.Println(userId)
			//	fmt.Println(auctionNo)
			//	fmt.Println(orderId)
		} else {
			//file记录了不合法日志中 删除文件
			//continue
			logstr := "file name not on rule: " + filePath
			rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
		}
	}
	/*
	   type ZhaoAuction struct {
	       AuctionNo    string
	       AuctionId    string
	       AuctionOrder string
	       AuctionUser  string
	       AuctionPic   *Zhaopic
	       NewAuction   bool
	       NewPicture   bool
	   }
	*/
	for _, zac := range zhaoAuctionSl {
		err := zac.AuctionPic.ConventPic(destDir, orgDir)
		if err != nil {
			//转图出错
			logstr := "[func ConventPic] "+fmt.Sprintf("%v", err)+" convent picture false - [skip]"
			rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
		} else {
			var sqlAuctions, sqlAuctionPic string

			//插入到auction_pictures
			//如果order是A 还需要更新auctions
			//pic_path =http://images.zhaoonlien.com/images/2014/w33/xxxxxxxA.jpg
			//imageurlPrefix
			logstr := "[fuc main] newpic-picURI:" + zac.AuctionPicUri
			rhc.Logcat(logstr, defLogLevel, INFO, logPri, logTag)

			picUriarr := strings.Split(zac.AuctionPicUri, "/")
			//picPath 为picUri去掉images目录
			picPath := "/" + picUriarr[2] + "/" + picUriarr[3] + "/" + picUriarr[4]
			imageUrlPrefix2 := imageUrlPrefix + "/" + picUriarr[1]
			//mqLine := zac.AuctionId + "_" + zac.AuctionNo + "_" + zac.AuctionOrder + "_" + picUriarr[2] + "_" + picUriarr[3]
			//2955127_161463045_C_2014_w48

			if zac.NewPicture {
				//只要是新图片的路径都需要更新auctions表
				if zac.AuctionOrder == "A" {
					sqlAuctions = "update ec_goods set PIC_URL='" + imageUrlPrefix +
						zac.AuctionPicUri + "' where id =" + zac.AuctionId
				}
				sqlAuctionPic = "insert into ec_good_pictures(ID,AUCTION_ID,PATH,NAME,PIC_ORDER,PIC_FILE_SIZE," +
					"OPERATOR_ID,OPERATOR_NAME,REMARKS,STATUS,CREATED_AT,UPDATED_AT,PIC_FILE_SPEC," +
					"UPDATED_ID,UPDATED_NAME) values(EC_GOOD_PICTURES_SEQ.nextval," +
					zac.AuctionId + ",'" + imageUrlPrefix2 + "','" + picPath + "','" + zac.AuctionOrder +
					"'," + fmt.Sprintf("%d", zac.AuctionPic.LargPicFilesize) + ",'" + zac.AuctionUser + "','" +
					inOrderUserId[zac.AuctionUser] + "',''," + "1," + "sysdate,NULL,'" + zac.AuctionPic.LargPicSpec + "','','')"
			} else { //old pic
				sqlAuctionPic = "update ec_good_pictures au set au.pic_file_size=" +
					fmt.Sprintf("%d", zac.AuctionPic.LargPicFilesize) + ",au.Pic_File_Spec='" + zac.AuctionPic.LargPicSpec +
					"',au.updated_id=" + zac.AuctionUser + ",au.updated_name='" + inOrderUserId[zac.AuctionUser] + "'" +
					",au.updated_at=sysdate,path='"+imageUrlPrefix2+ "',name='" + picPath+"' where au.auction_id=" + 
					zac.AuctionId + " and au.PIC_ORDER='" + zac.AuctionOrder + "'"
				if zac.AuctionOrder == "A" {
					sqlAuctions = "update ec_goods set PIC_URL='" + imageUrlPrefix +
						zac.AuctionPicUri + "' where id =" + zac.AuctionId
				}
				//sqlUpdatestats = "UPDATE AUCTION_STATUS_CHANGE_LOGS SET NEW_STATUS=OLD_STATUS + 1," +
				//	"UPDATED_AT=SYSDATE WHERE AUCTION_ID=" + zac.AuctionId
			}
			//更新数据库
			updateDBTx, err := db.Begin()
			updateDBTxErr := false
			if err == nil {
				_, err = updateDBTx.Exec(sqlAuctionPic)
				if err != nil {
					updateDBTxErr = true
					logstr := "[func updateDBTx] exec sqlAuctionPic failed! " + fmt.Sprintf("%v", err)
					rhc.Logcat(logstr, defLogLevel, INFO, logPri, logTag)

				} else {
					logstr := "[func updateDBTx] exec sqlAuctionPic succeed"
					rhc.Logcat(logstr, defLogLevel, INFO, logPri, logTag)
				}
				if zac.AuctionOrder == "A" {
					_, err = updateDBTx.Exec(sqlAuctions)
					if err != nil {
						logstr := "[func updateDBTx] exec sqlAuctions failed! " + fmt.Sprintf("%v", err)
						rhc.Logcat(logstr, defLogLevel, INFO, logPri, logTag)
						updateDBTxErr = true
					} else {
						fmt.Println("exec  tansaction sqlAuctions ")
					}
				}
				sqlLine := sqlAuctionPic + "\n" + sqlAuctions + "\n"
				if !updateDBTxErr {
					err = updateDBTx.Commit()
					if err != nil {
						logstr := "[func updateDB] exec  tansaction commit failed"
						rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
						//save sql line
						err := rhc.UpdateRetryFile(retryDbFile, sqlLine)
						if err != nil {
							logstr := "[func UpdateRetryFile] update retryDbFile failed"
							rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
						} else {
							logstr := "[func UpdateRetryFile] update retryDbFile succeed"
							rhc.Logcat(logstr, defLogLevel, INFO, logPri, logTag)
						}
					}
				} else {
					err := rhc.UpdateRetryFile(retryDbFile, sqlLine)
					if err != nil {
						logstr := "[func UpdateRetryFile] update retryDbFile failed"
						rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
						os.Exit(1)
					} else {
						logstr := "[func UpdateRetryFile] update retryDbFile succeed"
						rhc.Logcat(logstr, defLogLevel, INFO, logPri, logTag)
					}
				}
			} else {
				logstr := "[func updateDB] start tansaction failed"
				rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
			}
			//exec db update
		}
	} //end   for _, zac := range zhaoAuctionSl
 logstr := "[func main] sleep 5000ms"
                                rhc.Logcat(logstr, defLogLevel, ERR, logPri, logTag)
time.Sleep(5000 * time.Millisecond)
}
}
