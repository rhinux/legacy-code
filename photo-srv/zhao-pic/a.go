/**
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-10-23 14:52:33
 * Last-Modified: 2014-12-29 19:01:42
 */
package main

import (
	"fmt"
	"time"
)

func main() {
	nowTime := time.Now()
	//date := time.Now().Format(time.RFC1123)
	fmt.Println(nowTime)
	year, week := nowTime.ISOWeek()
	fmt.Println(year)
	fmt.Println(week)
}
