/**
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-10-28 14:35:35
 * Last-Modified: 2014-10-29 11:32:40
 */
package main

import (
	"fmt"
	"time"
)

func main() {
	/*
		inOrderUserId := make(map[string]string)
		inOrderUserId["666"] = "ab8888c"
		fmt.Println(len(inOrderUserId["666"]))
		if len(inOrderUserId["666"]) > 0 {
			fmt.Println(inOrderUserId["566"])
		}
	*/
	t := time.Now()
	y, w := t.ISOWeek()
	a := y + "/" + w
	fmt.Println(a)
}
