/**
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-10-20 15:57:15
 * Last-Modified: 2014-10-29 10:46:13
 */
package main

import (
	"errors"
	"fmt"
	"github.com/gographics/imagick/imagick"
	_ "github.com/mattn/go-oci8"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func isFileExist(file string) bool {
	if _, err := os.Stat(file); err == nil {
		return true
	} else {
		return false
	}
}

type Zhaopic struct {
	inFileName string
	heightPix  int
	widthPix   int
	quality    uint
	width      int
	height     int
	LargSize   *PicSize
	MidSize    *PicSize
	S1Size     *PicSize
	S2Size     *PicSize
	SSSize     *PicSize
}

func NewZhaopic(inFileName string) *Zhaopic {
	z := new(Zhaopic)
	z.inFileName = inFileName
	z.heightPix = 75
	z.widthPix = 75
	z.quality = 80
	z.LargSize = NewPicSize(2000, 2000)
	z.MidSize = NewPicSize(400, 400)
	z.S1Size = NewPicSize(166, 0)
	z.S2Size = NewPicSize(80, 60)
	z.SSSize = NewPicSize(34, 26)
	return z
}
func (z *Zhaopic) Version() string {
	return "0.1"
}

//func (z *Zhaopic) Picm(width, height int) error {
//}
type PicSize struct {
	Width  uint
	Height uint
}

func NewPicSize(width, height uint) *PicSize {
	nps := new(PicSize)
	nps.Width = width
	nps.Height = height
	return nps
}

/*
*
*@ p *PicSize 尺寸限制
*@ op *PicSize 图片原始尺寸
*@ 返回最大边不超过尺寸限制的宽高值
 */

func countWH(p *PicSize, op *PicSize) (uint, uint) {
	var hWidth, hHeight uint
	if op.Width > p.Width || op.Height > p.Height {
		if (p.Width == 0) || (p.Width > p.Height && p.Height != 0) {
			hHeight = uint(p.Height)
			hWidth = uint((op.Width * (p.Height * 10000 / op.Height)) / 10000)
			//按最短边转换后查看如果还有变超过限制尺寸就按此边重新转换，排除不受限制的边
			if (hWidth > p.Width) && p.Width != 0 {
				hWidth = uint(p.Width)
				hHeight = uint((op.Height * (p.Width * 10000 / op.Width) / 10000))
			}
		} else {
			hWidth = uint(p.Width)
			hHeight = uint((op.Height * (p.Width * 10000 / op.Width) / 10000))
			if (hHeight > p.Height) && p.Height != 0 {
				hHeight = uint(p.Height)
				hWidth = uint((op.Width * (p.Height * 10000 / op.Height)) / 10000)
			}
		}
	} else {
		hWidth = op.Width
		hHeight = op.Height
	}
	return hWidth, hHeight
}

func (z *Zhaopic) ConventPic(outFileDir, orgFileBakDir string) error {
	if !isFileExist(z.inFileName) {
		return errors.New(z.inFileName + " does not exist")
	}
	if !isFileExist(outFileDir) {
		err := os.MkdirAll(outFileDir, 0777)
		if err != nil {
			return err
		}
	}
	if !isFileExist(orgFileBakDir) {
		err := os.MkdirAll(orgFileBakDir, 0777)
		if err != nil {
			return err
		}
	}
	fileBase := filepath.Base(z.inFileName)
	outfileBase := strings.Split(fileBase, "_")[1]
	fileP := strings.Split(outfileBase, ".")[0]
	fileL := outFileDir + "/" + fileBase
	fileM := outFileDir + "/" + fileP + "_M.jpg"
	fileS1 := outFileDir + "/" + fileP + "_S1.jpg"
	fileS2 := outFileDir + "/" + fileP + "_S2.jpg"
	fileSS := outFileDir + "/" + fileP + "_SS.jpg"
	fileOrg := orgFileBakDir + "/" + outfileBase
	fmt.Println(fileM)

	imagick.Initialize()
	defer imagick.Terminate()
	mw := imagick.NewMagickWand()
	defer mw.Destroy()
	err := mw.ReadImage(z.inFileName)
	if err != nil {
		panic(err)
	}

	width := mw.GetImageWidth()
	height := mw.GetImageHeight()
	orgSize := NewPicSize(width, height)
	fmt.Println(width)
	fmt.Println(height)

	largW, largH := countWH(z.LargSize, orgSize)
	midW, midH := countWH(z.MidSize, orgSize)
	s1W, s1H := countWH(z.S1Size, orgSize)
	s2W, s2H := countWH(z.S2Size, orgSize)
	ssW, ssH := countWH(z.SSSize, orgSize)

	fmt.Println(largW, largH)
	fmt.Println(midW, midH)
	fmt.Println(s1W, s1H)
	fmt.Println(s2W, s2H)
	fmt.Println(ssW, ssH)

	err = mw.SetImageCompressionQuality(z.quality)

	err = mw.ResizeImage(largW, largH, imagick.FILTER_LANCZOS, 1)
	if err != nil {
		panic(err)
	}
	err = mw.WriteImage(fileL)
	if err != nil {
		panic(err)
	}
	err = mw.ResizeImage(midW, midH, imagick.FILTER_LANCZOS, 1)
	err = mw.WriteImage(fileM)

	err = mw.ResizeImage(s1W, s1H, imagick.FILTER_LANCZOS, 1)
	err = mw.WriteImage(fileS1)

	err = mw.ResizeImage(s2W, s2H, imagick.FILTER_LANCZOS, 1)
	err = mw.WriteImage(fileS2)

	err = mw.ResizeImage(ssW, ssH, imagick.FILTER_LANCZOS, 1)
	err = mw.WriteImage(fileSS)
	os.Rename(z.inFileName, fileOrg)
	fmt.Println("pic saved")
	return nil
}
func main() {
	nzpic := NewZhaopic("/services/tmp/6003_159761001A.jpg")
	err := nzpic.ConventPic("/services/images/2014/w40", "/services/images/org/2014/w40")
	fmt.Println(err)
}

/*
func main() {
	filemid := "159761002A"
	sourcePath = "/services/tmp/"
	destPath = "/services/image/2014/w38"
	file := "/services/tmp/" + filemid + ".jpg"
	fileL := "/services/tmp/" + filemid + "_L.jpg"
	fileM := "/services/tmp/" + filemid + "_M-t.jpg"
	fileS1 := "/services/tmp/" + filemid + "_S1-t.jpg"
	fileS2 := "/services/tmp/" + filemid + "_S2-t.jpg"
	fileSS := "/services/tmp/" + filemid + "_SS-t.jpg"
	//definite picture size
	largSize := NewPicSize(2000, 2000)
	MidSize := NewPicSize(400, 400)
	S1Size := NewPicSize(166, 0)
	S2Size := NewPicSize(80, 60)
	SSSize := NewPicSize(34, 26)

	imagick.Initialize()
	defer imagick.Terminate()
	mw := imagick.NewMagickWand()
	defer mw.Destroy()
	err := mw.ReadImage(file)
	if err != nil {
		panic(err)
	}
	width := mw.GetImageWidth()
	height := mw.GetImageHeight()
	orgSize := NewPicSize(width, height)
	fmt.Println(width)
	fmt.Println(height)
	largW, largH := countWH(largSize, orgSize)
	midW, midH := countWH(MidSize, orgSize)
	s1W, s1H := countWH(S1Size, orgSize)
	s2W, s2H := countWH(S2Size, orgSize)
	ssW, ssH := countWH(SSSize, orgSize)

	fmt.Println(largW, largH)
	fmt.Println(midW, midH)
	fmt.Println(s1W, s1H)
	fmt.Println(s2W, s2H)
	fmt.Println(ssW, ssH)

	err = mw.SetImageCompressionQuality(80)
	if err != nil {
		panic(err)
	}
	err = mw.ResizeImage(largW, largH, imagick.FILTER_LANCZOS, 1)
	err = mw.WriteImage(fileL)

	err = mw.ResizeImage(midW, midH, imagick.FILTER_LANCZOS, 1)
	err = mw.WriteImage(fileM)

	err = mw.ResizeImage(s1W, s1H, imagick.FILTER_LANCZOS, 1)
	err = mw.WriteImage(fileS1)

	err = mw.ResizeImage(s2W, s2H, imagick.FILTER_LANCZOS, 1)
	err = mw.WriteImage(fileS2)

	err = mw.ResizeImage(ssW, ssH, imagick.FILTER_LANCZOS, 1)
	err = mw.WriteImage(fileSS)
	fmt.Println("pic saved")
}*/
