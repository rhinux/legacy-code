/**
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-12-01 18:34:56
 * Last-Modified: 2014-12-02 13:36:48
 */
package main

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-oci8"
	"mylib/rhc"
	"os"
	"strings"
)

func main() {
	databaseIP := "101.231.110.138"
	databasePort := 3321
	databaseInstans := "zhaodb"
	databaseUser := "zhao"
	databasePasswd := "zhao"
	dsn := rhc.ZhaoGetdsn(databaseUser, databasePasswd, databaseIP, databasePort, databaseInstans)

	nlsLang := "AMERICAN_AMERICA.AL32UTF8"
	os.Setenv("NLS_LANG", nlsLang)
	db, err := sql.Open("oci8", dsn)
	if err != nil {
		logstr := "[func zhaoDbUpdate] open db false exit"
		fmt.Println(logstr)
		os.Exit(1)
	}
	defer db.Close()
	var nStrAuctionid sql.NullString
	sql := "select auction_id from auction_pictures where id=25"
	err = db.QueryRow(sql).Scan(&nStrAuctionid)
	logstr := fmt.Sprintf("%v", err)
	fmt.Println(logstr)
	if strings.Contains(logstr, "ORA-") {
		fmt.Println(err)
	} else {
		if nStrAuctionid.Valid {
			fmt.Println(nStrAuctionid.String)
			fmt.Println("valid")
		} else {
			fmt.Println("novalid")
		}
		/*for rows.Next() {
			err := rows.Scan(&nStrAuctionid)
			fmt.Println(err)
			fmt.Println(nStrAuctionid.String)
		}
		*/
	}
	sqlu1 := "update auction_pictures set OPERATOR_NAME='3rh' where id=12391"
	sqlu2 := "update auction_pictures set OPERATOR_NAM='2rhx' where id=12392"
	updateDBTx, err := db.Begin()

	if err == nil {
		//_, err = updateDBTx.Exec(sqlAuctionPic)
		// _, err = updateDBTx.Prepare(sqlAuctionPic)
		_, err = updateDBTx.Exec(sqlu1)
		if err != nil {
			fmt.Println("exec  tansaction " + sqlu1 + " failed")
			fmt.Println(err)
			//exit
		}
		_, err = updateDBTx.Exec(sqlu2)
	}
	fmt.Println("start a")
	//	err = updateDBTx.Commit()
	//	fmt.Println(err)
	fmt.Println("end a")
}
