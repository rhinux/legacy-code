package sohu

/**
 * 注意: 该包只实现了上传object的功能，其他功能未经测试
 */
import (
	"bytes"
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha1"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

type Sohu struct {
	httpClient  *http.Client
	trans       *http.Transport
	bucketName  string
	accessKey   string
	secKey      string
	apiHost     string
	contentMd5  string
	tmpHeaders  map[string]string
	TimeOut     int
	contentType string
	UpLoad      bool
	Debug       bool
}

/**
 * 初始化 Sohu 存储接口
 * @param bucketName 空间名称
 * @param accessKey 操作员访问keyid
 * @param secKey  操作员密钥
 * return Sohu object
 */
func NewSohu(bucketName, accessKey, secKey string) *Sohu {
	s := new(Sohu)
	s.TimeOut = 300
	s.httpClient = &http.Client{}
	s.httpClient.Transport = &http.Transport{Dial: timeoutDialer(s.TimeOut)}
	s.bucketName = bucketName
	s.accessKey = accessKey
	s.secKey = secKey
	//4s8fsTMby7rhXSbodtKEZA==
	s.apiHost = "shctc.scs.sohucs.com"
	s.UpLoad = true
	s.Debug = false
	return s
}

func (s *Sohu) Version() string {
	return "0.1"
}

/**
 * 切换 API 接口的域名
 * @param domain {
bjctc.scs.sohucs.com 北京电信
bjcnc.scs.sohucs.com 北京联通
shctc.scs.sohucs.com 上海电信
gzctc.scs.sohucs.com 广州电信
 }
 * return 无
*/
func (s *Sohu) SetApiHost(domain string) {
	s.apiHost = domain
}

func (s *Sohu) SetBucketName(bucket string) {
	s.bucketName = bucket
}

func (s *Sohu) SetUpLoad(b bool) {
	s.UpLoad = b
}

/**
 * 设置连接超时时间
 * @param time 秒
 * return 无
 */
func (s *Sohu) SetTimeout(time int) {
	s.TimeOut = time
	s.httpClient.Transport = &http.Transport{Dial: timeoutDialer(s.TimeOut)}
}

/**
 * 设置待上传文件的 Content-MD5 值（如又拍云服务端收到的文件MD5值与用户设置的不一致，
 * 将回报 406 Not Acceptable 错误）
 * @param str （文件 MD5 校验码）
 * return 无
 */
func (s *Sohu) SetContentMD5(str string) {
	s.contentMd5 = str
}

func (s *Sohu) SetContentType(str string) {
	s.contentType = str
}

/**
 * 连接签名方法
 * @param method 请求方式 {GET, POST, PUT, DELETE}
 * 由于sohu云签名是用小写的方法名所以必须转换成小写后再签名
 * return 签名字符串
 */
func (s *Sohu) sign(method, uri, reqHeader, secKey, resourceType string) string {
	var bufSign bytes.Buffer
	if method == "PUT" {
		method = "put"
	}
	if method == "POST" {
		method = "post"
	}
	if method == "DELETE" {
		method = "delete"
	}
	if method == "GET" {
		method = "get"
	}
	bufSign.WriteString(method)
	bufSign.WriteString("\n")
	bufSign.WriteString(reqHeader)
	bufSign.WriteString("\n")
	bufSign.WriteString(uri)
	bufSign.WriteString(resourceType)
	//输出签名的头信息fmt.Println("\n\n" + secKey + "\n" + bufSign.String() + "\n\n")
	rstr := hmacSha1(bufSign.String(), secKey)
	return rstr
}

/**
 * 连接处理逻辑
 * @param method 请求方式 {GET, POST, PUT, DELETE}
 * @param uri 请求地址
 * @param inFile 如果是POST上传文件，传递文件IO数据流
 * @param outFile 如果是GET下载文件，可传递文件IO数据流，这种情况函数也返回""
 * return 请求返回字符串，失败返回""(打开debug状态下遇到错误将中止程序执行)
 */
func (s *Sohu) httpAction(method, uri string, headers map[string]string,
	inFile, outFile *os.File) (string, error) {
	uri = "/" + s.bucketName + uri
	url := "http://" + s.apiHost + uri
	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		if s.Debug {
			fmt.Println("%v", err)
			panic("http.NewRequest failed: " + err.Error())
		}
		return "", err
	}
	for k, v := range headers {
		req.Header.Add(k, v)
	}

	length := FileSize(inFile)
	if s.Debug {
		fmt.Println("inFileSize: ", length)
	}
	resourceType := "?location"
	date := fmt.Sprintf("%v", time.Now().Format("2006-01-02 15:04:05"))
	tmpHead := s.contentMd5 + "\n" + s.contentType + "\n" + date
	host := s.bucketName + "." + s.apiHost
	req.Header.Add("Host", host)
	if method == "PUT" || method == "POST" {
		method = "PUT"
		if inFile != nil {
			if s.contentMd5 != "" {
				req.Header.Add("Content-MD5", s.contentMd5)
				s.contentMd5 = ""
			}
			if s.contentType != "" {
				req.Header.Add("Content-Type", s.contentType)
				s.contentType = ""
			}
			req.Header.Add("Content-Length", strconv.FormatInt(length, 10))
			req.Body = inFile
			req.ContentLength = length
		}
		resourceType = ""
	}
	req.Method = method
	//	fmt.Println(s.accessKey + ":" + method + "--" + uri + "--" + tmpHead + "--" + s.secKey + "--" + resourceType)
	req.Header.Add("Date", date)
	req.Header.Add("Authorization", s.accessKey+":"+s.sign(method, uri, tmpHead, s.secKey, resourceType))

	if method == "HEAD" {
		req.Body = nil
	}

	if s.Debug {
		fmt.Println("resquest:")
		fmt.Println(req)
	}
	resp, err := s.httpClient.Do(req)
	if err != nil {
		if s.Debug {
			fmt.Println(resp.Status, err)
			panic("httpClient.Do failed: " + resp.Status + err.Error())
		}
		return "", err
	}

	fmt.Println(resp.Status, err)
	rc := resp.StatusCode
	if rc == 200 {
		s.tmpHeaders = make(map[string]string)
		for k, v := range resp.Header {
			if strings.Contains(k, "x-scs") {
				s.tmpHeaders[k] = v[0]
			}
		}

		if method == "GET" && outFile != nil {
			_, err := io.Copy(outFile, resp.Body)
			if err != nil {
				if s.Debug {
					fmt.Printf("%v %v\n", rc, err)
					panic("write output file failed: ")
				}
				return "", err
			}
			return "", nil
		}

		buf := bytes.NewBuffer(make([]byte, 0, 8192))
		buf.ReadFrom(resp.Body)
		return buf.String(), nil
	}

	return "", errors.New(resp.Status)
}

/**
 * 获取总体空间的占用信息
 * return 空间占用量，失败返回0.0
 */
func (s *Sohu) GetBucketUsage() (float64, error) {
	return s.GetFolderUsage("/")
}

/**
 * 获取某个子目录的占用信息
 * @param $path 目标路径
 * return 空间占用量和error，失败空间占用量返回0.0
 */
func (s *Sohu) GetFolderUsage(path string) (float64, error) {
	r, err := s.httpAction("GET", path+"?usage", nil, nil, nil)
	if err != nil {
		return 0.0, err
	}
	v, _ := strconv.ParseFloat(r, 64)
	return v, nil
}

/**
 * 上传文件
 * @param filePath 文件路径（包含文件名）
 * @param inFile 文件IO数据流
 * @param autoMkdir 是否自动创建父级目录(最深10级目录)
 * return error
 */
func (s *Sohu) WriteFile(filePath string, inFile *os.File) error {
	var headers map[string]string
	_, err := s.httpAction("PUT", filePath, headers, inFile, nil)
	return err
}

/**
 * 获取上传文件后的信息（仅图片空间有返回数据）
 * @param key 信息字段名（x-upyun-width、x-upyun-height、x-upyun-frames、x-upyun-file-type）
 * return string or ""
 */
func (s *Sohu) GetWritedFileInfo(key string) string {
	if s.tmpHeaders == nil {
		return ""
	}
	return s.tmpHeaders[strings.ToLower(key)]
}

/**
 * 读取文件
 * @param file 文件路径（包含文件名）
 * @param outFile 可传递文件IO数据流（结果返回true or false）
 * return error
 */
func (s *Sohu) ReadFile(file string, outFile *os.File) error {
	_, err := s.httpAction("GET", file, nil, nil, outFile)
	return err
}

/**
 * 获取文件信息
 * @param file 文件路径（包含文件名）
 * return array('type': file | folder, 'size': file size, 'date': unix time) 或 nil
 */
/*
func (s *Sohu) GetFileInfo(file string) map[string]string {
	_, err := s.httpAction("HEAD", file, nil, nil, nil)
	if err != nil {
		return nil
	}
	if s.tmpHeaders == nil {
		return nil
	}
	m := make(map[string]string)
	if v, ok := u.tmpHeaders["x-upyun-file-type"]; ok {
		m["type"] = v
	}
	if v, ok := u.tmpHeaders["x-upyun-file-size"]; ok {
		m["size"] = v
	}
	if v, ok := u.tmpHeaders["x-upyun-file-date"]; ok {
		m["date"] = v
	}
	return m
}
*/

type DirInfo struct {
	Name string
	Type string
	Size int64
	Time int64
}

/**
 * 读取目录列表
 * @param path 目录路径
 * return DirInfo数组 或 nil
 */
func (s *Sohu) ReadDir(path string) ([]*DirInfo, error) {
	r, err := s.httpAction("GET", path, nil, nil, nil)
	if err != nil {
		return nil, err
	}

	dirs := make([]*DirInfo, 0, 8)
	rs := strings.Split(r, "\n")
	for i := 0; i < len(rs); i++ {
		ri := strings.TrimSpace(rs[i])
		rid := strings.Split(ri, "\t")
		d := new(DirInfo)
		d.Name = rid[0]
		if len(rid) > 3 && rid[3] != "" {
			if rid[1] == "N" {
				d.Type = "file"
			} else {
				d.Type = "folder"
			}
			d.Time, _ = strconv.ParseInt(rid[3], 10, 64)
		}
		if len(rid) > 2 {
			d.Size, _ = strconv.ParseInt(rid[2], 10, 64)
		}
		dirs = append(dirs, d)
	}
	return dirs, nil
}

/**
 * 删除文件
 * @param file 文件路径（包含文件名）
 * return error
 */
func (s *Sohu) DeleteFile(file string) error {
	_, err := s.httpAction("DELETE", file, nil, nil, nil)
	return err
}

/**
 * 创建目录
 * @param path 目录路径
 * @param auto_mkdir=false 是否自动创建父级目录
 * return error
 */
func (s *Sohu) MkDir(path string, autoMkdir bool) error {
	var headers map[string]string
	headers = make(map[string]string)
	headers["Folder"] = "true"
	if autoMkdir {
		headers["Mkdir"] = "true"
	}
	_, err := s.httpAction("PUT", path, headers, nil, nil)
	return err
}

/**
 * 删除目录
 * @param path 目录路径
 * return error
 */
func (s *Sohu) RmDir(dir string) error {
	_, err := s.httpAction("DELETE", dir, nil, nil, nil)
	return err
}

func FileSize(f *os.File) int64 {
	if f == nil {
		return 0
	}
	if fi, err := f.Stat(); err == nil {
		return fi.Size()
	}
	return 0
}

func StringMd5(s string) string {
	h := md5.New()
	io.WriteString(h, s)
	return fmt.Sprintf("%x", h.Sum(nil))
}

// S
func FileMd5(name string) string {
	f, err := os.Open(name)
	if err != nil {
		return ""
	}
	defer f.Close()

	h := md5.New()
	io.Copy(h, f)
	return fmt.Sprintf("%x", h.Sum(nil))
}

func timeoutDialer(timeout int) func(string, string) (net.Conn, error) {
	return func(netw, addr string) (c net.Conn, err error) {
		delta := time.Duration(timeout) * time.Second
		c, err = net.DialTimeout(netw, addr, delta)
		if err != nil {
			return nil, err
		}
		c.SetDeadline(time.Now().Add(delta))
		return c, nil
	}
}

/**
 * hmacSha1 (str,key)
 * @param str 需要转码的字符串
 * @param key 密钥
 * return 经过hmac-sha1转码后的字符串
 */

func hmacSha1(str, key string) string {
	hmacKey := []byte(key)
	myHmac := hmac.New(sha1.New, hmacKey)
	myHmac.Write([]byte(str))
	//rstr := fmt.Sprintf("%x", myHmac.Sum(nil))
	rstr := base64.StdEncoding.EncodeToString(myHmac.Sum(nil))
	return rstr
}

func base64str(s string) string {
	return base64.StdEncoding.EncodeToString([]byte(s))
}
