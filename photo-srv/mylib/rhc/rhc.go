package rhc

/**
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-11-26 18:07:10
 * Last-Modified: 2014-12-02 17:24:41
 */

import (
	"database/sql"
	"errors"
	"flag"
	"fmt"
	_ "github.com/mattn/go-oci8"
	"log/syslog"
	"os"
	"os/exec"
	"path/filepath"
	"redis"
	"strconv"
	"strings"
	"time"
)

var (
	//conffile    = flag.String("conf", "./conf/zhaopic.ini", "configure file")
	dev         = flag.String("dev", "false", "dev mode ")
	defLogLevel int
	logPri      string
	logTag      string
)

//loglevel
const (
	DEBUG = 4
	INFO  = 3
	WARN  = 2
	ERR   = 1
)

func Abc() {
	fmt.Println("abc")
}
func Substr(str string, start, length int) string {
	rs := []rune(str)
	rl := len(rs)
	end := 0
	if start < 0 {
		start = rl - 1 + start
	}
	end = start + length
	if start > end {
		start, end = end, start
	}
	if start < 0 {
		start = 0
	}
	if start > rl {
		start = rl
	}
	if end < 0 {
		end = 0
	}
	if end > rl {
		end = rl
	}
	return string(rs[start:end])
}

/*
* @redisHost
* @redisPort
* @redisDb
* @mqKey
* return  queuestring errorString
 */
func RpopMq(redisHost string, redisPort int, redisDb int, mqKey string) (string, error) {
	spec := redis.DefaultSpec().Host(redisHost).Port(redisPort).Db(redisDb).Password("")
	client, err := redis.NewSynchClientWithSpec(spec)
	if err != nil {
		logstr := "[func RpopMq] connect  redis " + redisHost + ":" + strconv.Itoa(redisPort) + " false"
		return logstr, err
	}
	url, err2 := client.Rpop(mqKey)
	if err2 != nil {
		logstr := "[func RpopMq] pop queue " + mqKey + " false"
		return logstr, err2
	} else {
		return string(url), nil
	}
}

func LpushMq(redisHost string, redisPort int, redisDb int, mqKey string, mqLine string) error {
	mqbyte := []byte(strings.TrimSpace(mqLine))
	spec := redis.DefaultSpec().Host(redisHost).Port(redisPort).Db(redisDb).Password("")
	client, err := redis.NewSynchClientWithSpec(spec)
	if err != nil {
		logstr := "[func LpushMq] connect  redis " + redisHost + ":" + strconv.Itoa(redisPort) + " false"
		Logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err
	}
	err2 := client.Lpush(mqKey, mqbyte)
	if err2 != nil {
		logstr := "[func LpushMq] push queue " + mqKey + " false"
		Logcat(logstr, defLogLevel, ERR, logPri, logTag)
		return err2
	} else {
		return nil
	}
}

func ZhaoGetdsn(usr string, passwd string, dbIp string, dbPort int, serviceName string) string {
	dsn := usr + "/" + passwd + "@" + dbIp + ":" + strconv.Itoa(dbPort) + "/" + serviceName
	return dsn
}

func ZhaoDbUpdate(dsn string, query string) (string, error) {
	db, err := sql.Open("oci8", dsn)
	if err != nil {
		return "[func zhaoDbUpdate] open db false", err
	}
	defer db.Close()

	_, err = db.Exec(query)
	if err != nil {
		return "[func zhaoDbUpdate] update query exec  false", err
	} else {
		return "[func zhaoDbUpdate] update query exec successed", err
	}
}

func Logcat(logs string, defLogLevel int, curLogLevel int, logPri string, logTag string) {
	devmode := *dev
	if devmode == "true" {
		fmt.Printf("[loglevel: %d ][%s]\n", curLogLevel, logs)
	}

	if defLogLevel >= curLogLevel {
		logpriArr := strings.Split(logPri, ".")
		var l3 *syslog.Writer
		switch logpriArr[0] {
		case "LOCAL0":
			l3, _ = syslog.New(syslog.LOG_LOCAL0, logTag)
		case "LOCAL1":
			l3, _ = syslog.New(syslog.LOG_LOCAL1, logTag)
		case "LOCAL2":
			l3, _ = syslog.New(syslog.LOG_LOCAL2, logTag)
		case "LOCAL3":
			l3, _ = syslog.New(syslog.LOG_LOCAL3, logTag)
		case "LOCAL4":
			l3, _ = syslog.New(syslog.LOG_LOCAL4, logTag)
		case "LOCAL5":
			l3, _ = syslog.New(syslog.LOG_LOCAL5, logTag)
		case "LOCAL6":
			l3, _ = syslog.New(syslog.LOG_LOCAL6, logTag)
		case "LOCAL7":
			l3, _ = syslog.New(syslog.LOG_LOCAL7, logTag)
		default:
			l3, _ = syslog.New(syslog.LOG_LOCAL3, logTag)
		}
		defer l3.Close()
		switch logpriArr[1] {
		case "DEBUG":
			l3.Debug(logs)
		case "INFO":
			l3.Info(logs)
		case "NOTICE":
			l3.Notice(logs)
		case "WARNING":
			l3.Warning(logs)
		case "ERR":
			l3.Err(logs)
		case "CRIT":
			l3.Crit(logs)
		case "ALERT":
			l3.Alert(logs)
		case "EMERG":
			l3.Emerg(logs)
		default:
			l3.Info(logs)
		}
		l3.Close()
	}
}

func IsFileExist(file string) bool {
	if _, err := os.Stat(file); err == nil {
		return true
	} else {
		return false
	}
}

func GetMyPath() (string, string) {
	file, _ := exec.LookPath(os.Args[0])
	file, _ = filepath.Abs(file)
	path := filepath.Dir(file)
	processName := filepath.Base(file)
	return path, processName
}

func UpdateStatFile(statFile string) {
	// truncat it if it already existes
	if fi, err := os.Create(statFile); err == nil {
		statline := fmt.Sprintf("%s", time.Now())
		if _, err2 := fi.WriteString(statline); err2 == nil {
			logstr := "[func UpdateStatFile] stat file " + statFile + " update success with " + statline
			Logcat(logstr, defLogLevel, INFO, logPri, logTag)
		} else {
			logstr := "[func UpdateStatFile] stat file " + statFile + " update false "
			Logcat(logstr, defLogLevel, ERR, logPri, logTag)
			os.Exit(1)
		}
	} else {
		logstr := "[func UpdateStatFile] stat file " + statFile + " open  false "
		Logcat(logstr, defLogLevel, ERR, logPri, logTag)
		os.Exit(1)
	}
}

func UpdateRetryFile(retryFile string, retryLine string) error {
	var err error
	var fi *os.File
	if IsFileExist(retryFile) {
		fi, err = os.OpenFile(retryFile, os.O_RDWR, 0666)
		if err != nil {
			return err
		}
		fi.Seek(0, os.SEEK_END)
	} else {
		fi, err = os.Create(retryFile)
		if err != nil {
			return err
		}
	}
	if _, err2 := fi.WriteString(retryLine); err2 == nil {
		logstr := "[func UpdateRetryFile] retry file " + retryFile + " update success with " + retryLine
		Logcat(logstr, 0, 0, logPri, logTag)
		return nil
	} else {
		logstr := "[func UpdateRetryFile] retry file " + retryFile + " update false  with " + retryLine + "exit"
		Logcat(logstr, 0, 0, logPri, logTag)
		os.Exit(1)
	}
	return nil
}

/*
*@path 需要遍历的目录
*@return 返回所有文件列表
 */
func GetFilelist(path string, limit int) ([]string, error) {
	PicFilesS := []string{}
	count := 0
	err := filepath.Walk(path, func(path string, f os.FileInfo, err error) error {
		if f == nil {
			return err
		}
		if f.IsDir() {
			return nil
		}
		PicFilesS = append(PicFilesS, path)
		count += 1
		if count < limit {
			return nil
		} else {
			return errors.New("[func GetFilelist] Found more picfiles than limit")
		}
	})
	if err != nil {
		return PicFilesS, err
	}
	return PicFilesS, err
}
