package Zhaopic

/**
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-11-26 17:49:53
 * Last-Modified: 2014-12-03 14:50:47
 */

import (
	"errors"
	"fmt"
	"github.com/gographics/imagick/imagick"
	"mylib/rhc"
	"os"
	"path/filepath"
	"strings"
)

type Zhaopic struct {
	inFileName      string
	heightPix       int
	widthPix        int
	quality         uint
	width           int
	height          int
	LargSize        *PicWH
	MidSize         *PicWH
	S1Size          *PicWH
	S2Size          *PicWH
	SSSize          *PicWH
	LargPicSpec     string
	LargPicFilesize int64
}

func NewZhaopic(inFileName string) *Zhaopic {
	z := new(Zhaopic)
	z.inFileName = inFileName
	z.heightPix = 75
	z.widthPix = 75
	z.quality = 80
	z.LargSize = NewPicWH(2000, 2000)
	z.MidSize = NewPicWH(400, 400)
	z.S1Size = NewPicWH(166, 0)
	z.S2Size = NewPicWH(80, 60)
	z.SSSize = NewPicWH(34, 26)
	return z
}

func (z *Zhaopic) Version() string {
	return "0.1"
}

//图片宽高
type PicWH struct {
	Width  uint
	Height uint
}

func NewPicWH(width, height uint) *PicWH {
	nps := new(PicWH)
	nps.Width = width
	nps.Height = height
	return nps
}

/*
*
*@ p *PicWH 尺寸限制
*@ op *PicWH 图片原始尺寸
*@ 返回最大边不超过尺寸限制的宽高值
 */

func countWH(p *PicWH, op *PicWH) (uint, uint) {
	var hWidth, hHeight uint
	if op.Width > p.Width || op.Height > p.Height {
		if (p.Width == 0) || (p.Width > p.Height && p.Height != 0) {
			hHeight = uint(p.Height)
			hWidth = uint((op.Width * (p.Height * 10000 / op.Height)) / 10000)
			//按最短边转换后查看如果还有变超过限制尺寸就按此边重新转换，排除不受限制的边
			if (hWidth > p.Width) && p.Width != 0 {
				hWidth = uint(p.Width)
				hHeight = uint((op.Height * (p.Width * 10000 / op.Width) / 10000))
			}
		} else {
			hWidth = uint(p.Width)
			hHeight = uint((op.Height * (p.Width * 10000 / op.Width) / 10000))
			if (hHeight > p.Height) && p.Height != 0 {
				hHeight = uint(p.Height)
				hWidth = uint((op.Width * (p.Height * 10000 / op.Height)) / 10000)
			}
		}
	} else {
		hWidth = op.Width
		hHeight = op.Height
	}
	return hWidth, hHeight
}

/*
*@outFileDir  处理过的图片输出目录
*@orgFileBakDir  原图备份目录
*@return error错误信息
 */
func (z *Zhaopic) ConventPic(outFileDir, orgFileBakDirPrefix string) error {
	if !rhc.IsFileExist(z.inFileName) {
		return errors.New(z.inFileName + " does not exist")
	}
	fileBase := filepath.Base(z.inFileName)
	outfileBase := strings.Split(fileBase, "_")[1]
	fileP := strings.Split(outfileBase, ".")[0]

	if !rhc.IsFileExist(outFileDir) {
		err := os.MkdirAll(outFileDir, 0777)
		if err != nil {
			return err
		}
	}
	orgFileBakDir := orgFileBakDirPrefix + "/" + fileP +"."
	if !rhc.IsFileExist(orgFileBakDir) {
		err := os.MkdirAll(orgFileBakDir, 0777)
		if err != nil {
			return err
		}
	}
		fileL := outFileDir + "/" + outfileBase
	fileM := outFileDir + "/" + fileP + "_M.jpg"
	fileS1 := outFileDir + "/" + fileP + "_S1.jpg"
	fileS2 := outFileDir + "/" + fileP + "_S2.jpg"
	fileSS := outFileDir + "/" + fileP + "_SS.jpg"
	fileOrg := orgFileBakDir + "/" + outfileBase

	imagick.Initialize()
	defer imagick.Terminate()
	mw := imagick.NewMagickWand()
	defer mw.Destroy()
	err := mw.ReadImage(z.inFileName)
	if err != nil {
		return err
	}

	width := mw.GetImageWidth()
	height := mw.GetImageHeight()
	orgSize := NewPicWH(width, height)

	largW, largH := countWH(z.LargSize, orgSize)
	midW, midH := countWH(z.MidSize, orgSize)
	s1W, s1H := countWH(z.S1Size, orgSize)
	s2W, s2H := countWH(z.S2Size, orgSize)
	ssW, ssH := countWH(z.SSSize, orgSize)

	z.LargPicSpec = fmt.Sprintf("%d", largW) + "X" + fmt.Sprintf("%d", largH)
	/*
		fmt.Println(largW, largH)
		fmt.Println(midW, midH)
		fmt.Println(s1W, s1H)
		fmt.Println(s2W, s2H)
		fmt.Println(ssW, ssH)
	*/
	err = mw.SetImageCompressionQuality(z.quality)
	if err != nil {
		return err
	}
	err = mw.ResizeImage(largW, largH, imagick.FILTER_LANCZOS, 1)
	if err != nil {
		return err
	}
	err = mw.WriteImage(fileL)
	if err != nil {
		return err
	}
	fileInfo, err := os.Stat(fileL)
	if err != nil {
		return err
	}
	z.LargPicFilesize = fileInfo.Size() / 1024
	err = mw.ResizeImage(midW, midH, imagick.FILTER_LANCZOS, 1)
	if err != nil {
		return err
	}
	err = mw.WriteImage(fileM)
	if err != nil {
		return err
	}

	err = mw.ResizeImage(s1W, s1H, imagick.FILTER_LANCZOS, 1)
	if err != nil {
		return err
	}
	err = mw.WriteImage(fileS1)
	if err != nil {
		return err
	}

	err = mw.ResizeImage(s2W, s2H, imagick.FILTER_LANCZOS, 1)
	if err != nil {
		return err
	}
	err = mw.WriteImage(fileS2)
	if err != nil {
		return err
	}

	err = mw.ResizeImage(ssW, ssH, imagick.FILTER_LANCZOS, 1)
	if err != nil {
		return err
	}
	err = mw.WriteImage(fileSS)
	if err != nil {
		return err
	}
	err = os.Rename(z.inFileName, fileOrg)
	if err != nil {
		return err
	}
	return nil
}

type ZhaoAuction struct {
	AuctionNo     string
	AuctionId     string
	AuctionOrder  string
	AuctionUser   string
	AuctionPicUri string
	AuctionPic    *Zhaopic
	NewPicture    bool
}

func NewZhaoAuction(auctionNo, auctionId, auctionOrder, auctionUser, auctionPicUri string,
	auctionPic *Zhaopic, newPicture bool) *ZhaoAuction {
	za := new(ZhaoAuction)
	za.AuctionNo = auctionNo
	za.AuctionId = auctionId
	za.AuctionOrder = auctionOrder
	za.AuctionUser = auctionUser
	za.AuctionPicUri = auctionPicUri
	za.AuctionPic = auctionPic
	za.NewPicture = newPicture
	return za
}
