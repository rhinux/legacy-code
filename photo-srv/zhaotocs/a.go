/**
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2015-03-24 14:36:36
 * Last-Modified: 2015-03-24 14:52:38
 */
package main

import (
	"fmt"
	"reflect"
)

type Rect struct {
	X, Y, Area, Length float64
}

func (r *Rect) CalArea() {

	r.Area = r.X * r.Y

}

func (r Rect) CalLength() {
	r.Length = 2 * (r.X + r.Y)
	fmt.Println(r.Length)
}

func main() {
	r1 := Rect{3, 4, 0, 0}
	r1.CalArea()
	r1.CalLength()
	fmt.Println(r1.Area)
	fmt.Println(r1.Length)

	r2 := &Rect{5, 6, 0, 0}
	r2.CalArea()
	r2.CalLength()
	fmt.Println(r2.Area)
	fmt.Println(r2.Length)
	fmt.Println(reflect.ValueOf(r2.Area))
	fmt.Println(reflect.ValueOf(r1.Area))

}
