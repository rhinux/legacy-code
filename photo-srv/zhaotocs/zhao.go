/**
 * Name:
 * Comment:
 * Author: Rhinux
 * Web: http://www.rhinux.info./
 * Created: 2014-09-04 17:21:49
 * Last-Modified: 2015-03-31 01:57:28
 */
package main

import (
	"crypto/hmac"
	"crypto/sha1"
	"crypto/tls"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

/*
keyid: g55AbLkUt4uxUCw23LnYpc
key: eb057cba-6f61-4ad2-b67c-cd87b98583a7
appid/appkey如下：
appid: fKhnk3UwbjhLP5NuFNQm5W
appkey: 9dbe75e0-d10a-55a5-b232-ecd60a9f60be
*/

func hmacSha1(str, key string) string {
	hmacKey := []byte(key)
	myHmac := hmac.New(sha1.New, hmacKey)
	myHmac.Write([]byte(str))
	rstr := base64.StdEncoding.EncodeToString(myHmac.Sum(nil))
	return rstr
}
func main() {
	//date := time.Now().UTC().Format(time.RFC1123)
	date := time.Now().Format(time.RFC1123)
	//date := time.Now().Format(time.RFC3339)
	path := "/auth"
	str := "POST\n" + date + "\n" + path + "\nopmonitor"
	//fmt.Println(str)
	signstr := hmacSha1(str, "9dbe75e0-d10a-55a5-b232-ecd60a9f60be")
	//fmt.Println(signstr)
	//cmd := "/usr/bin/curl -vv -X POST -H \"Content-Type: application/json; charset=UTF-8\" -H \"Authorization: zhao fKhnk3UwbjhLP5NuFNQm5W:" + signstr + "\" -H \"X-Zhao-DeviceId: opmonitor\"  -H \"Date:" + date + "\"  -k https://auth.services.zhao-cloud.com/auth  --data-ascii \"{ \\\"username\\\": \\\"8073494\\\", \\\"password\\\": \\\"111111\\\" }\""
	//client
	body := strings.NewReader("{ \"username\": \"8073494\", \"password\": \"111111\" }")
	//client := new(http.Client)

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	reg, err := http.NewRequest("POST", "https://auth.services.zhao-cloud.com/auth", body)
	//reg, err := http.NewRequest("POST", "https://114.80.139.104/auth", body)
	if err != nil {
		fmt.Println(err)
	}
	reg.Header.Set("Content-Type", "application/json; charset=UTF-8")
	reg.Header.Set("Authorization", "zhao fKhnk3UwbjhLP5NuFNQm5W:"+signstr)
	reg.Header.Set("X-Zhao-DeviceId", "opmonitor")
	reg.Header.Set("Date", date)
	resp, err := client.Do(reg)
	//defer resp.Body.Close()
	if err != nil {
		fmt.Println("Error2:", err)
		return
	}
	repsbody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	fmt.Println(string(repsbody))
}
