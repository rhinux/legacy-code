#!/usr/bin/env perl
use Mojolicious::Lite;
use DBI;
use lib 'lib';
use MyUsers;

# Make signed cookies secure
app->secret('Mojolicious rocks');

my $dbFile = "/usr/home/rhinux/Code/perl/cdndns/dnsdata.db";
my $dbh = DBI->connect("dbi:SQLite:dbname=$dbFile",'','',{
        RaiseError => 1,
        AutoCommit => 0
    }
);        
helper db=> sub{$dbh};
helper selectedge => sub {
 my $self = shift;
 my $sth = eval {$self->db->prepare('Select * from edgeServerInfo')} || return undef;
    $sth->execute;
 return $sth->fetchall_arrayref;
};

helper selectdomain => sub {
 my $self = shift;
 my ($foreigndomain,$basedomain) = @_;
 my $sth = eval {$self->db->prepare('Select * from clientToServer where foreigndomain=? and basedomain=?')} || return undef;
    $sth->execute($foreigndomain,$basedomain);
 return $sth->fetchall_arrayref;
};

helper insertedge => sub{
    my $self = shift;
    my($edgeserver,$iporcname,$mytype,$edgecarrier,$edgelocal,$edgestatus) = @_;
    my $sth = eval {$self->db->prepare('INSERT INTO edgeServerInfo 
            (edgeserver,iporcname,mytype,edgecarrier,edgelocal,edgestatus) values(?,?,?,?,?,?)')} 
    || return undef;
    $sth->execute($edgeserver,$iporcname,$mytype,$edgecarrier,$edgelocal,$edgestatus);
    $self->db->commit();
    return 1;
};

helper rmedge=> sub{
    my $self = shift;
    my ($id) = @_;
    my $sth = eval {$self->db->prepare('DELETE FROM edgeServerInfo WHERE id=?')} || return undef;
    $sth->execute($id);
    $self->db->commit();
};
my $users = MyUsers->new;
helper users => sub { return $users };

# Main login action
any '/' => sub {
    my $self = shift;

    # Query or POST parameters
    my $user = $self->param('user') || '';
    my $pass = $self->param('pass') || '';

    # Check password and render "index.html.ep" if necessary
    return $self->render unless $self->users->check($user, $pass);

    # Store username in session
    $self->session(user => $user);

    # Store a friendly message for the next page in flash
    $self->flash(message => 'Thanks for logging in.');

    # Redirect to protected page with a 302 response
    $self->redirect_to('protected');
} => 'index';

# A protected page auto rendering "protected.html.ep"
get '/protected' => sub {
    my $self = shift;

    # Redirect to main page with a 302 response if user is not logged in
    return $self->redirect_to('index') unless $self->session('user');
};

# Logout action
get '/logout' => sub {
    my $self = shift;

    # Expire and in turn clear session automatically
    $self->session(expires => 1);

    # Redirect to main page with a 302 response
    $self->redirect_to('index');
};
get '/status' => sub {
    my $self = shift;
    return $self->redirect_to('index') unless $self->session('user');
};
get '/allEdges' => sub {
    my $self = shift;
    return $self->redirect_to('index') unless $self->session('user');
    my $rows = $self->selectedge;
    $self->stash(rows=> $rows);
    $self->render('allEdges');
};
post '/addEdge' => sub {
    my $self = shift;
    return $self->redirect_to('index') unless $self->session('user');
    my $edgeserver = $self->param('edgeserver');
    my $iporcname = $self->param('iporcname');
    my $mytype= $self->param('mytype');
    my $edgecarrier= $self->param('edgecarrier');
    my $edgelocal= $self->param('edgelocal');
    my $edgestatus= $self->param('edgestatus');
    my $insert = $self->insertedge($edgeserver,$iporcname,$mytype,$edgecarrier,$edgelocal,$edgestatus);
    $self->redirect_to('/allEdges');
};
any '/rmEdge' => sub{
    my $self = shift;
    return $self->redirect_to('index') unless $self->session('user');
    my $id = $self->param('id');
    my $rm = $self->rmedge($id);
    $self->redirect_to('/allEdges');
};
any '/allDomain' => sub{
    my $self = shift;
    return $self->redirect_to('index') unless $self->session('user');
    my $foreigndomain = $self->param('foreigndomain');
    my $basedomain = $self->param('basedomain');
    #my $recordtype = $self->param('recordtype');
    my $rows = $self->selectdomain($foreigndomain,$basedomain);
    $self->stash(rows=>$rows);
    $self->render('allDomain');
};

app->start;

__DATA__

@@ allDomain.html.ep
<form action="/allDomain" method="post">
<table width="30%" border="0.1">
<tr>
<td>foreigndomain:</td>
<td><input type="text" name="foreigndomain" ></td>
</tr>

<tr>
<td>basedomain:</td>
<td><input type="text" name="basedomain" required="required" >*</td>
</tr>
<!--
<tr>
<td>recordtype:</td>
<td><select name="recordtype">
<option>All</option>
<option>CNAME</option>
<option>A</option>
</select>
</tr>
-->
<tr>
<td></td>
<td><input type="reset" value="Reset">&nbsp;&nbsp;&nbsp; <input type="submit"  value="Search"></td>
</tr>
</form>
</table>
<hr>
Domain name List:<br>
<table border="1">
<tr>
<th>id</th>
<th>foreigndomain</th>
<th>clicarrilocal</th>
<th>basedomain</th>
<th>recordtype</th>
<th>iporcname</th>
<th>pdnsid</th>
<th>changetime</th>
</tr>
% foreach my $row (@$rows){
<tr>
%foreach my $text(@$row){
<td><%= $text %></td>
%}
</tr>
%}
</table>
@@ allEdges.html.ep
<hr>
<form action="/addEdge" method="post">
<table width="30%" border="0.1">
<tr>
<td>edgeserver:</td>
<td><input type="text" name="edgeserver" required="required" >*</td>
</tr>
<tr>
<td>iporcname:</td>
<td><input type="text" name="iporcname" required="required" >*</td>
</tr>
<tr>
<td>mytype:</th>
<td><input type="text" name= "mytype" required="required" >*</td>
</tr>
<tr>
<td>edgecarrier:</th>
<td><input type="text" name="edgecarrier" required="required" >*</td>
</tr>
<tr>
<td>edgelocal:</th>
<td><input type="text" name="edgelocal" required="required" >*</td>
</tr>
<tr>
<td>edgestatus:</th>
<td><input type="text" name="edgestatus" required="required" >*</td>
</tr>
<tr>
<td></td>
<td><input type="submit"  value="Add"></td>
</tr>
</table>
</form>
<hr>
<form action="/rmEdge" method="post">
<table borde="0.5">
<tr>
<td>delete records id:</td>
<td><input type="text" name= "id" required="required" > *</td>
</tr>
<tr>
<td></td>
<td><input type="submit" value="Delete"></td>
</tr>
</table>
<hr>
EdgeServer Status List:<br>
<table border="1">
<tr>
<th>id</th>
<th>edgeserver</th>
<th>iporcname</th>
<th>mytype</th>
<th>edgecarrier</th>
<th>edgelocal</th>
<th>edgestatus</th>
</tr>
% foreach my $row (@$rows){
<tr>
%foreach my $text(@$row){
<td><%= $text %></td>
%}
</tr>
%}
</table>


@@ index.html.ep
% layout 'default';
%= form_for index => begin
% if (param 'user') {
<b>Wrong name or password, please try again.</b><br>
% }
Name:<br>
%= text_field 'user'
<br>Password:<br>
%= password_field 'pass'
<br>
%= submit_button 'Login'
% end

@@ protected.html.ep
% layout 'default';
% if (my $msg = flash 'message') {
<b><%= $msg %></b><br>
% }
Welcome <%= session 'user' %>.<br>
%= link_to Logout => 'logout'
<hr>
%= link_to 域名管理 => 'domainm'
%= link_to 状态查询 => 'status'

@@ status.html.ep
% if (my $msg = flash 'message') {                                                                                                                               
<b><%= $msg %></b><br>                                                                                                                                       
% }                                                                                                                                                          
Welcome <%= session 'user' %>.<br>                                                                                                                               
%= link_to Logout => 'logout'                                                                                                                                    
<hr>                             
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title></title>
<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
<meta name="ProgId" content="VisualStudio.HTML">
<meta name="Originator" content="Microsoft Visual Studio .NET 7.1">
</head>
<body>
<form name="doublecombo" ID="Form1">
<p>
<select name="example" size="1" onChange="redirect(this.options.selectedIndex)" ID="Select1">
<option>EdgesServer状态</option>
<option>Domain状态</option>

<option>国外的著名搜索引擎</option>
</select>
<select name="stage2" size="1" ID="Select2">
<option value="allEdges">全部服务器</option>
<option value="madhouseEdges">亿动服务器</option>
<option value="thirdEdges">第三方服务器</option>
</select>
<input type="button" name="test" value="Go!" onClick="go()" ID="Button1">
</p>
<script>
<!--

var groups=document.doublecombo.example.options.length
var group=new Array(groups)
for (i=0; i<groups; i++)
group[i]=new Array()
group[0][0]=new Option("全部服务器","allEdges")
group[0][1]=new Option("亿动服务器","madhouseEdges")
group[0][2]=new Option("第三方服务器","thirdEdges")
group[1][0]=new Option("CDN域名查询","allDomain")
group[1][1]=new Option("JavaScript2000","http://www.javascript2000.com")
group[1][2]=new Option("无忧脚本","http://www.51js.com")
group[2][0]=new Option("Hotbot","http://www.hotbot.com")
group[2][1]=new Option("Infoseek","http://www.infoseek.com")
group[2][2]=new Option("Excite","http://www.excite.com")
group[2][3]=new Option("Lycos","http://www.lycos.com")
var temp=document.doublecombo.stage2
function redirect(x){
for (m=temp.options.length-1;m>0;m--)
temp.options[m]=null
for (i=0;i<group[x].length;i++){
temp.options[i]=new Option(group[x][i].text,group[x][i].value)
}
temp.options[0].selected=true
}
function go(){
location=temp.options[temp.selectedIndex].value
}
//-->
</script>
</form>
</body>

</html>

@@ layouts/default.html.ep
<!DOCTYPE html>
<html>
<head><title>Login Manager</title></head>
<body><%= content %></body>
</html>
