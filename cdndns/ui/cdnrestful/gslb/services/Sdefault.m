<?php
/*
  +----------------------------------------------------------------------+
  | Name:services/Sdefault.m                                             |
  +----------------------------------------------------------------------+
  | Comment:                                                             |
  +----------------------------------------------------------------------+
  | Author:Odin                                                          |
  +----------------------------------------------------------------------+
  | Created:2012-03-07 18:10:12                                          |
  +----------------------------------------------------------------------+
  | Last-Modified:2012-03-07 19:36:29                                    |
  +----------------------------------------------------------------------+
*/

function _default() {
    $GLOBALS['outputContent']=array(
        'response'=>_getErrDesc($GLOBALS['errCode']),
        'protocolVer' => $GLOBALS['protocolVer'],
        'operation' => $GLOBALS['operation'],
        'serviceName' => $GLOBALS['serviceName'],
        'selector' => $GLOBALS['selector'],
        'rowKey' => $GLOBALS['rowKey'],
    );
}
