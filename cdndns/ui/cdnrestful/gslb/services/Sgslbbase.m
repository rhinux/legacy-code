<?php
/*
  +----------------------------------------------------------------------+
  | Name:                                                                |
  +----------------------------------------------------------------------+
  | Comment: GSLB相关基础数据更新                                                            |
  +----------------------------------------------------------------------+
  | Author:Rhinux                                                         |
  +----------------------------------------------------------------------+
  | Created:2012-03-19 17:46:23                              |
  +----------------------------------------------------------------------+
  | Last-Modified:2012-04-06 14:30:04                        |
  +----------------------------------------------------------------------+
*/
define('_SERV_ROOT', dirname(__FILE__).'/');
include_once(_SERV_ROOT.'../fun/fun.gslb.web.m');
/* {{{ 输入ip地址段返回一128个ip为一段的网络地址,所属CNAME 和EdgesServerIP
 * 
 */
/* $GLOBALS['outputContent']=array(
        'response'=>_getErrDesc($GLOBALS['errCode']),
        'protocolVer' => $GLOBALS['protocolVer'],
        'operation' => $GLOBALS['operation'],
        'serviceName' => $GLOBALS['serviceName'],
        'selector' => $GLOBALS['selector'],
        'rowKey' => $GLOBALS['rowKey'],
    );
 */
function _curlist(){
    $iprange=trim($GLOBALS['rowKey']);
    list($startip_dec,$endip_dec) = explode('-',$iprange);   
    $startip = long2ip($startip_dec);
    $endip = long2ip($endip_dec);
    $GLOBALS['outputContent']['startip'] = $startip;
    $GLOBALS['outputContent']['endip'] = $endip;

    $netarray = splitip2net($startip,$endip);
    foreach($netarray as $key=>$val){
     //   scandb($val);
        $GLOBALS['outputContent']["net".$key] = $val;
    }
/*    foreach($netarray as $key=>$val){
        $nets = $val."|".$nets;
   }
    $GLOBALS['outputContent']['nets'] = $nets;
 */

}
/*}}}*/

/* {{{ 扫描现有数据库中ip起始端的数据
 */
/* }}}*/

?>

