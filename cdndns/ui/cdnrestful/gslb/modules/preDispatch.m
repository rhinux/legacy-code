<?php
/*
  +----------------------------------------------------------------------+
  | Name:modules/preDispatch.m                                           |
  +----------------------------------------------------------------------+
  | Comment:控制器                                                       |
  +----------------------------------------------------------------------+
  | Author:Odin                                                          |
  +----------------------------------------------------------------------+
  | Created:2011-02-23 10:19:45                                          |
  +----------------------------------------------------------------------+
  | Last-Modified:2012-03-21 16:34:54                                    |
  +----------------------------------------------------------------------+
*/
$moduleName=basename(__FILE__);

/* 读取uri信息 */
$GLOBALS['urlInfo']=parse_url($_SERVER['REQUEST_URI']);
_debug("[{$GLOBALS['urlInfo']['path']}][start---------]",_DLV_NOTICE);
$arrPathInfo=pathinfo($GLOBALS['urlInfo']['path']);
$restQs=explode('/',$arrPathInfo['dirname']);
$restQs[]=$arrPathInfo['filename']; //将后缀名去掉

/* {{{ 获取版本
 */
$GLOBALS['protocolVer']=$restQs[1];
array_shift($restQs);
/* }}} */

/* {{{获取操作符
 */
$GLOBALS['operation']=$restQs[1];
array_shift($restQs);
/* }}} */

//获取service名称
$GLOBALS['serviceName']=strtolower($restQs[1]);
array_shift($restQs);

// selector
if ($restQs[1][0]==_SELECTOR_TAG) {    //只有以'@'开头的才是selector
    $GLOBALS['selector']=strtolower($restQs[1]);
    array_shift($restQs);
}

//rowkey
$GLOBALS['rowKey']=empty($restQs[1])?null:$restQs[1];

do {
    if (empty($GLOBALS['serviceName'])) {
        _debug("[none_service]",_DLV_CRIT);
        $GLOBALS['errCode']=_ERROR_BADREQUEST;
        break;
    }

    /* {{{ 这里判断签名
     */
    if ($GLOBALS['checkSign']===true && _ERROR_OK!==($GLOBALS['errCode']=_verifySign())) {
        _debug("[sign_error]",_DLV_ERROR);
        break;
    }
    /* }}} */

    $serviceFile=_API_ROOT."services/S{$GLOBALS['serviceName']}.m";   //在services目录下,以S开头的文件
    if (file_exists($serviceFile) && @include_once($serviceFile)) {
        _debug("[{$serviceFile}][included]",_DLV_NOTICE);
        /* {{{ 正确的请求,这时可以开始连接model
         */
        /* }}} */
    } else {
        //视为错误的请求
        $GLOBALS['errCode']=_ERROR_BADREQUEST;
        $defaultFile=_API_ROOT."services/Sdefault.m";
        _debug("[default:{$defaultFile}][invalid_service]",_DLV_CRIT);
        @include_once($defaultFile);
    }
} while(false);

