<?php
/*
  +----------------------------------------------------------------------+
  | Name:modules/initApi.m                                               |
  +----------------------------------------------------------------------+
  | Comment:初始化api,确定配置                                           |
  +----------------------------------------------------------------------+
  | Author:Odin                                                          |
  +----------------------------------------------------------------------+
  | Created:2011-02-23 10:44:39                                          |
  +----------------------------------------------------------------------+
  | Last-Modified:2012-03-21 16:31:05                                    |
  +----------------------------------------------------------------------+
*/
$moduleName=basename(__FILE__);

/* {{{ 初始化全局变量
 */
$GLOBALS['errCode']=_ERROR_OK; //默认即为成功
//输出内容,是个数组
$GLOBALS['outputContent']=array();
//输出headers,也是个数组
$GLOBALS['outputHeaders']=array();
//time info
list($usec,$GLOBALS['currentTime'])=explode(" ",microtime());
$GLOBALS['firstStamp']=(float)$usec+(float)$GLOBALS['currentTime'];    //请求开始时间
$GLOBALS['lastStamp']=$GLOBALS['firstStamp'];
$GLOBALS['requestDuration']=0;
$GLOBALS['descTime']=2000000000-$GLOBALS['currentTime']; //时间逆序(越大的时间戳排在越前面)
$GLOBALS['apiTimtout']=600; //时间误差10分钟之内的比较安全
/* }}} */

/* {{{ base configuation
 * 这里定义最基础的配置项目,全部要在hiphop配置文件中定义,并且都有默认值
 * 约定所有在hiphop中定义的配置都小写字母,并且用下划线'_'分隔
 * 如果有额外需要定义的配置,也在这里指定(文件)
 */
$GLOBALS['logTag']=isset($_SERVER['log_tag'])?$_SERVER['log_tag']:'mREST';
//debug 级别,优先级最高是通过参数传入的,这个纯粹为了方便debug
$GLOBALS['debugLevel']=empty($_GET['debug'])?(isset($_SERVER['debug_level'])?_getDebugLevel($_SERVER['debug_level']):_DLV_NONE):_getDebugLevel($_GET['debug']);
//debug_log,是一个字符串,形如facility.priority
$GLOBALS['debugLogStr']=isset($_SERVER['debug_log'])?$_SERVER['debug_log']:'local2.debug';
// 注册debug的syslog信息,$GLOBALS['sysLog']['_debug']
_syslogRegister($GLOBALS['debugLogStr'],'_debug');
//这个只能在参数中传
$GLOBALS['debugOutput']=(isset($_GET['debug_output']) && $_GET['debug_output']==='1')?true:false;
//log tag
//自定义的配置文档, 必须为ini格式,存在$GLOBALS['CONFIG']
$GLOBALS['configFile']=isset($_SERVER['config_file'])?$_SERVER['config_file']:'';
if (!empty($GLOBALS['configFile']) && file_exists($GLOBALS['configFile'])) {
    $GLOBALS['CONFIG']=@parse_ini_file($GLOBALS['configFile'],true);
}
//是否判断签名
$GLOBALS['checkSign']=(isset($_SERVER['check_sign']) && strtolower($_SERVER['check_sign'])==='true')?true:false;
/* }}} */

unset($moduleName);
