<?php
/*
  +----------------------------------------------------------------------+
  | Name:modules/parseRequest.m                                          |
  +----------------------------------------------------------------------+
  | Comment:处理访问信息                                                 |
  +----------------------------------------------------------------------+
  | Author:Odin                                                          |
  +----------------------------------------------------------------------+
  | Created:2011-02-23 10:19:45                                          |
  +----------------------------------------------------------------------+
  | Last-Modified:2012-03-06 19:18:35                                    |
  +----------------------------------------------------------------------+
*/
$moduleName=basename(__FILE__);

//filter
$GLOBALS['filterFields']=isset($_GET['fields'])?explode(',',$_GET['fields']):array();    //用户自定义过滤字段
$GLOBALS['filterStart']=isset($_GET['start'])?$_GET['start']:null;  //起始id
$GLOBALS['filterCount']=isset($_GET['count'])?(int)$_GET['count']:null; //最大数字

//获取post数据
$GLOBALS['postData']=file_get_contents("php://input");
