<?php
/*
  +----------------------------------------------------------------------+
  | Name:fun/base.m                                                      |
  +----------------------------------------------------------------------+
  | Comment:基础函数,函数名全部以_开头                                   |
  +----------------------------------------------------------------------+
  | Author:Odin                                                          |
  +----------------------------------------------------------------------+
  | Created:2011-02-23 10:24:26                                          |
  +----------------------------------------------------------------------+
  | Last-Modified:2012-03-22 15:14:25                                    |
  +----------------------------------------------------------------------+
*/

/* {{{ 获取debug级别
 * @param string $lvStr debug字符串
 */
function _getDebugLevel($lvStr='') {
    if (is_numeric($lvStr)) {   //是数字直接返回数字
        return (int)$lvStr;
    } elseif ($lv=constant('_DLV_'.strtoupper($lvStr))) {
        return $lv;
    } else {
        return _DLV_NONE;
    }
}
/* }}} */

/* {{{ 日志注册器
 * 将日志配置存入$GLOBALS['sysLog']中,可按照需要增加键指,默认_debug不能占用
 */
function _syslogRegister($logSettingStr,$logRKey=null,$logTag=null) {
    $logRKey=empty($logRKey)?'_debug':$logRKey; //下划线开头,避免冲突
    $logTag=empty($logTag)?$GLOBALS['logTag']:$logTag;
    $GLOBALS['sysLog'][$logRKey]['tag']=$logTag;
    list($facStr,$priStr)=explode('.',$logSettingStr);
    if ($facility=constant('_FACILITY_'.strtoupper($facStr))) {
        $GLOBALS['sysLog'][$logRKey]['facility']=$facility;
    } else {
        $GLOBALS['sysLog'][$logRKey]['facility']=_FACILITY_DEFAULT;
    }
    if ($priority=constant('_PRIORITY_'.strtoupper($priStr))) {
        $GLOBALS['sysLog'][$logRKey]['priority']=$priority;
    } else {
        $GLOBALS['sysLog'][$logRKey]['priority']=_PRIORITY_DEFAULT;
    }
    return true;
}
/* }}} */

/* {{{ debug描述
 * @param $lv int debug级别
 */
function _getDebugDesc($lv) {
    switch($lv) {
    case _DLV_INFO:
        $ret='INFO';
        break;
    case _DLV_NOTICE:
        $ret='NOTICE';
        break;
    case _DLV_WARNING:
        $ret='WARNING';
        break;
    case _DLV_ERROR:
        $ret='ERROR';
        break;
    case _DLV_CRIT:
        $ret='CRIT';
        break;
    case _DLV_ALERT:
        $ret='ALERT';
        break;
    case _DLV_EMERG:
        $ret='EMERG';
        break;
    case _DLV_NONE:
    default:
        $ret='NONE';
        break;
    }
    return $ret;
}
/* }}} */

/* {{{ 记录系统日志
 * @param   string  $data       log内容
 * @param   string  $tag        log标识
 * @param   int     $facility   log分类(与/etc/syslog.conf对应)
 * @param   int     $priority   log级别(与/etc/syslog.conf对应)  
 *
 * @return  null
 */
function _saveSysLog($data,$logRKey) {
    do {
        if (empty($data)) {
            _debug("[".__FUNCTION__."][no_data]",_DLV_CRIT);
            break;
        }
        if (false==($logSetting=$GLOBALS['sysLog'][$logRKey])) {
            _debug("[".__FUNCTION__."][logsetting_invalid]",_DLV_CRIT);
            break;
        }
        if (empty($logSetting['tag']) || empty($logSetting['facility']) || empty($logSetting['priority'])) {
            _debug("[".__FUNCTION__."][logsetting_miss]",_DLV_CRIT);
            break;
        }
        openlog($logSetting['tag'],LOG_PID,$logSetting['facility']);
        syslog($logSetting['priority'],$data);
        closelog();
    } while(false);
}
/* }}} */

/* {{{ debug函数
 * @param   string  $debugData      debug内容
 * @param   int     $debugLevel     debug级别
 *
 * @return  null
 */
function _debug($debugData,$debugLevel) {
    if ($debugLevel>=$GLOBALS['debugLevel'] && !empty($debugData)) {
        $lvDesc=_getDebugDesc($debugLevel);
        /* {{{ data prefix
         */
        $dataPrefix='';
        if (!empty($GLOBALS['operation'])) {
            $dataPrefix.="[{$GLOBALS['operation']}]";
        }
        if (!empty($GLOBALS['serviceName'])) {
            $dataPrefix.="[{$GLOBALS['serviceName']}]";
        }
        if (!empty($GLOBALS['selector'])) {
            $dataPrefix.="[{$GLOBALS['selector']}]";
        }
        if (!empty($GLOBALS['rowKey'])) {
            $dataPrefix.="[{$GLOBALS['rowKey']}]";
        }
        if (!empty($dataPrefix)) {
            $dataPrefix.=' ';
        }

        $dataSuffix=' ';
        if (!empty($GLOBALS['moduleName'])) {
            $dataSuffix.="[{$GLOBALS['moduleName']}]";
        }
        $dataSuffix.="["._getErrDesc($GLOBALS['errCode'])."({$GLOBALS['errCode']})]";
        if (!empty($GLOBALS['serviceSign'])) {
            $dataSuffix.="[{$GLOBALS['serviceSign']}]";
        }
        $dataSuffix.="["._procSpeed()."][$lvDesc]";

        $debugData=$dataPrefix.$debugData.$dataSuffix;
        /* }}} */
        if ($GLOBALS['debugOutput']) {
            printf("[%s]%s<br />\n",date('Y-m-d H:i:s'),$debugData);
        }
        _saveSysLog($debugData,'_debug');
    }
}
/* }}} */

/* {{{ 错误描述
 */
function _getErrDesc($errCode=false) {
    switch($errCode) {
    case _ERROR_OK:
        $ret='OK';
        break;
    case _ERROR_NOKEY:
        $ret='NoKey';
        break;
    case _ERROR_CONFILICT:
        $ret='Confilict';
        break;
    case _ERROR_MODEL:
        $ret='Data';
        break;
    case _ERROR_NOPROPERTY:
        $ret='NoProperty';
        break;
    case _ERROR_NOTEXISTS:
        $ret='NotExists';
        break;
    case _ERROR_FAILED:
        $ret='Failed';
        break;
    case _ERROR_ILLEGALSELECTOR:
        $ret='IllegalSelector';
        break;
    case _ERROR_BADREQUEST:
        $ret='BadRequest';
        break;
    case _ERROR_SIGN:
        $ret='Sign';
        break;
    default:
        $ret='Unknown';
        break;
    }
    return $ret;
}
/* }}} */

/* {{{ 错误处理函数
 */
function _checkError($errCode=false) {
    $ret=false;
    switch($errCode) {
    case _ERROR_OK:
        $ret=true;
        break;
    case _ERROR_NOKEY:
    case _ERROR_CONFILICT:
    case _ERROR_MODEL:
    case _ERROR_NOPROPERTY:
    case _ERROR_NOTEXISTS:
    case _ERROR_FAILED:
    case _ERROR_ILLEGALSELECTOR:
    case _ERROR_BADREQUEST:
    case _ERROR_SIGN:
    default:
        break;
    }
    _debug("[".__FUNCTION__."]["._getErrDesc($errCode)."]",_DLV_NOTICE);
    return $ret;
}
/* }}} */

/* {{{ api执行函数
 */
function _apiRun() {
    //运行程序块
    $errCode=$GLOBALS['errCode'];
    if (_checkError($errCode)) {  //目前位置没错，才有继续的价值
        $funcName='_'.strtolower($GLOBALS['operation']);
        if (function_exists($funcName)) {
            _debug("[".__FUNCTION__."][funcName:$funcName][call_it]",2);
            try {
                //$funcName();
                $errCode=call_user_func($funcName);  // 比$funcName()稍慢,但是可读性高
            } catch (Exception $e) {
                $errCode=_ERROR_FAILED;
                _debug("[".__FUNCTION__."][funcName:$funcName][Caught Exception:".$e->getMessage()."]",2);
            }
        } else {
            _debug("[".__FUNCTION__."][funcName:$funcName][function_not_exists]",2);
            $errCode=_ERROR_BADREQUEST;
        }
    } else {
        $funcName='_default';
        _debug("[".__FUNCTION__."][call_default][get_error]",_DLV_CRIT);
        try {
            call_user_func($funcName);  // 比$funcName()稍慢,但是可读性高
        } catch (Exception $e) {
            _debug("[".__FUNCTION__."][funcName:$funcName][Caught Exception:".$e->getMessage()."]",_DLV_CRIT);
        }
    }
    //输出结果
    _outputResults($errCode);
}
/* }}} */

/* {{{ output函数,相当于view
 */
function _outputResults($errCode,$encode='json') {

    //返回http状态码,以及自定义header
    _responseHeader($errCode);

    if (!empty($GLOBALS['outputContent'])) {  //result应该以一个数组传入
        if (is_array($GLOBALS['outputContent'])) {
            //数组就转换成json或者xml
            switch($encode) {   //暂时只支持json
            case 'json':
            default:
                header('Content-Type: application/json');
                $outputContent=json_encode($GLOBALS['outputContent']);
                break;
            }
        } else {
            //直接输出
            $outputContent=$GLOBALS['outputContent'];
        }
        echo $outputContent;
    }
}
/* }}} */

/* {{{ _getHttpStatus
 */
function _getHttpStatus($errCode=0) {
    switch($errCode) {
    case _ERROR_OK:
        switch($GLOBALS['operation']) {
        case _OPERATION_READ:
            $httpStatus=_HTTPSTATUS_OK;
            break;
        case _OPERATION_CREATE:
            $httpStatus=_HTTPSTATUS_CREATED;
            break;
        case _OPERATION_UPDATE:
            $httpStatus=_HTTPSTATUS_RESET_CONTENT;
            break;
        case _OPERATION_DELETE:
            $httpStatus=_HTTPSTATUS_NO_CONTENT;
            break;
        default:
            $httpStatus=_HTTPSTATUS_METHOD_NOT_ALLOWED;
            break;
        }
        break;
    case _ERROR_NOKEY:
        $httpStatus=_HTTPSTATUS_BAD_REQUEST;
        break;
    case _ERROR_CONFILICT:
        $httpStatus=_HTTPSTATUS_METHOD_CONFILICT;
        break;
    case _ERROR_MODEL:
        $httpStatus=_HTTPSTATUS_INTERNAL_SERVER_ERROR;
        break;
    case _ERROR_NOPROPERTY:
        $httpStatus=_HTTPSTATUS_BAD_REQUEST;
        break;
    case _ERROR_NOTEXISTS:
        $httpStatus=_HTTPSTATUS_NOT_FOUND;
        break;
    case _ERROR_FAILED:
        $httpStatus=_HTTPSTATUS_INTERNAL_SERVER_ERROR;
        break;
    case _ERROR_ILLEGALSELECTOR:
        $httpStatus=_HTTPSTATUS_BAD_REQUEST;
        break;
    case _ERROR_BADREQUEST:
        $httpStatus=_HTTPSTATUS_BAD_REQUEST;
        break;
    case _ERROR_SIGN:
        $httpStatus=_HTTPSTATUS_FORBIDDEN;
        break;
    default:
        $httpStatus=_HTTPSTATUS_BAD_REQUEST;
        break;
    }
    _debug("[".__FUNCTION__."] [errCode:$errCode][httpStatus:$httpStatus]",3);
    return $httpStatus;
}
/* }}} */

/* {{{ response header
 */
function _responseHeader($errCode=0) {
    $httpStatus=_getHttpStatus($errCode);
    switch($httpStatus) {
    case _HTTPSTATUS_OK:
        $headerLine="HTTP/1.1 200 OK";
        break;
    case _HTTPSTATUS_CREATED:
        $headerLine="HTTP/1.1 201 Created";
        break;
    case _HTTPSTATUS_NO_CONTENT:
        $headerLine="HTTP/1.1 204 No Content";
        break;
    case _HTTPSTATUS_RESET_CONTENT:
        $headerLine="HTTP/1.1 205 Reset Content";
        break;
    case _HTTPSTATUS_BAD_REQUEST:
        $headerLine="HTTP/1.1 400 Bad Request";
        break;
    case _HTTPSTATUS_UNAUTHORIZED:
        $headerLine="HTTP/1.1 401 Unauthorized";
        break;
    case _HTTPSTATUS_FORBIDDEN:
        $headerLine="HTTP/1.1 403 Forbidden";
        break;
    case _HTTPSTATUS_NOT_FOUND:
        $headerLine="HTTP/1.1 404 Not Found";
        break;
    case _HTTPSTATUS_METHOD_NOT_ALLOWED:
        $headerLine="HTTP/1.1 405 Method Not Allowed";
        break;
    case _HTTPSTATUS_METHOD_CONFILICT:
        $headerLine="HTTP/1.1 409 Conflict";
        break;
    case _HTTPSTATUS_INTERNAL_SERVER_ERROR:
        $headerLine="HTTP/1.1 500 Internal Server Error";
        break;
    default:
        $headerLine="HTTP/1.1 400 Bad Request";
    }
    _debug("[".__FUNCTION__."] [httpStatus:$httpStatus][outputLine:$headerLine]",2);
    header($headerLine);
    /* {{{ 这里加上自定义的header
     */
    if (!empty($GLOBALS['outputHeaders'])) {
        foreach ($GLOBALS['outputHeaders'] as $hKey=>$hValue) {
            _debug("[".__FUNCTION__."] [custom_header:$hKey][value:$hValue]",3);
            header("$hKey: $hValue");
        }
    }
    /* }}} */
}
/* }}} */

/* {{{ 精确时间
 */
function _microtimeFloat() {
    list($usec,$sec)=explode(" ",microtime());
    return ((float)$usec+(float)$sec);
}
/* }}} */

/* {{{ 程序速度
 */
function _procSpeed() {
    $cur_stamp=_microtimeFloat();
    $GLOBALS['requestDuration']=round($cur_stamp-$GLOBALS['firstStamp'],6);
    $GLOBALS['requestTip']=round($cur_stamp-$GLOBALS['lastStamp'],6);
    $GLOBALS['lastStamp']=$cur_stamp;
    return "{$GLOBALS['requestTip']}/{$GLOBALS['requestDuration']}";
}
/* }}} */

/* {{{ 签名检查
 */
function _verifySign() {
    $httpVerb=$_SERVER['REQUEST_METHOD'];
    $date=trim($_SERVER['HTTP_DATE']);
    $requestTS=strtotime($date);

    $ret=_ERROR_OK;
    do {
        if (abs($GLOBALS['currentTime']-$requestTS)>=$GLOBALS['apiTimtout']) {
            $ret=_ERROR_TIME;
            break;
        }
    } while(false);
    return $ret;
}
/* }}} */
