<?php
/*
  +----------------------------------------------------------------------+
  | Name:                                                                |
  +----------------------------------------------------------------------+
  | Comment:                                                             |
  +----------------------------------------------------------------------+
  | Author:Rhinux                                                        |
  +----------------------------------------------------------------------+
  | Created:2011-09-21 17:10:26                              |
  +----------------------------------------------------------------------+
  | Last-Modified:2012-04-09 10:31:13                        |
  +----------------------------------------------------------------------+
 */

/* {{{ 检查是不是合法的IP地址
*@matchip
*@return is ip address
*/

function matchip($ip){
    list($ip1,$ip2,$ip3,$ip4) = explode('.',$ip);
    if(!(isset($ip1,$ip2,$ip3,$ip4))){
        return 0;
    }
    if(!((0 <=$ip1) and ($ip1<=255))){
        echo "ip1:$ip1\n";
        return 0;
    }elseif (!((0 <=$ip2) and ($ip2<=255))){
        return 0;
    }elseif (!((0 <=$ip3) and ($ip3<=255))){
        return 0;
    }elseif (!((0 <=$ip4) and ($ip4<=255))){ 
        return 0;
    }else{
        return 1;
    }
}
/* }}} */

/* {{{ 检查结束IP是否大于开始IP
 *@endipbig
 *@return endipbig
 *
 */
function endipnotsmall($ipstart,$ipend){
    if(ip2long($ipstart) > ip2long($ipend)){
        return 0;
    }else {
        return 1;
    }
}
/* }}}*/
 
/* {{{ ip和bin转换操作
 */
function decextbin($dec, $bits = null){
    $bin = decbin($dec);
    if(!is_null($bits) && strlen($bin) > $bits)  throw new LengthException('The resulting binary is more than '.$bits.' bits.');
    else while(strlen($bin) < $bits) $bin = '0'.$bin;
    return $bin; //done
}
function ip2bin($ip){
    return decextbin(ip2long($ip),32);
}
 
function bin2ip($bin){
    return long2ip(bindec($bin));
}
/* }}}*/

/* {{{ 给出起址IP地址 根据子网掩码返回网络地址列表
 *@brief splitip2net 给出起址IP地址 根据子网掩码返回网络地址列表
 *@param $ipstart 起始IP 
 *@param $ipend   结束IP
 *@param $netmask 网络掩码
 *@return $netarray 返回网络地址列表
 */

function splitip2net($ipstart,$ipend,$netmask = '255.255.255.128'){
    $notnetmask_dec = ip2long($netmask) ^ ip2long('255.255.255.255');
    $startnet_dec = ip2long($ipstart) & ip2long($netmask);
    $endnet_dec = ip2long($ipend) & ip2long($netmask);
    if(ip2long($ipstart) > ip2long($ipend)){
         $GLOBALS['outputContent']['errinfo'] = $GLOBALS['outputContent']['errinfo']."Maybe you gave wrong ip range :( ";
        return 0;
    }
    do{
        $netarray[] = long2ip($startnet_dec);
        $startnet_dec = $startnet_dec + $notnetmask_dec + 1;
    }while($endnet_dec >= $startnet_dec );
    return $netarray;

}
/*}}}*/

/* {{{ 根据网络地址去查找数据库后返回该网络地址的信息(运营商，地域，CNAME，对于IP列表等)
 *@brief info4net  据网络地址去查找数据库后返回该网络地址的信息(运营商，地域，CNAME，对于IP列表等)
 *@param $netarray 网络地址
 *@return $netinfo 返回网络地址以及对应的运营商，地域，CNAME
 */

?>

