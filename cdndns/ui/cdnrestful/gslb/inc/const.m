<?php
/*
  +----------------------------------------------------------------------+
  | Name:inc/const.m                                                     |
  +----------------------------------------------------------------------+
  | Comment:RESTful API base const                                       |
  +----------------------------------------------------------------------+
  | Author:Odin                                                          |
  +----------------------------------------------------------------------+
  | Created:2011-02-22 10:30:48                                          |
  +----------------------------------------------------------------------+
  | Last-Modified:2012-03-21 16:29:53                                    |
  +----------------------------------------------------------------------+
*/

/* {{{ debug 错误级别
 */
define('_DLV_INFO',    1);
define('_DLV_NOTICE',  2);
define('_DLV_WARNING', 3);
define('_DLV_ERROR',   4);
define('_DLV_CRIT',    5);
define('_DLV_ALERT',   6);
define('_DLV_EMERG',   7);
define('_DLV_NONE',    8);
/* }}} */

/* {{{ operations
 */
define('_OPERATION_CREATE', 'create');
define('_OPERATION_READ',   'get');
define('_OPERATION_UPDATE', 'update');
define('_OPERATION_DELETE', 'delete');
define('_SELECTOR_TAG', '@');
/* }}} */


/* {{{ http status define
 */
define('_HTTPSTATUS_OK',                    200);
define('_HTTPSTATUS_CREATED',               201);
define('_HTTPSTATUS_NO_CONTENT',            204);
define('_HTTPSTATUS_RESET_CONTENT',         205);
define('_HTTPSTATUS_BAD_REQUEST',           400);
define('_HTTPSTATUS_UNAUTHORIZED',          401);
define('_HTTPSTATUS_FORBIDDEN',             403);
define('_HTTPSTATUS_NOT_FOUND',             404);
define('_HTTPSTATUS_METHOD_NOT_ALLOWED',    405);
define('_HTTPSTATUS_METHOD_CONFILICT',      409);
define('_HTTPSTATUS_INTERNAL_SERVER_ERROR', 500);
/* }}} */

/* {{{ errCodes
 */
define('_ERROR_OK',              0);    //成功
define('_ERROR_NOKEY',           1);    //无rowkey
define('_ERROR_CONFILICT',       2);    //产生冲突
define('_ERROR_MODEL',           3);    //model错误
define('_ERROR_NOPROPERTY',      4);    //缺少关键属性
define('_ERROR_NOTEXISTS',       6);    //不存在
define('_ERROR_FAILED',          7);    //失败
define('_ERROR_ILLEGALSELECTOR', 8);    //seletor 非法
define('_ERROR_PARAMERROR',      9);   //参数错误
define('_ERROR_BADREQUEST',      10);   //参数错误
define('_ERROR_SIGN',            11);   //签名错误
define('_ERROR_TIME',            12);   //时间错误
/* }}} */

/* {{{ syslog
 */
//facility
define('_FACILITY_AUTH',     LOG_AUTH);
define('_FACILITY_AUTHPRIV', LOG_AUTHPRIV);
define('_FACILITY_CRON',     LOG_CRON);
define('_FACILITY_DAEMON',   LOG_DAEMON);
define('_FACILITY_KERN',     LOG_KERN);
define('_FACILITY_LOCAL0',   LOG_LOCAL0);
define('_FACILITY_LOCAL1',   LOG_LOCAL1);
define('_FACILITY_LOCAL2',   LOG_LOCAL2);
define('_FACILITY_LOCAL3',   LOG_LOCAL3);
define('_FACILITY_LOCAL4',   LOG_LOCAL4);
define('_FACILITY_LOCAL5',   LOG_LOCAL5);
define('_FACILITY_LOCAL6',   LOG_LOCAL6);
define('_FACILITY_LOCAL7',   LOG_LOCAL7);
define('_FACILITY_LPR',      LOG_LPR);
define('_FACILITY_MAIL',     LOG_MAIL);
define('_FACILITY_NEWS',     LOG_NEWS);
define('_FACILITY_SYSLOG',   LOG_SYSLOG);
define('_FACILITY_USER',     LOG_USER);
define('_FACILITY_UUCP',     LOG_UUCP);
define('_FACILITY_DEFAULT',  LOG_LOCAL1);
//priority 
define('_PRIORITY_EMERG',    LOG_EMERG);
define('_PRIORITY_ALERT',    LOG_ALERT);
define('_PRIORITY_CRIT',     LOG_CRIT);
define('_PRIORITY_ERR',      LOG_ERR);
define('_PRIORITY_WARNING',  LOG_WARNING);
define('_PRIORITY_NOTICE',   LOG_NOTICE);
define('_PRIORITY_INFO',     LOG_INFO);
define('_PRIORITY_DEBUG',    LOG_DEBUG);
define('_PRIORITY_DEFAULT',  LOG_DEBUG);
/* }}} */
