<?php
#Author:wangtao modify 2012 7 10 email:wanghaofu@163.com
abstract class lib_server {
	var $param;
	var $query;
	var $result;
	var $url;
	var $format = 'json';
	var $secret = '';
	protected $method = '';
	var $baseUrl;
	const PUT = 'update'; // put
	const GET = 'get';
	const POST = 'insert';
	const DELETE = 'delete';
	protected function __construct($baseUrl) {
		$this->baseUrl = $baseUrl;
	}
	protected function setUrl() {
		$this->url = $this->baseUrl . "rest/$this->method/dns";
	}
	protected function setMethod($method) {
		$this->method = $method;
	}
}
class lib_restful extends lib_server {
	const VENDOR = '';
	const APP_NAME = '';
	var $decode = false;
	const CALL_BACK_URL = ''; // callbackkey
	function __construct($app_id = '', $api_key = '', $secret = '', $url = '') {
	}
	protected function exec() {
		$query = $this->restBuildQuery ( $this->param, $this->secret );
		$this->setUrl ();
		$result = $this->postRequest ( $this->url, $query );
		if ($this->decode) {
			$result = json_decode ( $result );
			$result = self::object_to_array ( $result );
		}
		return $result;
	}
	public function setDecode($decode = true) {
		$this->decode = $decode;
	}
	/**
	 * 对象转数组
	 * 
	 * @param object $obj        	
	 * @return array
	 */
	public static function object_to_array($obj) {
		if (empty ( $obj )) {
			throw new Exception ( "data is null", 400 );
		}
		$_arr = is_object ( $obj ) ? get_object_vars ( $obj ) : $obj;
		foreach ( $_arr as $key => $val ) {
			$val = (is_array ( $val ) || is_object ( $val )) ? self::object_to_array ( $val ) : $val;
			$arr [$key] = $val;
		}
		return $arr;
	}
	/**
	 * reset 直接调用
	 * 
	 * @param array $param        	
	 * @param string $secret        	
	 * @return string
	 */
	private function restBuildQuery($param, $secret) {
		if (0) {
			$param ['api_key'] = $this->api_key;
			$param ['session_key'] = $this->session_key;
			$param ['format'] = $this->format;
			$param ['v'] = '0.8';
			$sig = $this->makeSig ( $param, $secret );
			$param ['sig'] = $sig;
		} else {
			$param ['call_id'] = microtime ( true );
		}
		if (0) {
			$query = http_build_query ( $param );
		} else {
			$query = json_encode ( $param );
		}
		return $query;
	}
	private function verifyBuildQuery($param) {
		$query = http_build_query ( $param );
		return $query;
	}
	private function verifyMakeSig($param) {
		ksort ( $param );
		$request_str = '';
		foreach ( $param as $key => $value ) {
			if (empty ( $value ))
				continue;
			if (empty ( $request_str )) {
				$request_str .= $key . '=' . $value;
			} else {
				$request_str .= '&' . $key . '=' . $value;
			}
		}
		$sig = $request_str . '&' . $this->secret;
		$sig = md5 ( $sig );
		return $sig;
	}
	private function makeSig($param, $secret = '') {
		ksort ( $param );
		$request_str = '';
		foreach ( $param as $key => $value ) {
			$request_str .= $key . '=' . $value;
		}
		$sig = $request_str . $secret;
		$sig = md5 ( $sig );
		return $sig;
	}
	private function postRequest($url, $post_string) {
		$useragent = 'MadHouse  API PHP5 Client 0.8 (curl) ' . phpversion ();
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		if (strlen ( $post_string ) >= 3) {
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_string );
		}
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_USERAGENT, $useragent );
		curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
		curl_setopt ( $ch, CURLOPT_TIMEOUT, 30 );
		$result = curl_exec ( $ch );
		curl_close ( $ch );
		return $result;
	}
	private function url_base64_encode($str) {
		return $str;
		$search = array (
				'+',
				'/' 
		);
		$replace = array (
				'*',
				'-' 
		);
		$basestr = base64_encode ( $str );
		return str_replace ( $search, $replace, $basestr );
	}
	private function callBackSigCheck() {
		$queryStr = $_SERVER ['QUERY_STRING'];
		$queryStr = explode ( '&sig=', $queryStr );
		$localsig = md5 ( $queryStr [0] . $secret );
	}
}
class iClient extends lib_restful {
	static $data;
	public function __construct($ip) {
		lib_server::__construct ( $ip );
	}
	/**
	 *
	 * @param string $ip
	 *        	example: '222.73.254.111'
	 */
	public function getRecordByIp($ip) {
		
		$this->ip = $ip;
		$this->actionEvent = 'get';
		$this->setMethod ( self::GET );
		$this->params = self::getData ();
		return $this->exec ();
	}
	/**
	 *
	 * @param string $ip
	 *        	example '222.73.254.112'
	 * @param string $cname
	 *        	example 'beijingcm.mdn.ifree.cn'
	 * @param array $activeIp
	 *        	/* example array ('222.73.254.112','222.73.254.113');
	 */
	public function down($ip, $activeIp, $cname = '') {
		$this->ip = $ip;
		$this->activeIp = $activeIp;
		$this->canme = $cname;
		$this->actionEvent = 'down';
		$this->setMethod ( self::PUT );
		$this->param = self::getData ();
		return $this->exec ();
	}
	public function up($ip) {
		$this->ip = $ip;
		$this->actionEvent = 'up';
		$this->setMethod ( self::PUT );
		$this->param = self::getData ();
		return $this->exec ();
	}
	public function get($ip = '') {
		$this->ip = $ip;
		$this->setMethod ( self::GET );
		$this->param = self::getData ();
		return $this->exec ();
	}
	public function delete($ip = '') {
		$this->ip = $ip;
		$this->setMethod ( self::DELETE );
		$this->param = self::getData ();
		return $this->exec ();
	}
	private static function getData() {
		return self::$data;
	}
	public function __set($property, $value) {
		self::$data [$property] = $value;
	}
}
?>
