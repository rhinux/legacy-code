<?php
/*
  +----------------------------------------------------------------------+
  | Name:                                                                |
  +----------------------------------------------------------------------+
  | Comment:pdns更新相关函数 本函数库依赖 fun.db.m fun.pdnsclient.m      |
  +----------------------------------------------------------------------+
  | Author:xuyunfeng
  +----------------------------------------------------------------------+
  | Created:2012-07-04 19:59:12                              |
  +----------------------------------------------------------------------+
  | Last-Modified:2012-07-26 16:09:06                        |
  +----------------------------------------------------------------------+
 */

/*{{{
 * @param $pdns_url 类似于http://172.16.30.186:8181/
 * @param $send_value:list($method|$rowkey|$bak_iplist|$cname)
 * @param $retry  如果返回不是200 重试次数
 * @param $interval 重试间隔
 * @return  如果http返回200 将返回的data数组化后返回
 */
function update_send($pdns_url,$send_value,$retry=5,$interval=1){
    $funname="update_send";
    $pdns_client = new iClient($pdns_url);
    list($method,$rowkey,$bak_iplist,$cname)=explode('|',$send_value);
    $count=0;
    $pdns_return_code=0;
    do{
        if($method=='down'){
            $arr_bakip = explode(',',$bak_iplist);
            $pdns_result = $pdns_client->down($rowkey,$arr_bakip);
            $arr_result=json_decode($pdns_result,true);
            $pdns_return_code=$arr_result['status']['code'];
            if($pdns_return_code == 200){
                _debug("[fun:$funname]-[method:$method]-[pdns_return_code: 200 ]-[pdns_url:$pdns_url]-[send:$rowkey|$bak_iplist]-[retry $count times]",_DLV_WARNING);
                return $arr_result;
            }else {
                _debug("[fun:$funname]-[IN method:$method]-[pdns_return_code: $pdns_return_code ]",_DLV_WARNING);
            }
        }elseif($method=='up'){
            $pdns_result = $pdns_client->up($rowkey);
            $arr_result = json_decode($pdns_result,true);
            $pdns_return_code=$arr_result['status']['code'];
            if($pdns_return_code == 200){
                _debug("[fun:$funname]-[method:$method]-[pdns_return_code: 200 ]-[pdns_url:$pdns_url]-[send:$rowkey]-[retry $count times]",_DLV_WARNING);
                return  $arr_result;
            }
        }elseif($method=='delete'){
            $pdns_result=$pdns_client->delete($rowkey);
            $arr_result = json_decode($pdns_result,true);
            $pdns_return_code=$arr_result['status']['code'];
            if($pdns_delete_code == 200){
                _debug("[fun:$funname]-[method:$method clear $pdns_name resume record ]-[pdns_return_code: 200 ]-[pdns_url:$pdns_url]-[send:$rowkey]-[retry $count times]",_DLV_WARNING);
                return $arr_result;
            }
        }else{
            _debug("[fun:$funname]-[method can not avaliable]-[pdns_url:$pdns_url-send_value:$send_value-retry $count times]",_DLV_WARNING);
            return 0;
        }
        _debug("[fun:$funname]-[can not get expectation from  pdns return code $pdns_return_code]-[pdns_url:$pdns_url-send_value:$send_value-retry $count times]",_DLV_WARNING);
        $count++;
        sleep($interval);
    }while((!($pdns_delete_code == 200)) && ($count<=$retry));
    return $arr_result;
}
/* }}} */

/* {{{ ip_down 如果有ipdown 需要的操作;删除hbase中down机ip相关的的cname，将相关cname存入新的ip column中
 * @param $pdns_name GSLB_UPDATESN中的rowkey记录不同partdns的不同更新序列，GSLB_IPCNAME中CQ的前缀
 * @param $pdns_url partdns服务器的地址和端口
 * @param $send_value 有save_pdnsupdate_data函数保存到GSLB_UPDATESN表中的更新队列对应的值如'down|211.136.104.177|222.73.254.112,222.73.254.113'
 * @param $hbase_client
 * @param $hbase_table GSLB_IPCNAME 存储edge server 对外ip地址下edgeserver状态，ip的属性，以及对应服务的cname，其中对于cname可以按不同partdns的实际解析存储
 * @param $hbase_sntable GSLB_UPDATESN 存储更新序列号，待更新和返回的结果
 */
function ip_down($pdns_name,$pdns_url,$send_value,$hbase_client,$hbase_table,$hbase_sntable){
    $funname="ip_down";
    $arr_pdnsresult=update_send($pdns_url,$send_value);
    $pdns_return_code=$arr_pdnsresult['status']['code'];
    if($pdns_return_code == 200){
        foreach($arr_pdnsresult['data'] as $result_k => $result_v){
            $now_ip =$result_v['1']; //新的IP地址
            $cname_column ='cname:'.$pdns_name.'_'.$result_v['0']; //新ip地址对应的CNAME
            $down_ip=$arr_pdnsresult['ip'];
            _debug("[fun:$funname]-[read for delete old cname-ip  $pdns_return_code][hbase_table=>$hbase_sntable down_ip=>$down_ip cname_column=>$cname_column]",_DLV_WARNING);
            $del_result = delete_column_with_rowkey($hbase_client,$hbase_table,$down_ip,$cname_column); 
            if($del_result==1){//如果删除操作正常就将新的IP和cname存入hbase
                $set_result=set_cells_with_column($hbase_client,$hbase_table,$now_ip,$cname_column);
                if($set_result==1){
                _debug("[fun:$funname]-[delete and set :Successful]-[hbase_table:$hbase_table-downip:$down_ip-upip:$now_ip-column:$cname_column]",_DLV_WARNING);
                }else {
                _debug("[fun:$funname]-[delete:Successful-set: Error]-[hbase_table:$hbase_table-downip:$down_ip-upip:$now_ip-column:$cname_column]",_DLV_WARNING);
                }
            }else{
                _debug("[fun:$funname]-[delete old column:Error]-[hbase_table:$hbase_table-row_key:$down_ip-column:$cname_column]",_DLV_WARNING);
            }
        }

        return $arr_pdnsresult;
    }elseif($pdns_return_code == 500){
        _debug("[fun:$funname]-[update_send:Error]-[pdns:$pdns_url-send_value:$send_value-http code:500]",_DLV_WARNING);
        return 0;
    }else{
        _debug("[fun:$funname]-[update_send:Error]-[pdns:$pdns_url-send_value:$send_value-http code:$pdns_return_code]",_DLV_WARNING);
        return 0;
    }
    //将down ip作为rowkey cname作为 column 删除之
    //将新的ip作为rowkey cname作为 column 添加之
}
/* {{{ ip_up 如果有宕机的ip回复 需要的操作;删除hbase中原替换ip相关的的cname，将相关cname存入恢复的ip的 column中
 *
 *
 */
function ip_up($pdns_name,$pdns_url,$send_value,$hbase_client,$hbase_table,$hbase_sntable){
    $funname="ip_up";
    $arr_pdnsresult = update_send($pdns_url,$send_value);
    $pdns_return_code=$arr_pdnsresult['status']['code'];
    if($pdns_return_code == 200){
        foreach($arr_pdnsresult['data'] as $result_k => $result_v){
            $rowkey_ip =$result_v['1']; //ip恢复操作前 采用backup ip
            $cname_column ='cname:'.$pdns_name.'_'.$result_v['0']; //ip恢复操作中 pdns返回结果中的cname值
            $arr_rowkey_columns[$rowkey_ip] = $cname_column; //组成$arr['ip']=cname 格式的数组
            $recove_ip=$arr_pdnsresult['ip'];
            // echo "删除的ip和cname和恢复的IP\n";
            _debug("[fun:$funname]-[bakup-ip:$rowkey_ip]-[cname:$cname_column]-[recove-ip:$recove_ip]",_DLV_WARNING);
            $del_result = delete_column_with_rowkey($hbase_client,$hbase_table,$rowkey_ip,$cname_column);
            if($del_result==1){//如果删除操作正常就将恢复的IP和cname存入hbasej
                set_cells_with_column($hbase_client,$hbase_table,$recove_ip,$cname_column);
            }else{
                _debug("[fun:$funname]-[delete old column:Error]-[hbase_table:$hbase_table-row_key:$recove_ip-column:$cname_column]",_DLV_WARNING);
            }
        }
        $send_value="delete|$recove_ip"; 
        $arr_delete=update_send($pdns_url,$send_value); //主动删除pdns上的“恢复”纪录
        $pdns_delete_code=$arr_delete['status']['code'];
        if($pdns_delete_code == 200){
            _debug("[fun:$funname]-[deleted pdns recove_ip successful ]-[pdns_url:$pdns_url-send_value:$send_value]",_DLV_WARNING);
        }else{
            _debug("[fun:$funname]-[fail to deleted pdns recove_ip ]-[pdns_url:$pdns_url-send_value:$send_value]",_DLV_WARNING);
        }
        return $arr_pdnsresult; 
    }elseif ($pdns_return_code == 201){
        _debug("[fun:$funname]-[fail to resumed recove_ip ]-[pdns_url:$pdns_url-send_value:$send_value]",_DLV_WARNING);
        return 0;
    }
}//function END

?>
