<?php
/*
  +----------------------------------------------------------------------+
  | Name: workers/fun/db.m                                               |
  +----------------------------------------------------------------------+
  | Comment: mysql,redis,hbase相关的函数                                 |
  +----------------------------------------------------------------------+
  | Author:xuyunfeng
  +----------------------------------------------------------------------+
  | Created:2012-07-04 19:59:12                              |
  +----------------------------------------------------------------------+
  | Last-Modified:2012-07-25 19:10:26                        |
  +----------------------------------------------------------------------+
 */

function array_uniq($array1,$array2){
    foreach($array2 as $k2=>$v2){
        $same=0;
        foreach($array1 as $k1=>$v1){
            if($v2==$v1) {
                $same=1;
                break;
            }           
        }
        if(!$same) $array1[]=$v2;
    }
    return $array1;
}

/* {{{ redis 队列 push操作
 * @param $redis_host char IP address or hostname 
 * @param $redis_port char redis server port
 * @param $redis_dbname char redis 队列所在DB的名字
 * @param $mq_key char redis 队列所的名字
 * @param $push_value char redis 队列值
 * @param $push_method  char redis 队列push 方法 默认为队位插入 rpush为插入队首
 *
 * @return status 
 *
 */
function redis_push($redis_host='127.0.0.1',$redis_port='6379',$redis_dbname='1',$mq_key='MYMQ',$mq_value,$push_method='lpush'){
    $single_server = array(
        'host'     => $redis_host,
        'port'     => $redis_port,
        'database' => $redis_dbname 
    );
    $client = new Predis_Client($single_server); 
    $client->select($redis_dbname);
    try{
        if($push_method=='lpush'){
            $client->lpush($mq_key,$mq_value);
        } else {
            $client->rpush($mq_key,$mq_value);
        }
    }catch(Exception $e){
        _debug($e->getMessage(),_DLV_WARNING);
    }
    $client->close;
}
/*}}}*/

/* {{{ redis 队列 pop操作
 * @param $redis_host char IP address or hostname
 * @param $redis_port char redis server port
 * @param $redis_dbname char redis 队列所在DB的名字
 * @param $mq_key char redis 队列所的名字
 * @param $pop_method char redis 队列pop方法 默认rpot为先出队首  lpop 先出队尾
 * @return $mq_value
 *
 */
function redis_pop($redis_host='127.0.0.1',$redis_port='6379',$redis_dbname='1',$mq_key='MYMQ',$pop_method='rpop'){
    $single_server = array(
        'host'     => $redis_host,
        'port'     => $redis_port,
        'database' => $redis_dbname
    );
    $client = new Predis_Client($single_server);
    $client->select($redis_dbname);
    try{
        if($pop_method=='lpop'){
            $mq_value= $client->lpop($mq_key);
        } else {
            $mq_value= $client->rpop($mq_key);
        }
    }catch(Exception $e){
        _debug($e->getMessage(),_DLV_WARNING);
    }
    $client->close;
    return($mq_value); 
}
/*}}}*/

function open_hbase($host,$port=9090,$sendtimeout=10000,$recvtimeout=10000) {
    try {
        $socket = new TSocket($host, $port);
        $socket->setSendTimeout($sendtimeout); // 2 seconds
        $socket->setRecvTimeout($recvtimeout); // 2 seconds
        $mdb_transport = new TBufferedTransport($socket);
        $protocol = new TBinaryProtocol($mdb_transport);
        $mdb_client = new HbaseClient($protocol);
        $mdb_transport->open();
        $GLOBALS['hbase_transport']=$mdb_transport;
        return $mdb_client;
    } catch (Exception $e) {
        _debug($e->getMessage(),_DLV_WARNING);
        exit();
    }
}

function close_hbase() {
    if (isset($GLOBALS['hbase_transport'])) {
        $GLOBALS['hbase_transport']->close();
    }
}


/* {{{给定 table row_key CF 返回该CF下的所有CQ,Value,TS
 * @param $client hbase的HbaseClient对象
 * @param $table
 * @param $row_key
 * @param $columns 就是 ColumnFamily
 * @return $return_arr Array类型 $return_arr['ColumnQualified']['cells']=cells_value,例如
 * @return [status:TSbeiai01] => Array
 * @return (
 * @return     [value] => 1
 * @return     [timestamp] => 1341249949248
 * @return )

 */
function get_cells_with_columns($client,$table,$row_key,$columns){
    try{
        $arr_row= $client->getRowWithColumns($table,$row_key,array($columns));
    }catch(Exception $e){
        _debug($e->getMessage(),_DLV_WARNING);
        
        //var_dump($e->getMessage());
    }
    if(isset($arr_row['0'])){
        foreach ($arr_row['0'] as $k => $v){
            if($k=='columns'){
                foreach($v as $cq=>$cells){
                    foreach($cells as $k_cells=>$cells_v){
                        $return_arr[$cq][$k_cells]=$cells_v;
                    }
                }
            }
        }
        return $return_arr;
    }else {
        return 1;
    }
}

/* {{{给定 own: CQ 和 health的CQ 'status:health' 返回相同own:cq且status:health>0的ip列表,
 * 如果未指定own:cq或者own:cq为0则返回所有status:health>0的ip列表
 * @param $hbase_client
 * @param $hbase_table
 * @param $hbase_column_own 指定相同的运营商或者相同的地区
 * @param $hbase_column_health
 * @return $new_ip 返回一个new_ip的数组
 */ 

function get_rowkey_with_columns($hbase_client,$hbase_table,$hbase_column_own='0',$hbase_column_health='status:health'){
    try{
        if($hbase_column_own=='0'){
            $scanid= $hbase_client->scannerOpen($hbase_table,'0',array($hbase_column_health));
        }else{
            $scanid= $hbase_client->scannerOpen($hbase_table,'0',array($hbase_column_own,$hbase_column_health));
        }
    }catch(Exception $e){
        _debug($e->getMessage(),_DLV_WARNING);
    }
    $cells_with_columns= $hbase_client->scannerGetList($scanid,'100000');
    $hbase_client->scannerClose($scanid);
    foreach($cells_with_columns as $k=>$TRowResult){
        $provide_ip = $TRowResult->row; //取得provide_ip
        $cells_health = $cells_own = 0; //初始化
        foreach($TRowResult->columns as $cells =>$cells_obj){
            if(($cells==$hbase_column_health) && ($cells_obj->value > 0)){
                $cells_health=1;
            }elseif($cells==$hbase_column_own){
                $cells_own=1;
            }
        }
        if($hbase_column_own=='0'){
            ($cells_health=='1') && ($new_ip[]=$provide_ip);//存在provide_ip的health大于0并且包含有相同的own则选为备用新ip
        }else{
            ($cells_health=='1') && ($cells_own=='1') && ($new_ip[]=$provide_ip);//存在provide_ip的health大于0并且包含有相同的own则选为备用新ip
        }
    }
    return $new_ip;
}

function set_cells_with_mutations($client,$table,$row_key,$mutations){
/*    $mutations = array (
        new Mutation(array(
            'column'=>$columns,
            'value'=>$value
        )),
    );
 */
    try{
        $client->mutateRow($table,$row_key,$mutations);
    }catch(Exception $e){
        _debug($e->getMessage(),_DLV_WARNING);
    }

    return 1;
}

function set_cells_with_column($client,$table,$row_key,$columns,$value='1'){
    $mutations = array (
        new Mutation(array(
            'column'=>$columns,
            'value'=>$value
        )),
    );
    try{
        $client->mutateRow($table,$row_key,$mutations);
    }catch(Exception $e){
        _debug($e->getMessage(),_DLV_WARNING);
        return 0;
        // _debug($e->getMessage(),_DLV_WARNING);
    }

    return 1;
}
/* {{{ 更新 edge server的状态，并计算整个provide ip的heath状况
 * @param $mq_value 消息队列体
 * @param $hbase_client hbase对象
 * @param $ipcname_table 
 * @return $health_ch 0，1，3，5
 */
function edge_status_update($mq_value,$hbase_client,$ipcname_table){
    list($mq_opt,$mq_eds,$mq_pip,$mq_slv,$mq_sl,$mq_nip,$mq_car,$mq_loc,$mq_ts) = explode('|',$mq_value);
    $row_key=$mq_pip;
    $column='status:'.$mq_eds;
    $eds_exist = get_cells_with_columns($hbase_client,$ipcname_table,$row_key,$column);
    if($eds_exist== 1){
        _debug("[edge_status_update]-[can not found edges server $column in exist provideip  $row_key]-[$ipcname_table]",_DLV_WARNING);
        return 5;
    }
    _debug("[edge_status_update]-[look for $row_key]-[$ipcname_table]",_DLV_NOTICE);
    set_cells_with_column($hbase_client,$ipcname_table,$row_key,$column,$mq_slv);
    $mq_pip_status = get_cells_with_columns($hbase_client,$ipcname_table,$row_key,$columns='status:');
    $sum_edges = count($mq_pip_status) - 1; //此rowkey（provide_ip) 下所有的edge server数
    $old_health = $mq_pip_status['status:health']['value']; //保存原始health值用于计算变化
    $alive_edges=0;
    foreach ($mq_pip_status as $status_cq=>$v){ //计算目前存活edge server数
        if(($status_cq!=='status:health') && $v['value'] == '1'){
            $alive_edges += 1;
        }
    }
    $new_health = ceil(($alive_edges/$sum_edges) * 10 );
    /*
     *provide_ip的健康状况　$health_ch为0: 由非0变成0 需要移除; 1: 由 0 变成非0 ; 3: 说明health未发生变化
     */
    ($old_health > 0) && ($new_health > 0) && ($health_ch=3);
    ($old_health == 0) && ($new_health == 0) && ($health_ch=3);
    ($old_health == 0) && ($new_health > 0) && ($health_ch=1);
    ($old_health > 0) && ($new_health == 0) && ($health_ch=0);

    $setcells_ok=set_cells_with_column($hbase_client,$ipcname_table,$row_key,'status:health',$new_health);//更新status:health
    if($setcells_ok){
        _debug("[$whoami]-[update_status:health:Successful]-[$old_health change to $new_health  health_ch:$health_ch]",_DLV_WARNING);
        return $health_ch;
    }else {
        _debug("[$whoami]-[update_status:health:Error]-[$old_health change to $new_health  health_ch:$health_ch]",_DLV_ERROR);
        return 5; //更新出错
    }
}
/* }}} */

function updateiplist_generate($health_ch,$mq_pip,$mq_ts,$hbase_client,$hbase_table,$column='own:'){
    //如果health_ch为0，就需要为provide ip 需找一些替换用ip，目前按照运营商(carrier)优先地域(location)的原则
    if($health_ch==0){
        $down_ip_carrierlocation = get_cells_with_columns($hbase_client,$hbase_table,$mq_pip,$column); //找出downip的运营商和地域
        _debug("[get_cells_with_columns]-[$hbase_table---$mq_pip---$column]",_DLV_WARNING);
        foreach($down_ip_carrierlocation as $own_cq=>$v){
            if(strpos($own_cq,'c_') == true ){
                $down_ip_carrier=$own_cq;
            }elseif(strpos($own_cq,'l_') == true){
                $down_ip_location=$own_cq;
            }else {
                _debug("[$whoami]-[pdnsupdate_generate:Warning $mq_pip is not set own?]",_DLV_WARNING);
            }
        }
        _debug("[updateiplist_generate]-[jsondown: $json_down][down ip carrier: $down_ip_carrier] [down ip location:$down_ip_location]",_DLV_WARNING);
        $new_carrierip = get_rowkey_with_columns($hbase_client,$hbase_table,$down_ip_carrier);//找出相同own:c_的健康ip
        $new_iplist=$new_carrierip;
        if(count($new_iplist) < 2){
            $new_locationip = get_rowkey_with_columns($hbase_client,$hbase_table,$down_ip_location);//不足2个就找own:l_的健康ip
            $new_iplist=array_uniq($new_iplist,$new_locationip);
        }elseif(count($new_iplist) < 2){
            $new_otherip= get_rowkey_with_columns($hbase_client,$hbase_table,$down_ip_own='0');
            $new_iplist=array_uniq($new_iplist,$new_otherip);
        }elseif(count($new_iplist) < 1){
            _debug("[$whoami]-[pdnsupdate_generate:Error no backup ip]",_DLV_ERROR);
        }elseif(count($new_iplist) >= 2){
            _debug("[updateiplist_generate]-[pdnsupdate_generate:had enouth backup ip,".count($new_iplist)." larger than 2]",_DLV_WARNING);
        }else{
            _debug("[updateiplist_generate]-[pdnsupdate_generate:Warning no enouth backup ip,".count($new_iplist)." small than 2]",_DLV_WARNING);
        }
        return $new_iplist;
    }elseif ($health_ch==1){
        return array($mq_pip);
    }else {
        return 0;
    }
}
/*{{{ 保存需要更新的信息
 *
 */
function save_pdnsupdate_data($hbase_client,$hbase_sntable,$pdns_name,$mq_pip,$bakip_list,$mq_ts,$health_ch){
    if($health_ch == 0){
        foreach($bakip_list as $bip){
            $biplist .= $bip.',';
        }
        $biplist = substr($biplist,0,strlen($ip)-1);
        //        'down|211.136.104.177|222.73.254.112,222.73.254.113';
        $update_value='down|'.$mq_pip.'|'.$biplist;
    }else{
        $update_value='up|'.$mq_pip;
    }
    _debug("[save_pdnsupdate_data] [save key= send:$mq_ts] [save value:$update_value]",_DLV_WARNING);
    $mutations = array(
        new Mutation(array(
            'column' => 'send:'.$mq_ts,
            'value' => $update_value
        )),
        new Mutation(array(
            'column' => 'delay:'.$mq_ts,
            'value' => '1'
        )),
    );
    try{
        $hbase_client->mutateRow($hbase_sntable,$pdns_name, $mutations);
    }catch(Exception $e){
        _debug($e->getMessage(),_DLV_WARNING);
    }

}
/* }}} */


/*{{{在pdns返回恢复包时，取得现有cname和ip 并删除 columns
 *
 * 
 */
function delete_columns($hbase_client,$hbase_table,$columns){
    try{
        $scanid= $hbase_client->scannerOpen($hbase_table,'0',$columns); 
    }catch(Exception $e){
        _debug($e->getMessage(),_DLV_WARNING);
    }
    $cells_with_columns= $hbase_client->scannerGetList($scanid,'100000');
    $hbase_client->scannerClose($scanid);
    foreach($cells_with_columns as $k=>$TRowResult){
        $provide_ip = $TRowResult->row; //取得provide_ip
        foreach($TRowResult->columns as $column_name =>$cells_obj){
            try{
                $r = $hbase_client->deleteAll($hbase_table,$provide_ip,$column_name);
            }catch(Exception $e){
                _debug('[delete_columns] '.'[Table:'.$hbase_table.'rowkey:'.$provide_ip.'column_name:'.$column_name.']'.$e->getMessage(),_DLV_WARNING);
            }
        }
    }
    return 1;
}

function delete_columns_with_rowkey($hbase_client,$hbase_table,$arr_rowkey_columns){
    foreach($arr_rowkey_columns as $rowkey=>$column) {
        try{
            $r = $hbase_client->deleteAll($hbase_table,$rowkey,$column);
        }catch(Exception $e){
            _debug('[delete_columns] '.'[Table:'.$hbase_table.'rowkey:'.$rowkey.'column_name:'.$column.']'.$e->getMessage(),_DLV_WARNING);
        }
    }
    return 1;
}

function delete_column_with_rowkey($hbase_client,$hbase_table,$rowkey,$column){
    $funname="delete_column_with_rowkey";
    _debug("[fun:$funname]-[Table:$hbase_table][rowkey:$rowkey][column_name:$column]",_DLV_WARNING);
    try{
        $r = $hbase_client->deleteAll($hbase_table,$rowkey,$column);
    }catch(Exception $e){
        _debug("[delete_columns]-[Table: $hbase_table rowkey: $rowkey column_name: $column]".$e->getMessage(),_DLV_WARNING);
    }
    return 1;
}


?>
