<?php
/*
  +----------------------------------------------------------------------+
  | Name:services/Sdefault.m                                             |
  +----------------------------------------------------------------------+
  | Comment:                                                             |
  +----------------------------------------------------------------------+
  | Author:Odin                                                          |
  +----------------------------------------------------------------------+
  | Created:2012-03-07 18:10:12                                          |
  +----------------------------------------------------------------------+
  | Last-Modified:2013-01-17 18:10:40                                    |
  +----------------------------------------------------------------------+
*/

function _default() {
    $GLOBALS['outputContent']=array(
        'response'=>_getErrDesc($GLOBALS['errCode']),
        'protocolVer' => $GLOBALS['protocolVer'],
        'operation' => $GLOBALS['operation'],
        'serviceName' => $GLOBALS['serviceName'],
        'selector' => $GLOBALS['selector'],
        'rowKey' => $GLOBALS['rowKey'],
    );
}
function _delete(){
    $edges=array("222.73.254.111","222.73.178.72","221.181.66.12","121.14.17.98","211.95.73.229","210.51.58.22","221.204.231.130","211.136.104.177");
    //    $edges=array("221.204.231.130");
    $domain=end(explode('=',$GLOBALS['rowKey']));
    $uri_arr=explode(',',$GLOBALS['postData']);
    $cdn_domain=array("host:stat.mdn2.net");
    _debug("[".__FUNCTION__."][funcName:$funcName] postdata {$GLOBALS['postData']} ]",2);
    foreach($uri_arr as $k=>$v) {
        $clear_uri=substr(substr($v,0,-1),1,255);
        $clear_url="http://$domain{$clear_uri}";
        $clear_url_decode=urlencode($clear_url);
        foreach($edges as $ip){
            $curl_url="http://${ip}/stat_cache/delete_url?url=${clear_url_decode}";
            $ch=curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER,$cdn_domain);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30 );
            curl_setopt($ch,CURLOPT_URL,$curl_url);
            curl_exec($ch);
            if($error =curl_error($ch)){
                _debug("[".__FUNCTION__."][funcName:$funcName]curl_exec error $curl_url]",2);
            }
            $curl_code=curl_getinfo($ch,CURLINFO_HTTP_CODE);
            _debug("[".__FUNCTION__."][funcName:$funcName]curl_exec return $curl_code $curl_url]",2);
        }
    }
}
function _get(){
require_once('lib/thrift/Thrift.php' );
require_once( 'lib/thrift/transport/TSocket.php' );
require_once( 'lib/thrift/transport/TBufferedTransport.php' );
require_once( 'lib/thrift/protocol/TBinaryProtocol.php' );
require_once( 'lib/thrift/packages/Hbase/Hbase.php' );
include_once('fun/fun.db.m');
//$hbase_table='TGSLB_IPCNAME';
$ipcname_cname="cname:";
$ipcname_own="own:";
$ipcname_status="status:";
$hbase_table="GSLB_IPCNAME";
$hbase_client=open_hbase('10.10.8.181','9090',10000,10000);
$scanid = $hbase_client->scannerOpen($hbase_table,'0',array($ipcname_cname,$ipcname_own,$ipcname_status));
$cells_with_columns= $hbase_client->scannerGetList($scanid,'100000');
$hbase_client->scannerClose($scanid);
foreach($cells_with_columns as $k=>$TRowResult){
    $provide_ip = $TRowResult->row; //取得provide_ip
    $allEdges[$provide_ip]['cname']=array();
    $allEdges[$provide_ip]['own']=array();
    $allEdges[$provide_ip]['status']=array();
    foreach($TRowResult->columns as $cells =>$cells_obj){
        if(strstr($cells,$ipcname_cname)){
            $cnameTmp = substr($cells,strlen($ipcname_cname));
            array_push($allEdges[$provide_ip]['cname'],$cnameTmp);
        }
        if(strstr($cells,$ipcname_own)){
            $ownTmp =  substr($cells,strlen($ipcname_own));
            array_push($allEdges[$provide_ip]['own'],$ownTmp);
        }
        if(strstr($cells,$ipcname_status)){
            if($cells=='status:health') {
                $statusTmp=':'.$cells_obj->value;
            }else {
                $statusTmp =  substr($cells,strlen($ipcname_status));
            }
            array_push($allEdges[$provide_ip]['status'],$statusTmp);
        }
    }
}
$allserver=json_encode($allEdges);
//foreach($allEdges $ip => $cf){

//}
$GLOBALS['outputContent']=array(
    'response'=>_getErrDesc($GLOBALS['errCode']),
    'protocolVer' => $GLOBALS['protocolVer'],
    'operation' => $GLOBALS['operation'],
    'serviceName' => $GLOBALS['serviceName'],
    'selector' => $GLOBALS['selector'],
    'value' => $allserver,
);

}
