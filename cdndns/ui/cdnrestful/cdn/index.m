<?php
/*
  +----------------------------------------------------------------------+
  | Name:index                                                           |
  +----------------------------------------------------------------------+
  | Comment:REST API Entrance                                            |
  +----------------------------------------------------------------------+
  | Author:
  +----------------------------------------------------------------------+
  | Created:2011-02-22 10:41:44                                          |
  +----------------------------------------------------------------------+
  | Last-Modified:2012-03-06 19:22:05                                    |
  +----------------------------------------------------------------------+
*/
error_reporting(E_ALL | E_STRICT);

define('_API_ROOT', dirname(__FILE__).'/');

/* {{{ 载入常数
 */
include_once(_API_ROOT.'inc/const.m');
/* }}} */

/* {{{ 基础函数
 */
include_once(_API_ROOT.'fun/base.m');
/* }}} */

/* {{{ 初始化(载入配置等)
 */
include_once(_API_ROOT.'modules/initApi.m');
/* }}} */

/* {{{preDispatch
 */
include_once(_API_ROOT.'modules/preDispatch.m');
/* }}} */

/* {{{ 分析请求,生成全局数组,加载需要的函数
 */
include_once(_API_ROOT.'modules/parseRequest.m');
/* }}} */

/* {{{ api运行
 */
_apiRun();
/* }}} */
