#!/usr/local/bin/perl -w
use File::Basename;
use DBD::SQLite;

##hash init
#
%ipRegion = (                                                                                                                                  
    'beijing'=>'北京','shanghai'=>'上海','tianjin'=>'天津','chongqing'=>'重庆','heilongjiang'=>'黑龙江','jilin'=>'吉林',                       
    'liaoning'=>'辽宁','neimeng'=>'内蒙古','hebei'=>'河北','henan'=>'河南','guangdong'=>'广东','hubei'=>'河北',                                
    'shandong'=>'山东','zhejiang'=>'浙江','anhui'=>'安徽','jiangsu'=>'江苏','jiangxi'=>'江西','yunan'=>'云南',                                 
    'ningxia'=>'宁夏','qinghai'=>'青海','hillxi'=>'山西','shanxi'=>'陕西','hunan'=>'湖南','fujian'=>'福建',                                    
    'gansu'=>'甘肃','sichuan'=>'四川','guangxi'=>'广西','guizhou'=>'贵州','hainan'=>'海南','xizang'=>'西藏',
    'xinjiang'=>'新疆','hongkong'=>'香港','macao'=>'澳门','taiwan'=>'台湾','or'=>'其他地区',                                                   
);                                                                                                                                             
%ipCarrier = (
    'mm'=>'移动mobile','cm'=>'电信mobile','um'=>'联通mobile','mw'=>'移动wifi','cw'=>'电信wifi','uw'=>'联通wifi',                               
    'oi'=>'其它运营商',                                                                                                                        
);       
#hash end

my $indbFile = "./dnsdata.db";
my $outdbFile = "./data/pdns.db";
my $inTable = "clientToServer";
my $outTable = "records";
$indbh = DBI->connect("dbi:SQLite:dbname=$indbFile",'','',{
        RaiseError => 1,
        AutoCommit => 0
    }
);

$outdbh = DBI->connect("dbi:SQLite:dbname=$outdbFile",'','',{
        RaiseError => 1,
        AutoCommit => 0
    }
);

#read recordes from powerdns db
my $outdbconn = $outdbh->prepare("select * from $outTable");
$outdbconn->execute();
while (my @row = $outdbconn->fetchrow_array){
    my($pdnsid,$did,$name,$type,$ttl,$pr,$content,$chtime,) = @row;
    my($foreigndomain,$clicarrilocal,$basedomain);
    if($type=~/^A$/ or $type =~ /^CNAME$/ ){
        if($name =~ /(.+)\.(\w+)\.(\w+\.\w+\.\w+$)/){
            $foreigndomain = $1;
            $clicarrilocal = $2;
            $basedomain = $3;
        }elsif($name =~ /(\w+)\.(\w+\.\w+\.\w+$)/){
            $foreigndomain = '';
            $clicarrilocal = $1;
            $basedomain = $2;
        }else {
            print "read outdb failed\n";
            exit(0);
        }
        $indbh->do("insert into $inTable(foreigndomain,clicarrilocal,basedomain,recordtype,iporcname,pdnsid)
            values('$foreigndomain','$clicarrilocal','$basedomain','$type','$content','$pdnsid')");
        if ($indbh->err()) {
            die "$DBI::errstr\n";
        }
        $indbh->commit();
    }
}

sub scanDir{
    my $dirPath = pop(@_);
    opendir DIR, ${dirPath} or die "Can't open \"$dirPath\"\n";
    return(readdir DIR);
}
sub readWholeFile{
    my $file = pop(@_);
    open FILE, $file or die "Can't open \"$file\"\n";
    return <FILE>;
}

<<COM;
CREATE TABLE clientToServer (
    id              INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    foreigndomain   VARCHAR(255),
    clicarrilocal   VARCHAR(255) NOT NULL,
    basedomain      VARCHAR(255) NOT NULL,
    recordtype      VARCHAR(255) NOT NULL,
    iporcname       VARCHAR(255) NOT NULL,
    changetime      VARCHAR(255),
    UNIQUE  (id)
    );
CREATE TABLE clientCarrierLocal (
    id              INTEGER NOT NULL PRIMARY KEY,
    clicarrilocal   VARCHAR(255) NOT NULL,
    clicarri        VARCHAR(255) NOT NULL,
    clilocal        VARCHAR(255) NOT NULL,
    iprank          VARCHAR(255) NOT NULL,
    UNIQUE  (id,clicarrilocal) 
    );
CREATE TABLE edgeServerInfo (
    id                 INTEGER NOT NULL PRIMARY KEY,
    iporcname          VARCHAR(255) NOT NULL,
    mytype             INTEGER(1),
    edgecarrier        VARCHAR(255) NOT NULL,
    edgelocal          VARCHAR(255) NOT NULL,
    edgestatus         INTEGER(1),
    UNIQUE  (id,iporcname)
    );
COM
