#!/usr/local/bin/perl -w
use DBD::SQLite;


chomp(@ARGV) if $ARGV;
&printusage if $ARGV[0] eq '--help';

my %args=();
foreach (@ARGV){
    my @args  = split(/=/,$_);
    %args = (%args,substr($args[0],2),$args[1]);
}

print "updatecontent.pl --help" if (!defined($args{'foreigndomain'}) || 
    !defined($args{'basedomain'}) || !defined($args{'recordtype'}) || 
    !defined($args{'newcontent'}) );

#db and output sqlfile config
$time=time();
my $dbFile = "./dnsdata.db";
my $clTable = "clientCarrierLocal";
my $ctsTable = "clientToServer";
my $outputSQLFile="./pdnsUpdate$time.sql";
$dbh = DBI->connect("dbi:SQLite:dbname=$dbFile",'','',{
        RaiseError => 1,
        AutoCommit => 0
    }
);               
open(OUTFILE,">$outputSQLFile")  or die ("Can not open file $outputSQLFile");

#找出ACL view
my $clQuery = &generateclSQL;
print $clQuery;
my $cldbconn = $dbh->prepare("$clQuery");
$cldbconn->execute();

while (my @clrow = $cldbconn->fetchrow_array){
    my ($cl) =  @clrow;
    #没有指定原始ip或cname
    if (!defined($args{'originalcontent'})) {
        $recordQuery="select id,iporcname,pdnsid from clientToServer where 
        foreigndomain = '$args{'foreigndomain'}' and clicarrilocal = '$cl' ".
        "and basedomain = '$args{'basedomain'}'";
        my $recorddbconn = $dbh->prepare("$recordQuery");
        $recorddbconn->execute();
        print "\n";
        #不指定原始ip或cname，默认替换第一条记录
        while (my @recordrow = $recorddbconn->fetchrow_array){
            my($id,$iporcname,$pdnsid) = @recordrow;
            print "$id<>$iporcname<>$pdnsid-->$args{'recordtype'}<>$args{'newcontent'}\n";
             $updateSQL = "update clientToServer set recordtype='$args{'recordtype'}', ".
             "iporcname='$args{'newcontent'}', changetime='$time' where id=$id";
             print $updateSQL."\n";
             $dbh->do($updateSQL);
             if ($dbh->err()){
                 die "$DBI::errstr\n";
             }
             $dbh->commit();
             print OUTFILE "update records set type='$args{'recordtype'}', ".
             "content='$args{'newcontent'}', change_date='$time' where id=$pdnsid;\n";
             last;
         }
     }elsif (defined($args{'originalcontent'})) {
         $recordQuery="select id,iporcname,pdnsid from clientToServer where 
         foreigndomain = '$args{'foreigndomain'}' and clicarrilocal = '$cl' and  
         basedomain = '$args{'basedomain'}' and iporcname in ($args{'originalcontent'})";
         my $recorddbconn = $dbh->prepare("$recordQuery");
         $recorddbconn->execute();
         print "\n";
         while (my @recordrow = $recorddbconn->fetchrow_array){
             print "in\n";
             my($id,$iporcname,$pdnsid) = @recordrow;
             print "$id<>$iporcname<>$pdnsid-->$args{'recordtype'}<>$args{'newcontent'}\n";
             $updateSQL = "update clientToServer set recordtype='$args{'recordtype'}', ".
             "iporcname='$args{'newcontent'}', changetime='$time' where id=$id";
             $dbh->do($updateSQL);
             if ($dbh->err()){
                 die "$DBI::errstr\n";
             }
             $dbh->commit();
             print OUTFILE "update records set type='$args{'recordtype'}', ".
             "content='$args{'newcontent'}', change_date='$time' where id=$pdnsid;\n";
         }

     }
 }

#looking for acl view (carrierlocal)
 sub generateclSQL{
     return "select clicarrilocal from $clTable where clicarri in($args{'carrier'}) ".
     "group by clicarrilocal"
     if (defined($args{'carrier'}) && !defined($args{'local'}) && !defined($args{'region'}));

     return "select clicarrilocal from $clTable where clilocal in($args{'local'}) ".
     "group by clicarrilocal"
     if (!defined($args{'carrier'}) && defined($args{'local'}) && !defined($args{'region'}));

     return "select clicarrilocal from $clTable where cliregion in($args{'region'}) ".
     "group by clicarrilocal"
     if (!defined($args{'carrier'}) && !defined($args{'local'}) && defined($args{'region'}));

     return "select clicarrilocal from $clTable where  clicarri in($args{'carrier'}) ".
     "and clilocal in($args{'local'}) group by clicarriloca"
     if (defined($args{'carrier'}) && defined($args{'local'}) && !defined($args{'region'}));

     return "select clicarrilocal from $clTable where  clicarri in($args{'carrier'}) ".
     "and cliregion in($args{'region'}) group by clicarrilocal"
     if (defined($args{'carrier'}) && !defined($args{'local'}) && defined($args{'region'}));
 }

 sub printusage{
     print "\nusage: updatecontent <arg1> <arg2> <arg3> <arg4> [arg5] <arg6>\n\n";
     print "arg1:\n";
     print "    --foreigndomain=<domainname>\n";
     print "             domainname就是需要做更新的域名，注意将.替换成-\n";
     print "arg2:\n";
     print "    --basedomain=<basedomainname>\n";
     print "             domainname就是需要做更新的域名基于的cdn基础域名一般为mdn.mdn2.net\n";
     print "arg3:\n";
     print "    --carrier = <carrier code>\n";
     print "    --local   = <province code>\n";
     print "    --region  = <region code>\n";
     print "             如果只指定--carrier 就代表所有省市或则地区的运营商\n";
     print "             如果只指定---local或者--region 就代表所在省市或地区的所有运营商\n";
     print "             如果只指定---local或者--region 同时指定--carrier 就代表所在省市或地区的运营商\n";
     print "             目前不支持同时指定---local和--region\n";
     print "arg4:\n";
     print "    --recordtype   = <CNAME/A>\n";
     print "             新内容的记录类型，一般ip为A记录，域名为CNAME\n";
     print "arg5:\n";
     print "    --originalcontent   = <IP/domainname>\n";
     print "             原始内容的可能是一个ip地址，或者一个域名,如果指定默认替换recordeid较小的记录\n";
     print "arg6:\n";
     print "    --newcontent   = <IP/domainname>\n";
     print "             新内容的可能是一个ip地址，或者一个域名\n";
     print "\n范例： updatecontent --foreigndomain=dl-mgogo-com --basedomain=mdn.mdn2.net --carrier='\"mm\",\"cm\"' ";
     print "--region='\"Northeast\"' --recordtype=CNAME --newcontent=dl.mgogo.com.wscdns.\n";
     print "\n";
     exit;
 }

