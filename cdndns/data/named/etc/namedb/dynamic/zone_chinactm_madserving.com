$TTL    3600
$ORIGIN madserving.com.
@               IN      SOA     ns01.mdn2.net.     domains.madhouse-inc.com.       (
                                2011112802     ; Serial
                                10800           ; Refresh
                                1800            ; Retry
                                1296000         ; Expire
                                3600    )       ; Minimum TTL
       3600    IN      NS      ns01.mdn2.net.
       3600    IN      NS      ns02.mdn2.net.
       3600    IN      NS      ns03.mdn2.net.
       3600    IN      NS      ns04.mdn2.net.

; A Records
m1      86400   IN      A       221.130.176.180
r1      86400   IN      A       222.73.246.55
t1      86400   IN      A       221.130.176.180
;ns01   3600    IN      A       211.136.105.207
ns1     3600    IN      A       121.14.17.97
appsrv1 1200    IN      A       211.136.107.49
appsrv2 1200    IN      A       211.136.107.49
api     1200    IN      A       211.136.107.49 
ns2     3600    IN      A       202.106.169.109
mdnapi.service    3600    IN      A       211.136.107.44
yicha.api 1200 IN       A       220.189.221.37
api.appsrv3 1200 IN       CNAME       madservelb-365301140.ap-southeast-1.elb.amazonaws.com.
inimg 1200 IN       CNAME     madimageselb-1852159349.ap-southeast-1.elb.amazonaws.com.
appsrv3 1200 IN       CNAME       madservelb-365301140.ap-southeast-1.elb.amazonaws.com.
madn.mws 1200 IN       A       211.136.107.47
maas.mws 1200 IN       A       211.136.107.47
mgmtapi1 1200 IN       A       220.189.221.61
www 1200 IN       A       211.136.105.207
service.dsp 1200 IN       A       211.136.107.39
srv.dsp 1200 IN       A       211.136.107.39
api.dsp 1200 IN       A       211.136.107.39
tc.dsp 1200 IN       A       211.136.107.39
appsrv4 1200 IN A 211.136.107.49
material 1200 IN       CNAME sm2.mdn2.net.
material-orig 1200 IN A  220.189.221.61

; CNAME Records
;imap   3600    IN      CNAME   imap.secureserver.net

; MX Records
;@      3600    IN      MX      5       mxbiz1.qq.com.
;@      3600    IN      MX      10      mxbiz2.qq.com.
