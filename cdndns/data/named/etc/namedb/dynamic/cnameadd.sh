#!/bin/sh
cd $1
DOMAIN=$2
for FILE in `/usr/bin/find . -name "*_mdn2.net" | cut -c 3-`
do
echo $FILE add $DOMAIN
HOSTNAME=`echo $DOMAIN|sed 's/\./\-/g'`
ID=`echo "$FILE" |cut -f 2 -d _`
LINE="${HOSTNAME}	  1200 IN CNAME  	${HOSTNAME}.${ID}.mdn.mdn2.net."
echo $LINE >> $FILE
echo "origin 	1200 IN  A 	211.136.107.42" >>$FILE
done
