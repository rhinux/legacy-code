#!/usr/local/bin/perl -w
use File::Basename;
use DBD::SQLite;

##hash init
#
%ipRegion = (                                                                                                                                  
    'beijing'=>'北京','shanghai'=>'上海','tianjin'=>'天津','chongqing'=>'重庆','heilongjiang'=>'黑龙江','jilin'=>'吉林',                       
    'liaoning'=>'辽宁','neimeng'=>'内蒙古','hebei'=>'河北','henan'=>'河南','guangdong'=>'广东','hubei'=>'河北',                                
    'shandong'=>'山东','zhejiang'=>'浙江','anhui'=>'安徽','jiangsu'=>'江苏','jiangxi'=>'江西','yunan'=>'云南',                                 
    'ningxia'=>'宁夏','qinghai'=>'青海','hillxi'=>'山西','shanxi'=>'陕西','hunan'=>'湖南','fujian'=>'福建',                                    
    'gansu'=>'甘肃','sichuan'=>'四川','guangxi'=>'广西','guizhou'=>'贵州','hainan'=>'海南','xizang'=>'西藏',
    'xinjiang'=>'新疆','hongkong'=>'香港','macao'=>'澳门','taiwan'=>'台湾','or'=>'其他地区',                                                   
);                                                                                                                                             
%ipCarrier = (
    'mm'=>'移动mobile','cm'=>'电信mobile','um'=>'联通mobile','mw'=>'移动wifi','cw'=>'电信wifi','uw'=>'联通wifi',                               
    'oi'=>'其它运营商',                                                                                                                        
);       
%chinaregion = (
    'EastCN' =>'华东','SouthCN'=>'华南','CentralCN'=>'华中', 'NorthCN'=>'华北',
    'Northwest'=>'西北','Southwest'=>'西南', 'Northeast'=>'西北', 'HMT'=>'港澳台',
)
#hash end

my $dbFile = "./dnsdata.db";
my $mydir = "/usr/home/rhinux/Code/perl/cdndns/data/named/etc/namedb/ACL";
my @files = &scanDir($mydir);
my $tableClientIp = "clientCarrierLocal";
$dbh = DBI->connect("dbi:SQLite:dbname=./dnsdata.db",'','',{
        RaiseError => 1,
        AutoCommit => 0
    }
);


foreach (@files) {
    next if $_ eq "." or $_ eq "..";
    my $basename = basename($_,'.acl');
    my $carrier = substr($basename,-2);
    my $location = substr($basename,0,-2);
    my $carriercn = $ipCarrier{$carrier};
    my $locationcn = $ipRegion{$location};
    my @lines = &readWholeFile($mydir.'/'.$_);
    chomp(@lines);

    foreach (@lines) {
        if($_ =~ /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])/) {
            my $ip = substr($_,0,-1);
            $dbh->do("insert into $tableClientIp(clicarrilocal,iprank,clicarri,clilocal,clicarricn,clilocalcn) values('$location$carrier','$ip','$carrier','$location','$carriercn','$locationcn')");
        }
    }
    $dbh->commit();
}

sub scanDir{
    my $dirPath = pop(@_);
    opendir DIR, ${dirPath} or die "Can't open \"$dirPath\"\n";
    return(readdir DIR);
}
sub readWholeFile{
    my $file = pop(@_);
    open FILE, $file or die "Can't open \"$file\"\n";
    return <FILE>;
}

<<COM;
CREATE TABLE clientToServer (
    id              INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    foreigndomain   VARCHAR(255),
    clicarrilocal   VARCHAR(255) NOT NULL,
    basedomain      VARCHAR(255) NOT NULL,
    recordtype      VARCHAR(255) NOT NULL,
    iporcname       VARCHAR(255) NOT NULL,
    changetime      VARCHAR(255),
    UNIQUE  (id)
    );
CREATE TABLE clientCarrierLocal (
    id              INTEGER NOT NULL PRIMARY KEY,
    clicarrilocal   VARCHAR(255) NOT NULL,
    clicarri        VARCHAR(255) NOT NULL,
    clilocal        VARCHAR(255) NOT NULL,
    iprank          VARCHAR(255) NOT NULL,
    UNIQUE  (id,clicarrilocal) 
    );
CREATE TABLE edgeServerInfo (
    id                 INTEGER NOT NULL PRIMARY KEY,
    iporcname          VARCHAR(255) NOT NULL,
    edgecarrier        VARCHAR(255) NOT NULL,
    edgelocal          VARCHAR(255) NOT NULL,
    UNIQUE  (id,iporcname)
    );
COM
