#!/usr/local/bin/perl -w
use DBI;

my $dbFile = "./dnsdata.db";
$dbh = DBI->connect("dbi:SQLite:dbname=$dbFile",'','',{
        RaiseError => 1,
        AutoCommit => 0
    }
);

$dbh->do(<<DBEOF);
    CREATE TABLE IF NOT EXISTS clientCarrierLocal (
    id              INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    clicarrilocal   VARCHAR(255) NOT NULL,
    iprank          VARCHAR(255) NOT NULL,
    clicarri        VARCHAR(255) NOT NULL,
    clilocal        VARCHAR(255) NOT NULL,
    clicarricn        VARCHAR(255) NOT NULL,
    clilocalcn        VARCHAR(255) NOT NULL,
    cliregion        VARCHAR(255) NULL,
    cliregioncn        VARCHAR(255) NULL,
UNIQUE  (id,clicarrilocal) 
    )
DBEOF

$dbh->do(<<DBEOF);
    CREATE TABLE IF NOT EXISTS edgeServerInfo (
    id                 INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    edgeserver         VARCHAR(255) NOT NULL,
    iporcname          VARCHAR(255) NOT NULL,
    mytype             INTERGE(1),
    edgecarrier        VARCHAR(255) NOT NULL,
    edgelocal          VARCHAR(255) NOT NULL,
    edgestatus         INTERGE(1) NOT NULL,
    UNIQUE  (id,iporcname)
    )
DBEOF

$dbh->do(<<DBEOF);
    CREATE TABLE IF NOT EXISTS clientToServer (
    id              INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    foreigndomain   VARCHAR(255),
    clicarrilocal   VARCHAR(255) NOT NULL,
    basedomain      VARCHAR(255) NOT NULL,
    recordtype      VARCHAR(255) NOT NULL,
    iporcname       VARCHAR(255) NOT NULL,
    pdnsid          INTEGER NOT NULL,
    changetime      VARCHAR(255),
    UNIQUE  (id)
    )
DBEOF

$dbh->commit();
if($dbh->err()){
print "$DBI::errstr\n";
}
