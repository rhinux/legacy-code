#!/usr/local/bin/perl -w
%ipRegion = (                                                                                                            
    'beijing'=>'北京','shanghai'=>'上海','tianjin'=>'天津','chongqing'=>'重庆','heilongjiang'=>'黑龙江','jilin'=>'吉林',
    'liaoning'=>'辽宁','neimeng'=>'内蒙古','hebei'=>'河北','henan'=>'河南','guangdong'=>'广东','hubei'=>'河北',
    'shandong'=>'山东','zhejiang'=>'浙江','anhui'=>'安徽','jiangsu'=>'江苏','jiangxi'=>'江西','yunan'=>'云南',
    'ningxia'=>'宁夏','qinghai'=>'青海','hillxi'=>'山西','shanxi'=>'陕西','hunan'=>'湖南','fujian'=>'福建',
    'gansu'=>'甘肃','sichuan'=>'四川','guangxi'=>'广西','guizhou'=>'贵州','hainan'=>'海南','xizang'=>'西藏', 
    'xinjiang'=>'新疆','hongkong'=>'香港','macao'=>'澳门','taiwan'=>'台湾','or'=>'其他地区', 
);                                                                                                                                             
%ipCarrier = (                                                                                                                
    'mm'=>'移动mobile','cm'=>'电信mobile','um'=>'联通mobile','mw'=>'移动wifi','cw'=>'电信wifi','uw'=>'联通wifi',
    'oi'=>'其它运营商',
); 
1;
