#!/usr/local/bin/perl5 -w
chdir("/usr/home/rhinux/Code/perl/cdnlogcatch");
use LWP::Simple;
use Sys::Syslog qw(:DEFAULT setlogsock);
use Config::IniFiles;
use Net::FTP;
use File::Copy;
use File::Path;
use POSIX qw/strftime/;
use Compress::Bzip2;
require 'fun/fun.pl';

#init global config
my $cfg = new Config::IniFiles(-file => "conf/config.ini",
    -allowcontinue => 1,
    -reloadwarn => 1,
    -nocase => 1,);

@sections = $cfg->Sections;
shift(@sections);
$mylogIdentity = $cfg->val('global','mylogIdentity')?$cfg->val('global','mylogIdentity'):'cdnLogCatch';
$mylogOn = $cfg->val('global','mylogOn')?$cfg->val('global','mylogOn'):1;
$mylogFacility = $cfg->val('global','mylogFacility')?$cfg->val('global','mylogFacility'):'local0';
#$applogFacility = $cfg->val('global','applogFacility')?$cfg->val('global','applogFacility'):'local1';
$mylogLevel = $cfg->val('global','mylogLevel')?$cfg->val('global','mylogLevel'):'debug';
#$applogLevel = $cfg->val('global','applogLevel')?$cfg->val('global','applogLevel'):'info';
$localPath = $cfg->val('global','localPath')?$cfg->val('global','localPath'):'/services/CDNLOGS';
$yearM = strftime "%Y/%m",localtime(time() - 86400);
$logBak = "log_bak/".$yearM."/";
$srcBak = "src_bak/".$yearM."/";
$logCollectHost = $cfg->val('global','logCollectHost')?$cfg->val('global','logCollectHost'):'211.136.105.229';
$logCollectPort = $cfg->val('global','logCollectPort')?$cfg->val('global','logCollectPort'):'9999';
$logCollectUser = $cfg->val('global','logCollectUser')?$cfg->val('global','logCollectUser'):'uploadmdnlog';
$logCollectPass = $cfg->val('global','logCollectPass')?$cfg->val('global','logCollectPass'):'loadlog';
$logCollectPath = $cfg->val('global','logCollectPath')?$cfg->val('global','logCollectPath'):'/services/mdnlog';
#$bucketList = $cfg->val('global','bucketList')?$cfg->val('global','bucketList'):'madsolution,optimad';
#$monitorMail = $cfg->val('global','monitorMail')?$cfg->val('global','monitorMail'):'xuyunfeng@madhouse-inc.com';

-d $logBak || mkpath($logBak,0,0777);
-d $srcBak || mkpath($srcBak,0,0777);

while($provider=shift(@sections)){
    sleep 1;
    if($provider =~ /ws/){
        $trytimes=1;
        my @wsuploadFiles = &wsLogProcess;
        foreach $wsuploadFile (@wsuploadFiles){
            if($wsuploadFile){
                #scp($host,$port,$user,$password,$src,$dest)
                if(&scp($logCollectHost,$logCollectPort,$logCollectUser,$logCollectPass,$wsuploadFile,$logCollectPath)){
                    move($wsuploadFile,$logBak) or &mylogSay("[wsLogPorcess] backup $wsuploadFile to $logBak failed");
                    &mylogSay("[wsLogPorcess] backup $wsuploadFile to $logBak");
                    &mylogSay("[wsLogPorcess] upload $wsuploadFile to $logCollectHost succeed");
                    &sendMail("[wsLogPorcess] upload $wsuploadFile to $logCollectHost succeed");
                }else{
                    &sendMail("[wsLogPorcess] get succeed but upload $wsuploadFile to $logCollectHost failed");
                }
            }else{
                &sendMail("[wsLogPorcess] get logs failed");
            }
        }
    }elsif($provider =~ /wg/){
        $trytimes=1;
        my @wguploadFiles = &wgLogProcess;
        foreach $wguploadFile (@wguploadFiles){
            if($wguploadFile){
                if(&scp($logCollectHost,$logCollectPort,$logCollectUser,$logCollectPass,$wguploadFile,$logCollectPath)){
                    move($wguploadFile,$logBak) or &mylogSay("[wgLogPorcess] backup $wguploadFile to $logBak failed");
                    &mylogSay("[wgLogPorcess] backup $wguploadFile to $logBak");
                    &mylogSay("[wgLogPorcess] upload $wguploadFile to $logCollectHost succeed");
                    &sendMail("[wgLogPorcess] upload $wguploadFile to $logCollectHost succeed");
                }else{
                    &sendMail("[wgLogPorcess] get succeed but upload $wguploadFile to $logCollectHost failed");
                }
            }else{
                &sendMail("[wgLogPorcess] get logs failed");
            }
        }
    }else{
        &mylogSay("unknow sections:$provider");
    }
}

#syslogSay($messages)
sub mylogSay{
    my $messages = pop(@_);
    if($mylogOn){
        $socketype = "unix";
        setlogsock($socketype);
        @options = ('cons','pid');
        openlog($mylogIdentity,\@options,$mylogFacility);
    syslog($mylogLevel,$messages);
}
}
sub wsLogProcess {
    if ($trytimes >= 4) {
        &mylogSay("[wsLogProcess] Too many try times quit getting!");
        &sendMail("[wsLogProcess] Too many try times quit getting!");
        return(0);
    }
    &mylogSay("[wsLogPorcess] Try $trytimes times Getting logs");
    $trytimes++;

    my $ftpHost = $cfg->val($provider,'host')?$cfg->val($provider,'host'):'ftp.wslog.chinanetcenter.com';
    my $ftpUser = $cfg->val($provider,'user')?$cfg->val($provider,'user'):'madhouse';
    my $ftpPasswd = $cfg->val($provider,'passwd')?$cfg->val($provider,'user'):'YiDong123';
    my $ftpPath = $cfg->val($provider,'path')?$cfg->val($provider,'path'):'dl.mgogo.com';
    my $date = strftime "%Y-%m-%d",localtime(time() - 86400);
    my $remoteFile =$date."-0000-2330_".$ftpPath.".cn.log.gz";
    my $localFile = $localPath.'/'.$remoteFile;

    my $wsFtp = Net::FTP->new($ftpHost, Timeout => 300)
        or die &mylogSay("Can't connect ftphost $ftpHost "); 
    $wsFtp->login($ftpUser,$ftpPasswd) or die &mylogSay("Can't login $ftpHost with $ftpUser");
    $wsFtp->binary();
    $wsFtp->cwd($ftpPath);
    $wsFtp->get($remoteFile,$localFile) or &wsLogProcess;
    my $remoteSize = $wsFtp->size($remoteFile);
    $wsFtp->quit;
    my $localSize =  -s $localFile;
    if ($remoteSize == $localSize){
        &mylogSay("[wsLogPorcess] remoteSize: $remoteSize = localSize $localSize trans succeed");
        &mylogSay("[wsLogPorcess] localFile in $localFile");
        
        my @saveLog = &logSplit("ws",$localFile);
        return (@saveLog);
        #return($localFile);
    }else{
        &mylogSay("[wsLogPorcess] remoteSize: $remoteSize != localSize $localSize trans failed");
        return(0);
    }
}

sub wgLogProcess {
    #wglog url"http://log.cdnunion.com/17058/dl.mgogo.com/2013_03_05.ftp.log.gz"
    if ($trytimes >= 4){
        &mylogSay("[wgLogProcess] Too many try times quit getting!");
        return(0);
    }
    &mylogSay("[wgLogPorcess] Try $trytimes times Getting logs");
    $trytimes++;

    my $httpHost = $cfg->val($provider,'host')?$cfg->val($provider,'host'):'log.cdnunion.com';
    my $httpPath = $cfg->val($provider,'path')?$cfg->val($provider,'path'):'/17058/dl.mgogo.com/';
    my $date = strftime "%Y_%m_%d",localtime(time() - 86400);
    my $fileName=$date.".ftp.log.gz";
    my $httpFile = "http://".$httpHost.$httpPath.$fileName;
    my $localFile = $localPath.'/'.$fileName;
    my $returnCode = getstore($httpFile, $localFile);
    my $localSize = -s $localFile;

    if ($returnCode == 200){
        &mylogSay("[wgLogPorcess] http code return 200 transfer $localSize Bytes trans succeed");
        my @saveLog = &logSplit("wg",$localFile);
        return (@saveLog);
    }else{
        &wgLogProcess;
    }
}

sub logSplit {
    my  $localFile = pop(@_);
    my  $publisher = pop(@_);
    my  @localFileList = (); 
    my  @outputHandle = (); 
    my $count=0;
    &mylogSay("[LogSplit] processing $localFile AT  $publisher");
    my $date = strftime "%Y%m%d",localtime(time() - 86400);
    open(FN,"gzip -dc $localFile|") or die ("can't open file\n");
    while(<FN>){
        #找到bucket名字最为文件
        my $ba = $1 if /GET http:\/\/dl\.mgogo\.com\/([\da-z]{1,32})\//;
        if(defined($ba)){
            ${$ba} = $localPath.'/'.$ba.'-'.$publisher.'_'.$date.'.bz2';
            my $out = "OUT".$ba;
            #bucket作为文件名和文件handle,每条数据根据匹配的bucket存入该文件
            if(!(-e ${$ba})){
                #新bucket文件名，存入返回数组返回给上层调用
                @localFileList = (@localFileList,${$ba});
                (${$out},$status) = bzdeflateInit();
                &mylogSay("erro write $bzerrno\n") if $status;
                ${$out} = Compress::Bzip2->new();
                ${$out} =Compress::Bzip2::bzopen("${$ba}","w");
                #压缩文件的对象名字保存好退出过程时需要关闭
                @outputHandle =(@outputHandle,$out);
            }
            ${$out}->bzwrite($_) or die  &mylogSay("erro write $bzerrno\n");
        }
    }
    close FN;
    #关闭压缩文件，所有内容才会压缩写入磁盘
    #如果每次bzwrite后用bzflush,会影响压缩率,也不能代替最后的bzclose动作
    foreach $outputHandle (@outputHandle){
        ${$outputHandle}->bzclose();
    }
    move($localFile,$srcBak) or &mylogSay("[$publisher LogPorcess] backup $localFile to $srcBak failed");
    &mylogSay("[$publisher LogPorcess] backup $localFile to $srcBak .");
    return @localFileList;
}

