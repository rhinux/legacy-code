#!/bin/bash
# ServerSecurityGroup(SSG) manager script
# version 20210325
# author Rhinux <Rhinux.x@gmail.com>
scriptName="${0##*/}"


function printUsage() {
    cat <<EOF
ServerSecurityGroup(SSG) manager script
version 20210325

Synopsis
    $scriptName < -C COMMAND > [-F profile] [-I interactive]  [interactive OPTIONS:<-N SSGName> [-M SSGMembers] [-P SSGSrvPorts] [-n ClientGroupNames] [-m ClientMembers]]
    
Usage:
-C COMMAND:
    SSGStatus             list ServerSecurityGroup rule
              OPTIONS: <-N SSGName> 
    INPUTLinksClear       clear INPUT Chain links 
              OPTIONS: <-N SSGName>
    selfChainDelete           delete selfChain
              OPTIONS: <-N SSGName>
    selfChainCreate           create new selfChain 
             OPTIONS: <-N SSGName>
    selfChainRuleCreate       create selfChain rules
             OPTIONS: <-N SSGName>  <-P SSGSrvPorts> <-m ClientMembers>.
    LinkRuleCreate        create links rule at INPUT Chain 
             OPTIONS: <-N SSGName> <-P SSGSrvPorts> 
    INPUTLinksClear       clear selfChain ref
             OPTIONS: <-N SSGName>
    doFlushToNewRule      flush  selfChian linksrule to new 
            OPTIONS: <ALL OPTIONS>

 -I interactive (true or false)
    default is  false

 -F profile
    SSG configure file. will  overwrite [interactive OPTIONS]
 interactive OPTIONS:
    -N SSGName 
    -M SSGMembers
       default localhost
    -P SSGSrvPorts
    -n ClientGroupNames
    -m ClientMembers : the ipaddress of services user  include MGR members

Example:
  ./myiptables.sh -C SSGStatus  -N KUBE-to-MYSQ
  ./myiptables.sh -C selfChainCreate  -N KUBE-to-MYSQL
  ./myiptables.sh -C LinkRuleCreate  -N KUBE-to-MYSQL -P "3306 3307"
  ./myiptables.sh -C selfChainRuleCreate -N KUBE-to-MYSQL -P "3306 3307" -m "192.168.33.1 192.168.33.8 192.168.72.8"
  ./myiptables.sh -C selfChainDelete -N KUBE-to-MYSQL
 ./myiptables.sh  -C INPUTLinksClear -N KUBE-to-MYSQL
  ./myiptables.sh -C doFlushToNewRule  -F iptablesProfile
profile example:
---------------------------------------------------------------------------------
SSGName="KUBE-to-MYSQL"
SSGMembers="192.168.196.119 192.168.196.118"
SSGSrvPorts="3301 3304"
ClientGroupNames="Kube,web"
ClientMembers="192.168.196.53 192.168.196.111 192.168.196.61 192.168.196.71"
COMMENT="$SSGName $ClientGroupNames"
---------------------------------------------------------------------------------
EOF
 
}

# Options.
while getopts ":C:I:F:N:M:P:n:m:" option; do
    case "$option" in
        C) COMMAND=$OPTARG ;;
        I) Interactive=$OPTARG;;
        F) PROFILE=$OPTARG ;;
        N) SSGName=$OPTARG ;;
        M) SSGMembers=$OPTARG ;;
        P) SSGSrvPorts=$OPTARG ;;
        n) ClientGroupNames=$OPTARG ;;
        m) ClientMembers=$OPTARG ;;
        *) printUsage; exit 1 ;;
    esac
done
#shift $((OPTIND - 1))

# $# should be at least 1 (the command to execute), however it may be strictly
# greater than 1 if the command itself has options.
if (($# == 0 )); then
    printUsage
    exit 1
fi

if [ 0$Interactive == "0" ] || [ $Interactive == "flase" ];then
    if [ 0$PROFILE -eq "0" ]; then
    echo "warnning: profile not found!"
    else
     source  $PROFILE
    fi
fi

if [ 0$COMMAND == "0" ]; then
 printUsage
 echo "something error: lost command"
 exit 1
fi

if [ $COMMAND == "SSGStatus" ] || [ $COMMAND == "selfChainDelete" ] ||  \
     [ $COMMAND == "ExistChain" ] || [ $COMMAND == "selfChainCreate" ] || \
        [ $COMMAND == "INPUTLinksClear" ] [ $COMMAND == "INPUTLinksClear" ]; then
  if [ 0$SSGName == "0" ] ; then
    printUsage
    echo "something error: lost SSGName option after SSGStaus|selfChainDelete|ExistChain|INPUTLinksClear commands!"
    exit 1
  fi
fi

if [ $COMMAND == "selfChainRuleCreate" ] ; then
  if [ 0$SSGName == "0" ]  || [ 0$SSGSrvPorts == "0" ] ||  [ 0$ClientMembers == "0" ]; then
    printUsage
    echo "something error: lost SSGName or SSGSrvPorts or ClientMembers options after command selfChainRuleCreate"
    exit 1
  fi
fi

if [ $COMMAND == "LinkRuleCreate" ] ; then
  if [ 0$SSGName == "0" ]  || [ 0$SSGSrvPorts == "0" ] ; then
    printUsage
    echo "something error: lost SSGName or SSGSrvPorts options after command LinkRuleCreate"
    exit 1
  fi
fi

#if [ 0$SSGName -eq "0" ]; then
# printUsage
# exit 1
#fi

#SSGName="KUBE-to-MYSQL"
#SSGMembers="192.168.196.119 192.168.196.118"
#SSGSrvPorts="3301 3304"
#ClientGroupNames="Kube,web"
#ClientMembers="192.168.196.53 192.168.196.111 192.168.196.61 192.168.196.71"
#COMMENT="$SSGName $ClientGroupNames"
#### ServerSecurityGroup(SSG) conf-file

#SSGName="KUBE-to-MYSQL"
#SSGPort="3301 3304"
#[SSGMembers]
#192.168.196.119
#192.168.196.118
#
#
####Client name & member conf-file
#[SSGSelf]
#192.168.196.119
#192.168.196.119
#[kube]
#192.168.196.53
#192.168.196.53
#[web]
#192.168.196.61
#192.168.196.71

# firewall status count
# func Create Chain  (clear,create,allow member)

#func clear (clear ref,clear chain)
#clear 
#Chain
#iptables --list INPUT  -nv --line-numbers|grep KUBE-to-MYSQL|awk '{print $1}'

#scan SSGName

function ExistLinkInINPUT()
{
EXISTININPUT=`iptables --list INPUT  -nv |grep $SSGName|wc -l`
 if [ $EXISTININPUT -ge 1 ] ; 
  then
 echo "`date`|FUN: ExistLinkInINPUT| $SSGName exists in INPUT Chain " 
 echo "################################ result ##############################"
 iptables --list INPUT  -nv --line-numbers| grep $SSGName
 echo "################################ result ##############################"
  return 1
 else
 echo "`date`|FUN: ExistLinkInINPUT| $SSGName not exists in INPUT Chain " 
  return 0
 fi
}


function ExistChain()
{
 #EXISTChain=`iptables --list $SSGName -nv --line-number|wc -l`
 EXISTChain=`iptables --list -nv |grep -i "Chain $SSGName" |wc -l`
 if [ $EXISTChain -ge 1 ] ; 
  then
  echo "`date`|FUN: ExistChain| $SSGName Chain exists"
 echo "################################ result ##############################"
 iptables --list $SSGName  -nv --line-numbers
 echo "################################ result ##############################"
  return 1
 else
  return 0
 fi
}

function SSGStatus()
{
 echo "`date`|FUN: SSGStatus| Scaning  ${SSGName} INPUT Links:"
 iptables --list INPUT  -nv --line-numbers|grep $SSGName
 echo "`date`|FUN: SSGStatus| Scaning Self Chain ${SSGName}:"
 iptables --list $SSGName -nv --line-numbers
}

function INPUTLinksClear()
{
ExistLinkInINPUT
if [ $? -eq 1 ]; then
  echo "`date`|FUN: INPUTLinksClear| Checking INPUT Chain With Link ${SSGName}:"
  for n in `iptables --list INPUT  -nv --line-numbers|grep $SSGName|awk '{print $1}'`
    do
    LineNumber=`iptables --list INPUT  -nv --line-numbers|grep $SSGName|head -n 1|awk '{print $1}'`
    iptables -D INPUT $LineNumber 
    if [ $? -eq 0 ] ; then
       echo "`date`|FUN: INPUTLinksClear| ${SSGName} INPUT Links Line $n clear succeeded"
    else
       echo "`date`|FUN: INPUTLinksClear| ${SSGName} INPUT Links Line $n clear failed"
       return 0
    fi
    done
    return 1
else
  echo "`date`|FUN: INPUTLinksClear| ${SSGName} INPUT Links not exist" 
  return 1
fi
}

function selfChainDelete()
{
 ExistChain
 if [ $? -eq 1 ] ; then
   echo "`date`|FUN: selfChainDelete| ${SSGName} exist in chain, cleaning..."
   iptables -t filter -F  ${SSGName}
   if [ $? -eq 0 ] ; then
     echo "`date`|FUN: selfChainDelete| Chain ${SSGName} rule Flush succeeded" 
     iptables -t filter -X  ${SSGName}
     if [ $? -eq 0 ];then
       echo "`date`|FUN: selfChainDelete| Chain ${SSGName} Delete succeeded"
       return 1
     else
       echo "`date`|FUN: selfChainDelete| Chain ${SSGName} Delete failed"
       return 0
     fi
   else
     echo "`date`|FUN: selfChainDelete| Chain ${SSGName} rule Flush failed"
     return 0
   fi
 else
   echo "`date`|FUN: selfChainDelete| Chain ${SSGName} not exist"
   return 1
 fi 
}

function selfChainCreate()
{
   ExistChain
   if [ $? -eq 1 ] ; then
     echo "`date`|FUN: selfChainCreate| Chain ${SSGName} exist created failed."
     return 0
   fi

   ExistLinkInINPUT
   if [ $? -eq 1 ] ; then
     echo "`date`|FUN: selfChainCreate| Chain ${SSGName} links exist in INPUT  created failed."
     return 0
   fi

   iptables -t filter -N ${SSGName} 
   if [ $? -eq 0 ]; then
     echo "`date`|FUN: selfChainCreate| Chain ${SSGName} links create succeeded"
     return 1
   fi
}

function LinkRuleCreate()
{
  ## Link Name,
  ExistLinkInINPUT
  if [ $? -eq 1 ] ; then
    echo "`date`|FUN: LinkRuleCreate| Chain ${SSGName} links exist in INPUT  created failed."
    return 0
  fi
  for PortNumber in `echo $SSGSrvPorts`
    do
    iptables -t filter -I INPUT -p tcp --dport $PortNumber -j $SSGName
    if [ $? -eq 0 ] ; then
      echo "`date`|FUN: LinkRuleCreate| Chain ${SSGName} TCP Port ${PortNumber} links rule  created succeeded." 
    else
      echo "`date`|FUN: LinkRuleCreate| Chain ${SSGName} TCP  Port ${PortNumber} links rule  created failed." 
      return 0
    fi
  done 
  echo "`date`|FUN: LinkRuleCreate| Chain ${SSGName} all tcp ports  links rule  created succeeded." 
  return 1
}


function selfChainRuleCreate()
{
   ExistChain
   if [ $? -eq 1 ] ; then
     echo "`date`|FUN: selfChainRuleCreate| Chain ${SSGName} checking exist,start create chain rule"
   else
     echo "`date`|FUN: selfChainRuleCreate| Chain ${SSGName} checking not exist,can not create Chain rule"
     return 0
   fi
#   if [ ExistLinkInINPUT ] ; then
#     echo "`date`|FUN: selfChainRuleCreate| Chain ${SSGName} Links checking exist."
#   else
#     echo "`date`|FUN: selfChainRuleCreate| Chain ${SSGName} Links checking not exist."
#     return 0
#   fi
   iptables -A $SSGName  -p tcp -m tcp  -j DROP  -m comment --comment "default rule for selfChain"

   for SRCIP in `echo $ClientMembers`
   do
      for DSTPORT in `echo $SSGSrvPorts`
      do
#   iptables -A KUBE-to-MYSQL  -p tcp -m tcp  -s 172.16.22.33/32 --dport 3301 -j ACCEPT  -m comment --comment "k8s to mysql port"
   	iptables -I $SSGName  -p tcp -m tcp  -s $SRCIP --dport $DSTPORT -j ACCEPT  -m comment --comment "$COMMENT"
        if [ $? -eq 0 ] ; then
           echo "`date`|FUN: selfChainRuleCreate| Chain ${SSGName} srcIP ${SRCIP} dstPort ${DSTPORT}  rule  created succeeded." 
        else
           echo "`date`|FUN: selfChainRuleCreate| Chain ${SSGName} srcIP ${SRCIP} dstPort ${DSTPORT}  rule  created failed." 
           return 0
        fi
      done
   done
   echo "`date`|FUN: selfChainRuleCreate| Chain ${SSGName} all rules  created succeeded." 
   return 1
}

function FlushToNewRule()
{
      INPUTLinksClear
      if [ $? -eq 1 ] ; then
         selfChainDelete
         if [ $? -eq 1 ]; then
            selfChainCreate
            if [ $? -eq 1 ]; then
	       selfChainRuleCreate
               if [ $? -eq 1 ]; then
 		  LinkRuleCreate
                  if [ $? -eq 1 ]; then
                     return 1
                   else
	   	      return 0
                  fi
               else
                 return 0
               fi
            else
               return 0
            fi        
         else
             return 0
         fi
      else
         return 0	 
      fi
}
function doFlushToNewRule()
{
FlushToNewRule
if [ $? -eq 1 ] ;then
   echo "flush succeeded!"
else
   echo "somethings error! check log for detitle"
fi
}

$COMMAND
