## 防火墙组构建脚本使用手册

### 逻辑和思路
**逻辑思路**

1.用服务器iptables创建基于服务端口和源IP的白名单
2


**创建步骤**
1. 创建自定义Chain（SSGNAME），并创建默认DROP规则
2. 按服务端口在INPUT表中关联自定义的Chain
3. 在定义Chain（SSGNAME）中创建白名单规则

ServerSecurityGroup(SSG) manager

### 用例说明

**信息和需求**

1. MYSQL服务端口为3301 3304 
2. MYSQL服务器地址为 192.168.196.71 192.168.196.61 192.168.196.51
3. 允许客户端地址为 192.168.196.111 和192.168.196.53 可以访问MYSQL服务端口

**规则构建结果**
```
## 将目标端口为3301 3304的 INPUT包转到自定义链 KUBE-to-MYSQL
Chain INPUT (policy ACCEPT 1095 packets, 76308 bytes)
num   pkts bytes target     prot opt in     out     source               destination
1        0     0 KUBE-to-MYSQL  tcp  --  *      *       0.0.0.0/0            0.0.0.0/0            tcp dpt:3304
2        0     0 KUBE-to-MYSQL  tcp  --  *      *       0.0.0.0/0            0.0.0.0/0            tcp dpt:3301

Chain FORWARD (policy DROP 10270 packets, 616K bytes)
num   pkts bytes target     prot opt in     out     source               destination

Chain OUTPUT (policy ACCEPT 592 packets, 120K bytes)
num   pkts bytes target     prot opt in     out     source               destination

## 自定义链 KUBE-to-MYSQL，允许指定的源IP和目标端口为3301 3304的包进入其他数据包DROP
Chain KUBE-to-MYSQL (2 references)
num   pkts bytes target     prot opt in     out     source               destination
1        0     0 ACCEPT     tcp  --  *      *       192.168.196.71       0.0.0.0/0            tcp dpt:3304 /* KUBE-to-MYSQL Kube,web */
2        0     0 ACCEPT     tcp  --  *      *       192.168.196.71       0.0.0.0/0            tcp dpt:3301 /* KUBE-to-MYSQL Kube,web */
3        0     0 ACCEPT     tcp  --  *      *       192.168.196.61       0.0.0.0/0            tcp dpt:3304 /* KUBE-to-MYSQL Kube,web */
4        0     0 ACCEPT     tcp  --  *      *       192.168.196.61       0.0.0.0/0            tcp dpt:3301 /* KUBE-to-MYSQL Kube,web */
5        0     0 ACCEPT     tcp  --  *      *       192.168.196.61       0.0.0.0/0            tcp dpt:3304 /* KUBE-to-MYSQL Kube,web */
6        0     0 ACCEPT     tcp  --  *      *       192.168.196.61       0.0.0.0/0            tcp dpt:3301 /* KUBE-to-MYSQL Kube,web */
7        0     0 ACCEPT     tcp  --  *      *       192.168.196.111      0.0.0.0/0            tcp dpt:3304 /* KUBE-to-MYSQL Kube,web */
8        0     0 ACCEPT     tcp  --  *      *       192.168.196.111      0.0.0.0/0            tcp dpt:3301 /* KUBE-to-MYSQL Kube,web */
9        0     0 ACCEPT     tcp  --  *      *       192.168.196.53       0.0.0.0/0            tcp dpt:3304 /* KUBE-to-MYSQL Kube,web */
10        0     0 ACCEPT     tcp  --  *      *       192.168.196.53       0.0.0.0/0            tcp dpt:3301 /* KUBE-to-MYSQL Kube,web */
11        0     0 DROP       tcp  --  *      *       0.0.0.0/0            0.0.0.0/0            tcp /* default rule for selfChain */
```

### 执行步骤和方式

**A-配置文件方式--创建一套安全组**

*cat  ./iptablesProfile*
```
SSGName="KUBE-to-MYSQL"
#SSGMembers="192.168.196.71 192.168.196.61 192.168.196.51"
SSGSrvPorts="3301 3304"
ClientGroupNames="Kube,web"
ClientMembers="192.168.196.53 192.168.196.111 192.168.196.71 192.168.196.61 192.168.196.51"
COMMENT="$SSGName $ClientGroupNames"
```
**命令**
```
 ./myiptables.sh -C doFlushToNewRule  -F iptablesProfile
```


**B-命令交互模式--创建一套安全组**

*1.创建自定义链KUBE-to-MYSQL*

```
./myiptables.sh -C selfChainCreate  -N KUBE-to-MYSQL
```
*2. 创建INPUT表入口规则为 所有到3301 3304端口的包转入 自定义链KUBE-to-MYSQL*

```
 ./myiptables.sh -C LinkRuleCreate  -N KUBE-to-MYSQL -P "3301 3304"
```

*3.创建自定义链KUBE-to-MYSQL的规则*

```
注意：-P和-N参数必须和第2步中的相同，否则规则不会生效
 ./myiptables.sh -C selfChainRuleCreate -N KUBE-to-MYSQL -P "3306 3307" -m "192.168.196.53 192.168.196.111 192.168.196.71 192.168.196.61 192.168.196.51"
```
*4. 查看自定义链KUBE-to-MYSQL的状态*

```
./myiptables.sh -C SSGStatus  -N KUBE-to-MYSQL

```

**命令交互模式--删除一套安全组**
*1.删除自定义链KUBE-to-MYSQL 的INPUT表入口规则为*
```
 ./myiptables.sh  -C INPUTLinksClear -N KUBE-to-MYSQL
```
*2.删除自定义链KUBE-to-MYSQL*
```
 ./myiptables.sh -C selfChainDelete -N KUBE-to-MYSQL
```
*3. 查看自定义链KUBE-to-MYSQL的状态*

```
./myiptables.sh -C SSGStatus  -N KUBE-to-MYSQL

```

